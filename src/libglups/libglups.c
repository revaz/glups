#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include <Python.h>
#include <GL/glut.h>
#include <math.h>

#include "numpy/ndarraytypes.h"
#include "numpy/npy_3kcompat.h"
#include "numpy/ufuncobject.h"

/*********************************/
/*                               */
/*********************************/

static PyObject *libglups_MakePointList(PyObject *self, PyObject *args) {

  PyArrayObject *pos;

  float *x, *y, *z;
  int i;

  /* parse arguments */

  if (!PyArg_ParseTuple(args, "O", &pos))
    return NULL;

  glBegin(GL_POINTS);

  /* loops over all elements */
  for (i = 0; i < PyArray_DIM(pos, 0); i++) {

    x = (float *)PyArray_GETPTR2(pos, i, 0);
    y = (float *)PyArray_GETPTR2(pos, i, 1);
    z = (float *)PyArray_GETPTR2(pos, i, 2);

    glVertex3f(*x, *y, *z);
  }

  glEnd();

  return Py_BuildValue("i", 1);
}

/*********************************/
/*                               */
/*********************************/

static PyObject *libglups_MakeColorPointList(PyObject *self, PyObject *args) {

  PyArrayObject *pos;
  PyArrayObject *var;
  PyArrayObject *pal;

  float *x, *y, *z;
  int i, pi;
  float r, g, b;
  float mn, mx;
  float v;

  /* parse arguments */

  if (!PyArg_ParseTuple(args, "OOffO", &pos, &var, &mn, &mx, &pal))
    return NULL;

  glBegin(GL_POINTS);

  /* loops over all elements */
  for (i = 0; i < PyArray_DIM(pos, 0); i++) {

    x = (float *)PyArray_GETPTR2(pos, i, 0);
    y = (float *)PyArray_GETPTR2(pos, i, 1);
    z = (float *)PyArray_GETPTR2(pos, i, 2);

    v = *(float *)PyArray_GETPTR1(var, i);
    pi = (int)(255 * (v - mn) / (mx - mn));

    r = *(float *)PyArray_GETPTR2(pal, pi, 0);
    g = *(float *)PyArray_GETPTR2(pal, pi, 1);
    b = *(float *)PyArray_GETPTR2(pal, pi, 2);

    r = r / 255.;
    g = g / 255.;
    b = b / 255.;

    glColor3f(r, g, b);
    glVertex3f(*x, *y, *z);
  }

  glEnd();

  return Py_BuildValue("i", 1);
}

/*********************************/
/*                               */
/*********************************/

static PyObject *libglups_MakeArrowList(PyObject *self, PyObject *args) {

  PyArrayObject *pos, *vel;

  float dt;
  float *x, *y, *z;
  float *vx, *vy, *vz;
  int i;

  /* parse arguments */

  if (!PyArg_ParseTuple(args, "OOf", &pos, &vel, &dt))
    return NULL;

  glBegin(GL_LINES);

  /* loops over all elements */
  for (i = 0; i < PyArray_DIM(pos, 0); i++) {

    x = (float *)PyArray_GETPTR2(pos, i, 0);
    y = (float *)PyArray_GETPTR2(pos, i, 1);
    z = (float *)PyArray_GETPTR2(pos, i, 2);

    vx = (float *)PyArray_GETPTR2(vel, i, 0);
    vy = (float *)PyArray_GETPTR2(vel, i, 1);
    vz = (float *)PyArray_GETPTR2(vel, i, 2);

    glVertex3f(*x, *y, *z);
    glVertex3f(*x + *vx * dt, *y + *vy * dt, *z + *vz * dt);
  }

  glEnd();

  return Py_BuildValue("i", 1);
}

/* definition of the method table */

static PyMethodDef libglupsMethods[] = {

    {"MakePointList", libglups_MakePointList, METH_VARARGS,
     "Create a point list."},

    {"MakeColorPointList", libglups_MakeColorPointList, METH_VARARGS,
     "Create a point list with colors."},

    {"MakeArrowList", libglups_MakeArrowList, METH_VARARGS,
     "Create an arrow list."},

    {NULL, NULL, 0, NULL} /* Sentinel */
};

static struct PyModuleDef libglupsmodule = {
    PyModuleDef_HEAD_INIT,
    "libglups",
    "Defines opengl functions",
    -1,
    libglupsMethods,
    NULL, /* m_slots */
    NULL, /* m_traverse */
    NULL, /* m_clear */
    NULL  /* m_free */
};

PyMODINIT_FUNC PyInit_libglups(void) {
  PyObject *m;
  m = PyModule_Create(&libglupsmodule);
  if (m == NULL)
    return NULL;

  import_array();

  return m;
}
