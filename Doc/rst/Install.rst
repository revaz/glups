Install
*******

To install **glups**, use the **pip** utility ::

  pip install ./glups

where ``./glups`` is the clone of the git repository.
