User Guide
**********

**glups** runs from the command line. To get the list of options type::

  glups -h

To open and visualize an N-body file::

  glups snapshot_0001.hdf5
  
  
To open and visualize a list of N-body files::

  glups snapshot_*.hdf5  


In both cases the **glups** window opens an display the N-body model.


Panels
------

The **glups** window is splitted into four panels.
The GL display panel, the Map display panel, the controll panel and the python interactive console.


.. toctree::
   :maxdepth: 2

   GLDisplay
   MapDisplay
   ControllPanel
   Console



Mouse commands
--------------




Observer mode

======== ====================================================
Key      Command
======== ====================================================
x        align x axis to the right
y        align y axis to the right
z        align z axis to the right
u        set the point of interest to (0,0,0)
r        reverse motion speed (auto motion mode)
m        switch motion mode
"+"      increase motion speed
"-"      decrease motion speed
"left"   rotate left  (depends on the motion mode)
"right"  rotate right (depends on the motion mode)
"up"     rotate up    (depends on the motion mode)
"down"   rotate down  (depends on the motion mode)
======== ====================================================


Object mode

======== =========================
Key      Command
======== =========================
"+"      increase object size
"-"      decrease object size
v        display/hide object
======== =========================



======== =========================
Key      Command
======== =========================
d
======== =========================




