Download
********

The current version of **glups** may be downloaded using the open source version control system `git <http://git-scm.com>`_::

  git clone https://gitlab.com/revaz/glups.git glups

