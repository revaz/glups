#!/usr/bin/env python
'''
 @package   glups
 @file      download.py
 @brief     Download a snapshot.
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section COPYRIGHT

 Copyright (C) 2015 EPFL (Ecole Polytechnique F??d??rale de Lausanne)
 LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of glups.
'''


import urllib.request
import urllib.parse
import urllib.error
import os

filename = ""

if (not os.path.isfile(filename)):
    print("Downloading file (1.2Gb)...")
    urllib.request.urlretrieve(
        "http://lastro.epfl.ch/projects/glups/shared/lcdm.dat", filename)
    print("Done.")
else:
    print("Tile already exists. No need to download...")
