#!/bin/bash

autopep8 -r -j -1 --in-place --aggressive --aggressive conf config examples glups scripts setup.py
clang-format-5.0 -style=file -i src/*/*.[ch]
