# -*- coding: utf-8 -*-

'''
 @package   glups
 @file      MireObject.py
 @brief     Definition of an Mire object
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section COPYRIGHT

 Copyright (C) 2015 EPFL (Ecole Polytechnique F??d??rale de Lausanne)
 LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of glups.
'''

from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *

from PyQt5 import QtCore, QtGui, QtOpenGL

from .GLObject import *
from .ParameterList import *


class MireObject(GLObject):

    def __init__(self, ID='Mire'):

        self.ID = ID
        self.ListID = None

        # init parameters
        self.param = ParametersList(self.ID)
        self.param["Color_r"] = 1.0
        self.param["Color_g"] = 1.0
        self.param["Color_b"] = 0.0
        self.param["Color_a"] = 1.0
        self.param["show"] = 0
        self.param["PosX"] = 0.0
        self.param["PosY"] = 0.0
        self.param["PosZ"] = 0.0
        self.param["SizeX"] = 1.0
        self.param["SizeY"] = 1.0
        self.param["SizeZ"] = 1.0
        self.param["RotX"] = 0.0
        self.param["RotY"] = 0.0
        self.param["RotZ"] = 0.0
        self.param["RotationFactor"] = 0.1
        self.param["SizeFactor"] = 0.1
        self.GenList()

    def GenList(self):

        if self.ListID is None:
            self.ListID = glGenLists(1)
        glNewList(self.ListID, GL_COMPILE)

        glPointSize(2.0)

        glBegin(GL_LINES)
        glVertex3f(0, 0, 0)
        glVertex3f(0, 1, 0)

        glVertex3f(0, 0, 0)
        glVertex3f(0, -1, 0)

        glVertex3f(0, 0, 0)
        glVertex3f(1, 0, 0)

        glVertex3f(0, 0, 0)
        glVertex3f(-1, 0, 0)
        glEnd()

        glBegin(GL_LINE_LOOP)

        for ti in range(360):
            t = float(ti) * pi / 180.
            glVertex3f(cos(t) * 0.7, sin(t) * 0.7, 0)

        glEnd()

        glPointSize(1.0)

        glEndList()

    def Display(self, color=None, mode=False):
        '''
        display the object
        '''

        if (self.param['show']):
            if color is None:
                glColor4d(
                    self.param["Color_r"],
                    self.param["Color_g"],
                    self.param["Color_b"],
                    self.param["Color_a"])
            else:
                glColor4d(color[0], color[1], color[2], self.param["Color_a"])

            glPushMatrix()

            if mode != "static":
                glTranslated(
                    self.param["PosX"],
                    self.param["PosY"],
                    self.param["PosZ"])
                glScaled(
                    self.param["SizeX"],
                    self.param["SizeY"],
                    self.param["SizeZ"])
                glRotated(self.param["RotX"], 1, 0, 0)
                glRotated(self.param["RotY"], 0, 1, 0)
                glRotated(self.param["RotZ"], 0, 0, 1)

            glCallList(self.ListID)
            glPopMatrix()
