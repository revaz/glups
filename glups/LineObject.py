# -*- coding: utf-8 -*-

'''
 @package   glups
 @file      LineObject.py
 @brief     Definition of an Line object
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section COPYRIGHT

 Copyright (C) 2015 EPFL (Ecole Polytechnique F??d??rale de Lausanne)
 LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of glups.
'''

from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *

from PyQt5 import QtCore, QtGui, QtOpenGL

from .GLObject import *
from .ParameterList import *


class LineObject(GLObject):

    def __init__(self, wx0, wy0, wz0, wx1, wy1, wz1, ID='Line'):

        self.ID = ID
        self.ListID = None

        self.wx0 = wx0
        self.wy0 = wy0
        self.wz0 = wz0
        self.wx1 = wx1
        self.wy1 = wy1
        self.wz1 = wz1

        # init parameters
        self.param = ParametersList(self.ID)
        self.param["Color_r"] = 1.0
        self.param["Color_g"] = 0.0
        self.param["Color_b"] = 0.0
        self.param["Color_a"] = 1.0
        self.param["show"] = 1
        self.GenList()

    def GenList(self):

        if self.ListID is None:
            self.ListID = glGenLists(1)
        glNewList(self.ListID, GL_COMPILE)

        glPointSize(2.0)

        glBegin(GL_LINES)
        glVertex3f(self.wx0, self.wy0, self.wz0)
        glVertex3f(self.wx1, self.wy1, self.wz1)
        glEnd()

        glPointSize(1.0)

        glEndList()

    def Display(self, color=None):
        '''
        display the object
        '''
        if (self.param['show']):
            if color is None:
                glColor4d(
                    self.param["Color_r"],
                    self.param["Color_g"],
                    self.param["Color_b"],
                    self.param["Color_a"])
            else:
                glColor4d(color[0], color[1], color[2], self.param["Color_a"])

            glPushMatrix()
            glCallList(self.ListID)
            glPopMatrix()

    def ReadParameters(self, file):
        '''
        read parameters
        '''
        pass

    def WriteParameters(self, file):
        '''
        write parameters
        '''
        pass

    def AutoSetParameters(self, ModelSize, CenterOfMass):
        '''
        auto set parameters
        '''
        pass
