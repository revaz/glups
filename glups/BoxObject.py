# -*- coding: utf-8 -*-

'''
 @package   glups
 @file      BoxObject.py
 @brief     Definition of an Box object
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section COPYRIGHT

 Copyright (C) 2015 EPFL (Ecole Polytechnique F??d??rale de Lausanne)
 LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of glups.
'''

from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *

from PyQt5 import QtCore, QtGui, QtOpenGL

from .GLObject import *
from .ParameterList import *


class BoxObject(GLObject):

    def __init__(self, ID='Box'):

        self.ID = ID
        self.ListID = None

        # init parameters
        self.param = ParametersList(self.ID)
        self.param["Color_r"] = 0.0
        self.param["Color_g"] = 1.0
        self.param["Color_b"] = 0.0
        self.param["Color_a"] = 1.0
        self.param["show"] = 1
        self.param["PosX"] = 0.0
        self.param["PosY"] = 0.0
        self.param["PosZ"] = 0.0
        self.param["SizeX"] = 10.0
        self.param["SizeY"] = 10.0
        self.param["SizeZ"] = 10.0
        self.param["RotX"] = 0.0
        self.param["RotY"] = 0.0
        self.param["RotZ"] = 0.0
        self.param["RotationFactor"] = 0.1
        self.param["SizeFactor"] = 0.1
        self.GenList()

    def GenList(self):

        if self.ListID is None:
            self.ListID = glGenLists(1)
        glNewList(self.ListID, GL_COMPILE)

        # this is crashing with a recent version of opengl !!!
        #glutWireCube(1)
        # explicitly defines segments instead
        
        glTranslated(-0.5,-0.5,-0.5)
                
        glBegin(GL_LINE_STRIP)
        glVertex3f(0,0,0)
        glVertex3f(0,1,0)
        glVertex3f(1,1,0)
        glVertex3f(1,0,0)
        glVertex3f(0,0,0)
        glEnd()
        
        glBegin(GL_LINE_STRIP)
        glVertex3f(0,0,1)
        glVertex3f(0,1,1)
        glVertex3f(1,1,1)
        glVertex3f(1,0,1)
        glVertex3f(0,0,1)
        glEnd()

        glBegin(GL_LINES)
        glVertex3f(0,0,0)
        glVertex3f(0,0,1)
        glEnd()
        
        glBegin(GL_LINES)
        glVertex3f(0,1,0)
        glVertex3f(0,1,1)
        glEnd()
        
        glBegin(GL_LINES)
        glVertex3f(1,0,0)
        glVertex3f(1,0,1)
        glEnd()
        
        glBegin(GL_LINES)
        glVertex3f(1,1,0)
        glVertex3f(1,1,1)
        glEnd()
                
        glEndList()
