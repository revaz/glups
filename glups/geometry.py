# -*- coding: utf-8 -*-

'''
 @package   glups
 @file      geometry.py
 @brief     Definition of some geometrical functions.
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section COPYRIGHT

 Copyright (C) 2015 EPFL (Ecole Polytechnique F??d??rale de Lausanne)
 LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of glups.
'''

from numpy import *


def RotateAround(angle, axis, point, ObsM):
    '''
    this should be C
    '''

    # this work with OpenGL
    #Q = ones(16,float)
    # glLoadIdentity();
    # glTranslated(point[0],point[1],point[2]);
    # glRotated(angle,axis[0],axis[1],axis[2]);
    # glTranslated(-point[0],-point[1],-point[2]);
    ##Q = glGetDoublev(GL_MODELVIEW_MATRIX);
    # glMultMatrixd(ObsM);
    #ObsM = glGetDoublev(GL_MODELVIEW_MATRIX);
    # return ravel(ObsM)

    angle = angle * pi / 180
    point = concatenate((point, array([0])))

    M = ObsM
    M.shape = (4, 4)

    # center point
    M = M - point

    # construction of the rotation matrix
    norm = sqrt(axis[0]**2 + axis[1]**2 + axis[2]**2)
    if norm == 0:
        return x
    sn = sin(-angle / 2.)

    e0 = cos(-angle / 2.)
    e1 = axis[0] * sn / norm
    e2 = axis[1] * sn / norm
    e3 = axis[2] * sn / norm

    a = zeros((4, 4), float)
    a[0, 0] = e0**2 + e1**2 - e2**2 - e3**2
    a[1, 0] = 2. * (e1 * e2 + e0 * e3)
    a[2, 0] = 2. * (e1 * e3 - e0 * e2)
    a[3, 0] = 0.
    a[0, 1] = 2. * (e1 * e2 - e0 * e3)
    a[1, 1] = e0**2 - e1**2 + e2**2 - e3**2
    a[2, 1] = 2. * (e2 * e3 + e0 * e1)
    a[3, 1] = 0.
    a[0, 2] = 2. * (e1 * e3 + e0 * e2)
    a[1, 2] = 2. * (e2 * e3 - e0 * e1)
    a[2, 2] = e0**2 - e1**2 - e2**2 + e3**2
    a[3, 2] = 0.
    a[0, 3] = 0.
    a[1, 3] = 0.
    a[2, 3] = 0.
    a[3, 3] = 1.

    a = a.astype(float)

    # multiply x and v
    M = dot(M, a)

    # decenter point
    M = M + point

    return ravel(M)
