#!/usr/bin/env python

import numpy as np
from numpy.lib.recfunctions import append_fields

from pNbody import Nbody, ic

data_type = [
    ("name", "S20"),
    ("ascension", "f8"),
    ("declination", "f8"),
    ("distance", "f8"),
    ("mass", "f8"),
    ("radius", "f8"),
    ("source", "S200")
]


def computePositions(data):
    asc = data["ascension"]
    dec = data["declination"]
    dist = data["distance"]

    x = dist * np.cos(dec) * np.cos(asc)
    y = dist * np.cos(dec) * np.sin(asc)
    z = dist * np.sin(dec)
    return x, y, z


def addPosition(data, x, y, z):
    data = append_fields(data, "x", x)
    data = append_fields(data, "y", y)
    data = append_fields(data, "z", z)
    return data


def readDataFile(filename):

    data = np.genfromtxt(filename, dtype=data_type, delimiter=",")
    x, y, z = computePositions(data)
    data = addPosition(data, x, y, z)

    return data


def generateNbody(data, n_gal=1000, core=0.1, r_default=20.):
    # get data
    x = data["x"]
    y = data["y"]
    z = data["z"]
    halo_mass = data["mass"]
    radius = data["radius"]

    # create arrays
    N = len(x)
    pos = np.zeros((n_gal * N, 3))
    tpe = np.zeros(n_gal * N)
    mass = np.zeros(n_gal * N)

    # generate halo
    for i in range(N):
        ind = slice(i * n_gal, (i + 1) * n_gal)
        r = radius[i]
        m = halo_mass[i]
        if r < 0:
            tpe[ind] = 0
            r = r_default
        else:
            tpe[ind] = 1
        nb = ic.plummer(n_gal, a=r, b=r, c=r, eps=core, rmax=r, M=m)
        nb.translate([x[i], y[i], z[i]])
        pos[ind, :] = nb.pos
        mass[ind] = nb.mass

    # generate object
    nb = Nbody(pos=pos, mass=mass, tpe=tpe, ftype="gh5")
    nb.data = data
    return nb
