# -*- coding: utf-8 -*-

'''
 @package   glups
 @file      GridObject.py
 @brief     Definition of a Grid object
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section COPYRIGHT

 Copyright (C) 2015 EPFL (Ecole Polytechnique F??d??rale de Lausanne)
 LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of glups.
'''

from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *

from PyQt5 import QtCore, QtGui, QtOpenGL

from .GLObject import *
from .ParameterList import *


class GridObject(GLObject):

    def __init__(self, ID='Grid'):

        self.ID = ID
        self.ListID = None

        # init parameters
        self.param = ParametersList(self.ID)
        self.param["Color_r"] = 1.0
        self.param["Color_g"] = 0.0
        self.param["Color_b"] = 0.0
        self.param["Color_a"] = 1.0
        self.param["show"] = 1
        self.param["PosX"] = 0.0
        self.param["PosY"] = 0.0
        self.param["PosZ"] = 0.0
        self.param["SizeX"] = 10.0
        self.param["SizeY"] = 10.0
        self.param["SizeZ"] = 10.0
        self.param["RotX"] = 0.0
        self.param["RotY"] = 0.0
        self.param["RotZ"] = 0.0
        self.param["RotationFactor"] = 0.1
        self.param["SizeFactor"] = 0.1
        self.param["Number"] = 10
        self.GenList()

    def GenList(self):

        if self.ListID is None:
            self.ListID = glGenLists(1)
        glNewList(self.ListID, GL_COMPILE)

        n = self.param["Number"] + 1

        glScaled(2. / (n - 1), 2. / (n - 1), 2. / (n - 1))
        glTranslated(-(n - 1) / 2, -(n - 1) / 2, 0)

        glBegin(GL_LINES)

        for i in range(n):
            glVertex3f(i, 0, 0)
            glVertex3f(i, n - 1, 0)

            glVertex3f(0, i, 0)
            glVertex3f(n - 1, i, 0)

        glEnd()

        glEndList()

    def SetNumber(self, number):
        self.param["Number"] = int(number)
        self.GenList()

    def GetNumber(self):
        return self.param["Number"]
