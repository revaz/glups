# -*- coding: utf-8 -*-

'''
 @package   glups
 @file      ControllWidget.py
 @brief     Definition of the GL widget motions controller.
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section COPYRIGHT

 Copyright (C) 2015 EPFL (Ecole Polytechnique F??d??rale de Lausanne)
 LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of glups.
'''

from PyQt5 import QtCore, QtGui, QtWidgets
from numpy import *


class ControllWidget(QtWidgets.QWidget):

    def __init__(self, parent=None, argv=None):
        QtWidgets.QWidget.__init__(self, parent)

        self.parent = parent

        self.angle = 90.

        mainFrame = QtWidgets.QFrame()
        mainFrame.setFrameStyle(QtWidgets.QFrame.WinPanel | QtWidgets.QFrame.Sunken)

        self.InButton = QtWidgets.QPushButton("In")
        self.InButton.show()

        self.OutButton = QtWidgets.QPushButton("Out")
        self.OutButton.show()

        self.NButton = QtWidgets.QPushButton("N")
        self.NButton.show()

        self.SButton = QtWidgets.QPushButton("S")
        self.SButton.show()

        self.WButton = QtWidgets.QPushButton("W")
        self.WButton.show()

        self.EButton = QtWidgets.QPushButton("E")
        self.EButton.show()

        self.MButton = QtWidgets.QPushButton("M")
        self.MButton.show()

        self.PButton = QtWidgets.QPushButton("P")
        self.PButton.show()

        self.RButton = QtWidgets.QPushButton("*")
        self.RButton.show()

        self.XYButton = QtWidgets.QPushButton("XY")
        self.XYButton.show()

        self.XZButton = QtWidgets.QPushButton("XZ")
        self.XZButton.show()

        self.YZButton = QtWidgets.QPushButton("YZ")
        self.YZButton.show()

        self.ZXButton = QtWidgets.QPushButton("ZX")
        self.ZXButton.show()

        self.angleLabel = QtWidgets.QLabel(self.tr("angle :"))
        self.angleEntry = QtWidgets.QLineEdit()
        self.angleEntry.setText(self.tr("90"))
        self.angleEntry.show()

        self.AutoButton = QtWidgets.QPushButton("Auto Set Obs")
        self.AutoButton.show()

        self.ResetButton = QtWidgets.QPushButton("Reset Obs")
        self.ResetButton.show()

        gridLayout = QtWidgets.QGridLayout()

        gridLayout.addWidget(self.InButton, 0, 0, 1, 3)
        gridLayout.addWidget(self.OutButton, 1, 0, 1, 3)

        gridLayout.addWidget(self.EButton, 3, 0)
        gridLayout.addWidget(self.WButton, 3, 2)
        gridLayout.addWidget(self.NButton, 2, 1)
        gridLayout.addWidget(self.SButton, 4, 1)
        gridLayout.addWidget(self.RButton, 3, 1)

        gridLayout.addWidget(self.MButton, 2, 0)
        gridLayout.addWidget(self.PButton, 2, 2)

        gridLayout.addWidget(self.angleLabel, 5, 0, 1, 1)
        gridLayout.addWidget(self.angleEntry, 5, 1, 1, 2)

        gridLayout.addWidget(self.XYButton, 6, 0, 1, 1)
        gridLayout.addWidget(self.XZButton, 6, 1, 1, 1)
        gridLayout.addWidget(self.YZButton, 7, 0, 1, 1)
        gridLayout.addWidget(self.ZXButton, 7, 1, 1, 1)

        gridLayout.addWidget(self.AutoButton, 8, 0, 1, 3)
        gridLayout.addWidget(self.ResetButton, 9, 0, 1, 3)

        #gridLayout.addLayout(topLayout, 0, 1)
        #gridLayout.addLayout(leftLayout, 1, 0)
        #gridLayout.setColumnStretch(1, 10)
        self.setLayout(gridLayout)
        
        
        self.NButton.clicked.connect(self.RotateN)
        self.SButton.clicked.connect(self.RotateS)
        self.EButton.clicked.connect(self.RotateE)
        self.WButton.clicked.connect(self.RotateW)
        self.MButton.clicked.connect(self.RotateM)
        self.PButton.clicked.connect(self.RotateP)
        self.RButton.clicked.connect(self.RotateR)
        
        self.XYButton.clicked.connect(self.AlignXY)
        self.XZButton.clicked.connect(self.AlignXZ)
        self.YZButton.clicked.connect(self.AlignYZ)
        self.ZXButton.clicked.connect(self.AlignZX)
        
        self.angleEntry.textChanged[str].connect(self.setAngle)
         
        self.InButton.clicked.connect(self.In)
        self.OutButton.clicked.connect(self.Out)

        self.AutoButton.clicked.connect(self.AutoSet)
        self.ResetButton.clicked.connect(self.Reset)
                

    def AutoSet(self):
        self.parent.gl.AutoSetParameters()

        self.parent.gl.Observer.UpdateParameters()
        self.parent.gl.update()

    def Reset(self):
        self.parent.gl.ReadParameters()
        self.parent.gl.updateGL()

        # self.parent.gl.Observer.UpdateParameters()
        # self.parent.gl.update()

    def In(self):
        self.parent.gl.Observer.Translate_Obs_Pts(
            self.parent.gl.Observer.TranslationFactorZ * 500)

        self.parent.gl.Observer.UpdateParameters()
        self.parent.gl.update()

    def Out(self):
        self.parent.gl.Observer.Translate_Obs_Pts(
            self.parent.gl.Observer.TranslationFactorZ * -500)
        self.parent.gl.Observer.UpdateParameters()

        self.parent.gl.Observer.UpdateParameters()
        self.parent.gl.update()

    def RotateN(self):
        self.parent.gl.Observer.Rotate_Obs_xWin(self.angle)
        self.parent.gl.update()

    def RotateS(self):
        self.parent.gl.Observer.Rotate_Obs_xWin(-self.angle)
        self.parent.gl.update()

    def RotateE(self):
        self.parent.gl.Observer.Rotate_Obs_yWin(self.angle)

        self.parent.gl.Observer.UpdateParameters()
        self.parent.gl.update()

    def RotateW(self):
        self.parent.gl.Observer.Rotate_Obs_yWin(-self.angle)

        self.parent.gl.Observer.UpdateParameters()
        self.parent.gl.update()

    def RotateM(self):
        self.parent.gl.Observer.Rotate_Obs_zWin(self.angle)

        self.parent.gl.Observer.UpdateParameters()
        self.parent.gl.update()

    def RotateP(self):
        self.parent.gl.Observer.Rotate_Obs_zWin(-self.angle)

        self.parent.gl.Observer.UpdateParameters()
        self.parent.gl.update()

    def RotateR(self):
        self.parent.gl.Observer.SetIntP(0, 0, 0)
        # center IntP
        self.parent.gl.Observer.CenterIntP(
            self.parent.gl.Observer.P[0],
            self.parent.gl.Observer.P[1],
            self.parent.gl.Observer.P[2])
        # align xy
        self.parent.gl.Observer.AlignXY()

        self.parent.gl.Observer.UpdateParameters()
        self.parent.gl.update()

    def AlignXY(self):
        self.parent.gl.Observer.SetIntP(0, 0, 0)
        # center IntP
        self.parent.gl.Observer.CenterIntP(
            self.parent.gl.Observer.P[0],
            self.parent.gl.Observer.P[1],
            self.parent.gl.Observer.P[2])
        # align xy
        self.parent.gl.Observer.AlignXY()

        self.parent.gl.Observer.UpdateParameters()
        self.parent.gl.update()

    def AlignXZ(self):
        self.parent.gl.Observer.SetIntP(0, 0, 0)
        # center IntP
        self.parent.gl.Observer.CenterIntP(
            self.parent.gl.Observer.P[0],
            self.parent.gl.Observer.P[1],
            self.parent.gl.Observer.P[2])
        # align xy
        self.parent.gl.Observer.AlignXZ()

        self.parent.gl.Observer.UpdateParameters()
        self.parent.gl.update()

    def AlignYZ(self):
        self.parent.gl.Observer.SetIntP(0, 0, 0)
        # center IntP
        self.parent.gl.Observer.CenterIntP(
            self.parent.gl.Observer.P[0],
            self.parent.gl.Observer.P[1],
            self.parent.gl.Observer.P[2])
        # align xy
        self.parent.gl.Observer.AlignYZ()

        self.parent.gl.Observer.UpdateParameters()
        self.parent.gl.update()

    def AlignZX(self):
        self.parent.gl.Observer.SetIntP(0, 0, 0)
        # center IntP
        self.parent.gl.Observer.CenterIntP(
            self.parent.gl.Observer.P[0],
            self.parent.gl.Observer.P[1],
            self.parent.gl.Observer.P[2])
        # align xy
        self.parent.gl.Observer.AlignZX()

        self.parent.gl.Observer.UpdateParameters()
        self.parent.gl.update()

    def setAngle(self, entry):

        try:
            self.angle = float(entry)
        except BaseException:
            pass
