# -*- coding: utf-8 -*-

'''
 @package   glups
 @file      VectorObject.py
 @brief     Definition of a Vector object
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section COPYRIGHT

 Copyright (C) 2015 EPFL (Ecole Polytechnique F??d??rale de Lausanne)
 LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of glups.
'''

from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *

from PyQt5 import QtCore, QtGui, QtOpenGL

from .GLObject import *
from .ParameterList import *


class VectorObject(GLObject):

    def __init__(self, pos, ID='Axes'):

        self.ID = ID
        self.ListID = None
        self.pos = pos

        # init parameters
        self.param = ParametersList(self.ID)
        self.param["Color_r"] = 1.0
        self.param["Color_g"] = 1.0
        self.param["Color_b"] = 1.0
        self.param["Color_a"] = 1.0
        self.param["show"] = 1
        self.param["PosX"] = 0.0
        self.param["PosY"] = 0.0
        self.param["PosZ"] = 0.0
        self.param["SizeX"] = 1.0
        self.param["SizeY"] = 1.0
        self.param["SizeZ"] = 1.0
        self.param["RotX"] = 0.0
        self.param["RotY"] = 0.0
        self.param["RotZ"] = 0.0
        self.param["RotationFactor"] = 0.1
        self.param["SizeFactor"] = 0.1
        self.GenList()

    def GenList(self):

        if self.ListID is None:
            self.ListID = glGenLists(1)
        glNewList(self.ListID, GL_COMPILE)

        glBegin(GL_LINES)
        glVertex3f(self.pos[0], self.pos[1], self.pos[2])
        glVertex3f(self.pos[3], self.pos[4], self.pos[5])
        glEnd()

        glEndList()

    def AutoSetParameters(self, ModelSize, CenterOfMass):
        pass
