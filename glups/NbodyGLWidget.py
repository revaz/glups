# -*- coding: utf-8 -*-

'''
 @package   glups
 @file      NbodyGLWidget.py
 @brief     Definition of the NbodyGLWidget class (display objects using OpenGL).
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section COPYRIGHT

 Copyright (C) 2015 EPFL (Ecole Polytechnique F??d??rale de Lausanne)
 LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of glups.
'''

import sys
import math
import struct
import time
import shutil
import copy as pycopy
import numpy as np
from PyQt5 import QtCore, QtGui, QtOpenGL
from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
from PIL import Image
from scipy.optimize import bisect


from pNbody import *

from .ParameterList import *
from .GLObject import *
from .PointsObject import *
from .ObserverObject import *
from .AxesObject import *
from .GridObject import *
from .SphereObject import *
from .BoxObject import *
from .LineObject import *
from .ArrowObject import *
from .VectorObject import *
from .MireObject import *
from .PointerObject import *
from .TimerObject import *
from .DelaunayLinesObject import *
from .DelaunaySurfaceObject import *
from . import gtio
from . import geometry
from .Journey import Journey

MAX_POINTS_TYPE = 8
DefaultPointsColor = ones((MAX_POINTS_TYPE, 4), float)
DefaultPointsColor[0] = array([0, 1, 1, 1])
DefaultPointsColor[1] = array([0, 1, 0, 1])
DefaultPointsColor[2] = array([0, 0, 1, 1])
DefaultPointsColor[3] = array([1, 0, 0, 1])
DefaultPointsColor[4] = array([1, 1, 0, 1])
DefaultPointsColor[5] = array([1, 1, 1, 1])

STEREO_MODE_NONE = 0
STEREO_MODE_CROSSED = 1
STEREO_MODE_PARALLEL = 2
STEREO_MODE_RIGHT = 3
STEREO_MODE_LEFT = 4
STEREO_MODE_ANAGLYPH = 5
STEREO_MODE_3DTV = 6  # usable for 3d TV
STEREO_MAX_MODE = 7

NORMAL = 0
ROTATE_AROUND_PTS = 0
ROTATE_AROUND_EYE = 1
FLIGHT = 2
FLIGHT_AROUND_PTS = 3
TRANSLATE_PTS_FIXED = 4
TRANSLATE_EYE_FIXED = 5
TRANSLATE = 6
ROTATE_AROUND_EYE_HEAD = 7
ROTATE_AROUND_ONLY = 8

MOTION_MAX_MODE = 2

ParticleTypeDict = {1:"gas",2:"stars",3:"halo",4:"bndry",5:"sink",6:"disk",7:"bulge",8:"gasnc"}


def glPrint(x, y, line):
    '''
    x,y between 0,1
    '''

    x = (x - 0.5) * 20
    y = (y - 0.5) * 20

    glRasterPos2f(x, y)
    for l in line:
        glutBitmapCharacter(GLUT_BITMAP_9_BY_15, ord(l))




###############################################################################
class NbodyGLWidget(QtOpenGL.QGLWidget):
    def __init__(
            self,
            parent=None,
            argv=None,
            nb=None,
            ParameterFile=None,
            MarkersFile=None,
            VectorsFile=None,
            TrkFiles=None):
        self.parent = parent
        QtOpenGL.QGLWidget.__init__(self, parent)
        
   
        self.setFocusPolicy(QtCore.Qt.StrongFocus)

        self.Observer = ObserverObject(ID='Observer_0')

        # may not be invoqued if not fist time
        glutInit(argv)

        self.nb = nb
        self.object = 0
        self.xRot = 0
        self.yRot = 0
        self.zRot = 0

        self.lastPos = QtCore.QPoint()

        self.trolltechGreen = QtGui.QColor.fromCmykF(0.40, 0.0, 1.0, 0.0)
        self.trolltechPurple = QtGui.QColor.fromCmykF(0.39, 0.39, 0.0, 0.0)

        self.xWin = 512
        self.yWin = 512

        self.MouseX = 0.0
        self.MouseY = 0.0
        self.MouseZ = 0.0

        self.MouseScreenX = 0
        self.MouseScreenY = 0

        self.ActiveObject = 'Observer_0'

        self.AutoMotionTimerID = None
        self.AutoMotionDTime = 0.0
        self.AutoMotionOldTime = 0.0

        self.RecordTimerID = None
        self.IdleTimerID = None

        self.ScriptTimerID = None

        self.DownButton = None

        self.Message = None

        if ParameterFile is not None:
            self.ParameterFile = ParameterFile

        self.MarkersFile = MarkersFile
        self.VectorsFile = VectorsFile
        self.TrkFiles = TrkFiles

        self.RecordImages = False

        self.Journey = None
        

    def initializeGL(self):
        '''
        void MainWindow::Init()
        '''

        # init glut
        # glutInit('')

        # init the palette
        self.InitPalette()

        # create all GLObject
        self.InitObjects()

        # init marker
        # self.InitMarkers()

        # init vectors
        # self.InitVectors()

        # init trk
        self.InitTrk()

        # gl init
        glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE)
        glPixelStorei(GL_UNPACK_ALIGNMENT, 1)
        glClearColor(0.0, 0.0, 0.0, 0.0)
        glShadeModel(GL_FLAT)
        # glEnable(GL_DEPTH_TEST)
        # glEnable(GL_CULL_FACE)

        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
        glEnable(GL_BLEND)  	# needed for alpha

    def printText(self, x, font, text, r, g, b):
        blending = False
        if glIsEnabled(GL_BLEND):
            blending = True

        glColor3f(r, g, b)

        # shape: 4,4
        proj = glGetDoublev(GL_PROJECTION_MATRIX)
        # shape: 4,4
        model = glGetDoublev(GL_MODELVIEW_MATRIX)
        # shape: 4
        view = glGetIntegerv(GL_VIEWPORT)
        # shape: 2
        depth = glGetDoublev(GL_DEPTH_RANGE)

        screen = gluProject(x[0], x[1], x[2],
                            model, proj, view)

        self.renderText(x[0], x[1], x[2], text)

        if not blending:
            glDisable(GL_BLEND)

    def paintGL(self):
        '''
        void MainWindow::CallBackDisplayFunc(void)
        '''

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

        if self.Observer.DisplayInfo:
            # fixed object
            self.ProjectFixed()
            glViewport(0, 0, self.xWin, self.yWin)
            self.SetFixedObserverPos()
            self.DisplayInfo()

        if self.Message is not None:
            self.DisplayMessage()

        # set the mire
        self.SetMire()

        # set pointer to the right position
        self.SetPointer()

        if self.Observer.StereoMode == STEREO_MODE_NONE:

            self.Project()
            glViewport(0, 0, self.xWin, self.yWin)
            self.SetObserverPos()
            self.Display()

        elif self.Observer.StereoMode == STEREO_MODE_CROSSED:

            self.Project()
            glViewport(0, 0, self.xWin / 2, self.yWin)
            self.SetObserverPos('right')
            self.Display()

            self.Project()
            glViewport(self.xWin / 2, 0, self.xWin / 2, self.yWin)
            self.SetObserverPos('left')
            self.Display()

        elif self.Observer.StereoMode == STEREO_MODE_PARALLEL:

            self.Project()
            glViewport(0, 0, self.xWin / 2, self.yWin)
            self.SetObserverPos('left')
            self.Display()

            self.Project()
            glViewport(self.xWin / 2, 0, self.xWin / 2, self.yWin)
            self.SetObserverPos('right')
            self.Display()

        elif self.Observer.StereoMode == STEREO_MODE_3DTV:

            self.Project()
            glViewport(0, 0, self.xWin / 2, self.yWin)
            self.SetObserverPos('left')
            self.Display()

            self.Project()
            glViewport(self.xWin / 2, 0, self.xWin / 2, self.yWin)
            self.SetObserverPos('right')
            self.Display()

        elif self.Observer.StereoMode == STEREO_MODE_LEFT:

            self.Project()
            glViewport(0, 0, self.xWin, self.yWin)
            self.SetObserverPos('left')
            self.Display()

        elif self.Observer.StereoMode == STEREO_MODE_RIGHT:

            self.Project()
            glViewport(0, 0, self.xWin, self.yWin)
            self.SetObserverPos('right')
            self.Display()

        elif self.Observer.StereoMode == STEREO_MODE_ANAGLYPH:

            self.Project()
            glViewport(0, 0, self.xWin, self.yWin)
            self.SetObserverPos('right')
            self.Display(color=[0, 1, 1])

            self.Project()
            glViewport(0, 0, self.xWin, self.yWin)
            self.SetObserverPos('left')
            self.Display(color=[1, 0, 0])

        # plot rockstar ids
        self.DrawRockstar()

        # plot obs data name if required
        self.DrawObs()

    def ProjectFixed(self):
        '''
        void MainWindow::Project(double aspect)
        '''
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()

        PerspectiveLeft = -10
        PerspectiveRight = -PerspectiveLeft
        PerspectiveBottom = PerspectiveLeft
        PerspectiveTop = -PerspectiveBottom
        glOrtho(PerspectiveLeft, PerspectiveRight, PerspectiveBottom,
                PerspectiveTop, PerspectiveLeft, -PerspectiveLeft)

    def SetFixedObserverPos(self, eye=None):
        '''
        Set the observer position
        '''
        glMatrixMode(GL_MODELVIEW)
        glLoadIdentity()

    def Project(self):
        '''
        void MainWindow::Project(double aspect)
        '''
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()

        # frustum
        if self.Observer.ProjectionMode:
            aspect = self.Observer.PerspectiveAspect * \
                float(self.xWin) / float(self.yWin)
            gluPerspective(
                self.Observer.PerspectiveFov,
                aspect,
                self.Observer.PerspectiveNear,
                self.Observer.PerspectiveFar)
        # orthogonal
        else:
            aspect = 1 / self.Observer.PerspectiveAspect
            PerspectiveLeft = -5 * self.Observer.PerspectiveNear
            PerspectiveRight = -PerspectiveLeft
            PerspectiveBottom = PerspectiveLeft * \
                float(self.yWin) / float(self.xWin) * aspect
            PerspectiveTop = -PerspectiveBottom
            glOrtho(
                PerspectiveLeft,
                PerspectiveRight,
                PerspectiveBottom,
                PerspectiveTop,
                self.Observer.PerspectiveNear,
                self.Observer.PerspectiveFar)

    def SetObserverPos(self, eye=None):
        '''
        Set the observer position
        void MainWindow::SetObserverPos(int eyenumber)
        '''
        glMatrixMode(GL_MODELVIEW)
        glLoadIdentity()

        if eye is None:
            self.Observer.LookAt()
        elif eye == 'right':
            self.Observer.LookAtRight()
        elif eye == 'left':
            self.Observer.LookAtLeft()

    def Display(self, color=None):
        '''
        affiche toute les listes
        void MainWindow::Display(int mode)
        '''

        for ObjectKey in list(self.Objects.keys()):

            if ObjectKey == "Mire_1":
                continue

            object = self.Objects[ObjectKey].Display(color)

    def Update(self):
        self.updateGL()

    def update(self):
        self.updateGL()

    def resizeGL(self, width, height):
        """
            Resize the OpenGL window and adjust the viewport and projection matrix.

        Parameters:
            -----------
            width : int
            The new width of the OpenGL window.
            height : int
            The new height of the OpenGL window.
        """
        # Store the new dimensions
        self.xWin = width
        self.yWin = height

    def InitPalette(self, palette_name='rainbow4'):
        p = palette.Palette(palette_name)
        self.palette = transpose(array([p.r, p.g, p.b])).astype(float32)

    def InitObjects(self):

        self.Objects = {}

        # observer object
        self.Objects['Observer_0'] = self.Observer

        # points
        self.InitPoints()

        # init other objets, according to the parameter file
        self.InitObjectsFromParameterFile()

        # init the Mire
        self.InitMire()

        # init the Pointer
        self.InitPointer()

        # init timer object
        self.InitTimer()

    def InitPoints(self):

        # well, this should probably be written better

        # remove all Points and records old parameters
        params = {}
        hasPoints = 1
        while hasPoints:
            hasPoints = 0
            for ObjectKey in list(self.Objects.keys()):
                if ObjectKey[:6] == "Points":
                    params[self.Objects[ObjectKey].ID] = self.Objects[ObjectKey].param
                    del self.Objects[ObjectKey]

                    hasPoints = 1

        # set var, varmin, varmax

        if not self.nb.has_array("var"):
            self.nb.var = None

        if not self.nb.has_var("varmax"):
            self.nb.varmax = None

        if not self.nb.has_var("varmin"):
            self.nb.varmin = None

        if self.nb.var is not None:

            if self.nb.varmax is None:
                self.nb.varmax = max(self.nb.var)

            if self.nb.varmin is None:
                self.nb.varmin = min(self.nb.var)

        if not self.nb.has_var("ptsize"):
            self.nb.ptsize = None

        # add the new ones
        index = self.nb.getParticleMatchingDict()
        
        for i,key in enumerate(ParticleTypeDict):
            
            if ParticleTypeDict[key] in index:
            
              pointsobject = PointsObject(
                  self.nb.select(index[ParticleTypeDict[i+1]]),
                  ID="Points_%d" %
                  i,
                  var=self.nb.var,
                  mn=self.nb.varmin,
                  mx=self.nb.varmax,
                  pal=self.palette,
                  size=self.nb.ptsize)

              if pointsobject.ID in params:
                  pointsobject.param = params[pointsobject.ID]
              else:
                  pointsobject.SetColor(DefaultPointsColor[i])

              self.Objects[pointsobject.ID] = pointsobject

        del params

    def DrawRockstar(self):
        if self.parent.rockstar is None:
            return
        rgb = (1., 1., 1.)

        UL = 3.0857e24 / self.nb.UnitLength_in_cm

        ids = self.parent.rockstar.num.astype(int)
        pos = self.parent.rockstar.pos * UL
        N = len(ids)
        for i in range(N):
            self.parent.glWidget.printText(pos[i, :], GLUT_BITMAP_9_BY_15,
                                           str(ids[i]),
                                           *rgb)

    def DrawObs(self):
        data = self.parent.data
        if data is None:
            return
        rgb = (1., 1., 1.)

        x = data["x"]
        y = data["y"]
        z = data["z"]

        name = data["name"]

        N = len(x)
        for i in range(N):
            pos = np.array([x[i], y[i], z[i]])
            self.parent.glWidget.printText(pos, GLUT_BITMAP_9_BY_15,
                                           name[i],
                                           *rgb)

    def UpdateTraceParticle(self):
        if self.parent.trace is None:
            return

        idx = self.parent.ModelFiles.index(self.parent.CurrentFile)
        if idx == self.parent.trace[2]:
            return

        before = self.parent.trace[0]
        after = self.parent.trace[1]

        diff = idx - self.parent.trace[2]

        if diff > 0:
            if diff > after.shape[0]:
                return
            before = np.concatenate((before, after[:diff, :]), axis=0)
            after = after[diff:, :]
        else:
            if abs(diff) > before.shape[0]:
                return
            after = np.concatenate((before[diff:, :], after), axis=0)
            before = before[:diff, :]

        self.parent.trace = (before, after, idx)

    def InitTraceParticle(self):

        if self.parent.trace is None:
            return

        self.UpdateTraceParticle()

        count = 0
        before = self.parent.trace[0]
        after = self.parent.trace[1]

        if before.shape[0] > 0:
            i = before[0, :]
            for k in range(before.shape[0] - 1):
                count += 1
                j = before[k + 1, :]
                l = LineObject(i[0], i[1], i[2], j[0], j[1], j[2],
                               ID='Line_%i' % count)
                l.param["Color_r"] = 3. / 256.
                l.param["Color_g"] = 192. / 256.  # Dark Pastel Green
                l.param["Color_b"] = 60. / 256.
                self.Objects[l.ID] = l
                i = j

        if after.shape[0] > 0:
            if before.shape[0] > 0:
                i = before[-1, :]
            else:
                # "skip" first part
                i = after[0, :]
            for k in range(after.shape[0]):
                count += 1
                j = after[k, :]
                l = LineObject(i[0], i[1], i[2], j[0], j[1], j[2],
                               ID='Line_%i' % count)
                l.param["Color_r"] = 149. / 256.
                l.param["Color_g"] = 69. / 256.  # Chestnut
                l.param["Color_b"] = 53. / 256.
                self.Objects[l.ID] = l
                i = j

    def RemoveAllObjects(self):

        for ObjectKey in list(self.Objects.keys()):

            ObjectTpe = ObjectKey.split("_")[0]
            if ObjectTpe != 'Points' and ObjectTpe != 'Observer':
                del self.Objects[ObjectKey]

    #################
    # info functions
    #################

    def DisplayInfo(self):

        ########
        # top
        ########

        # between -10 and 10
        x = 0.02
        y = 0.975
        dy = -0.025

        glColor3f(1.0, 1.0, 1.0)

        glPrint(x, y, '%s' % (self.parent.CurrentFile))

        y = y + dy
        glPrint(x, y, 'Active object   : %s' % (self.ActiveObject))

        y = y + dy
        glPrint(x, y, 'Projection Mode : %s' % (self.Observer.ProjectionMode))

        y = y + dy
        glPrint(x, y, 'Stereo Mode     : %s' % (self.Observer.StereoMode))

        y = y + dy
        glPrint(x, y, 'Motion Mode     : %s' % (self.Observer.MotionMode))

        y = y + dy
        glPrint(
            x, y, 'Fov             : %5.1f' %
            (self.Observer.PerspectiveFov))

        y = y + dy
        glPrint(x, y, 'Near/Far planes : %5.1f %5.1f' %
                (self.Observer.PerspectiveNear, self.Observer.PerspectiveFar))

        y = y + dy
        glPrint(
            x, y, 'Near/Far factor : %6.3f %6.3f' %
            (self.Observer.PerspectiveNearFactor, self.Observer.PerspectiveFarFactor))

        ########
        # bottom
        ########

        x = 0.02
        y = 0.15
        glPrint(x, y, 'Mouse Position  : x=%5.1f y=%5.1f z=%5.1f ' %
                (self.MouseX, self.MouseY, self.MouseZ))

        y = y + dy
        glPrint(
            x, y, 'Mouse On screen : x=%5i y=%5i' %
            (-self.xWin / 2 + self.lastPos.x(), self.yWin / 2 - self.lastPos.y()))

        y = y + dy
        glPrint(x, y, 'Dist to IntP    : d=%7.3f' % (self.Observer.IntDist))

        y = y + dy
        glPrint(x, y, 'Observer pos    : x=%5.1f y=%5.1f z=%5.1f ' %
                (self.Observer.M[0], self.Observer.M[1], self.Observer.M[2]))

        y = y + dy
        glPrint(x, y, 'IntP     pos    : x=%5.1f y=%5.1f z=%5.1f ' %
                (self.Observer.P[0], self.Observer.P[1], self.Observer.P[2]))

    def SwitchDisplayInfo(self):
        if self.Observer.DisplayInfo:
            self.Observer.DisplayInfo = 0
        else:
            self.Observer.DisplayInfo = 1
        self.updateGL()

    def DisplayInfoOn(self):
        self.Observer.DisplayInfo = 1
        self.updateGL()

    def DisplayInfoOff(self):
        self.Observer.DisplayInfo = 0
        self.updateGL()

    #################
    # message functions
    #################

    def SetMessage(self, message):

        self.Message = message
        self.updateGL()

    def DisplayMessage(self):

        x = 0.5
        y = 0.9

        glPrint(x, y, self.Message)

    #################
    # mire
    #################

    def InitMire(self):

        name = "Mire_1"

        # here, we force a pointer object to exist
        if name not in self.Objects:
            self.Objects[name] = MireObject(ID=name)
            self.Objects[name].param['show'] = 0 	# hide it by default

    def SetMire(self):
        
        if "Mire_1" not in self.Objects:
          return
        
        if self.Objects["Mire_1"].param['show'] == 1:

            # fixed object
            self.ProjectFixed()
            glViewport(0, 0, self.xWin, self.yWin)
            self.SetFixedObserverPos()

            self.Objects["Mire_1"].Display(mode="static")

    def SwitchMire(self):
        self.Objects["Mire_1"].SwitchShow()
        self.Objects["Mire_1"].Display(mode="static")

    #################
    # Pointer
    #################

    def InitPointer(self):

        name = "Pointer_1"

        # here, we force a pointer object to exist
        if name not in self.Objects:
            self.Objects[name] = PointerObject(ID=name)
            self.Objects[name].param['show'] = 0  	# hide it by default

    def SetPointer(self):
      
        if "Pointer_1" not in self.Objects:
          return
      
        if self.Objects["Pointer_1"].param['show'] == 1:
            self.Objects["Pointer_1"].SetFromObserver(self.Observer)

    def SwitchPointer(self):
        self.Objects["Pointer_1"].SwitchShow()

    #################
    # Timer
    #################

    def InitTimer(self):

        name = "Timer_0"

        # here, we force a pointer object to exist
        if name not in self.Objects:
            self.Timer = TimerObject(ID=name)
            self.Objects[name] = self.Timer
        else:
            self.Timer = self.Objects[name]

    #################
    # projection mode
    #################

    def SetProjectionMode(self, mode="ortho"):

        if mode == "ortho":
            self.Observer.ProjectionMode = 0
        elif mode == "frustum":
            self.Observer.ProjectionMode = 1

        self.updateGL()

    def SwitchProjectionMode(self):

        if (self.Observer.ProjectionMode):
            self.Observer.ProjectionMode = 0
        else:
            self.Observer.ProjectionMode = 1

        self.updateGL()

    #################
    # stereo mode
    #################

    def SetStereoMode(self, mode=STEREO_MODE_NONE):

        if mode == "normal":
            self.Observer.StereoMode = STEREO_MODE_NONE
            self.Observer.PerspectiveAspect = 1

        elif mode == "crossed":
            self.Observer.StereoMode = STEREO_MODE_CROSSED
            self.Observer.PerspectiveAspect = 0.5

        elif mode == "parallel":
            self.Observer.StereoMode = STEREO_MODE_PARALLEL
            self.Observer.PerspectiveAspect = 0.5

        elif mode == "right":
            self.Observer.StereoMode = STEREO_MODE_RIGHT
            self.Observer.PerspectiveAspect = 1

        elif mode == "left":
            self.Observer.StereoMode = STEREO_MODE_LEFT
            self.Observer.PerspectiveAspect = 1

        elif mode == "anaglyph":
            self.Observer.StereoMode = STEREO_MODE_ANAGLYPH
            self.Observer.PerspectiveAspect = 1

        elif mode == "3dtv":
            self.Observer.StereoMode = STEREO_MODE_3DTV
            self.Observer.PerspectiveAspect = 1

        self.updateGL()

    #################
    # other functions
    #################

    def AutoSetParameters(self):

        # find model size and cm */
        CenterOfMass = self.nb.cm()
        ModelSize = 10 * sqrt(self.nb.size())

        # update every object
        self.Observer.AutoSetParameters(ModelSize, CenterOfMass)

    ######################
    # i/o parameters functions
    ######################

    def AddObjectsFromParameterFile(self, file):
        '''
        Add new objects from a parameter file
        Do not touch the old objects
        '''

        if os.path.exists(file):

            f = open(file, 'r')
            lines = f.readlines()
            f.close()

            ObjectsList = []
            NewObjects = {}
            for line in lines:
                line = line.split(":")[0]
                ObjectTpe = line.split("_")[0]
                ObjectNum = int(line.split("_")[1])

                if ObjectTpe != 'Points' and ObjectTpe != 'Observer':

                    ObjectName = "%s_%d" % (ObjectTpe, ObjectNum)
                    oldObjectName = ObjectName

                    try:
                        ObjectsList.index(ObjectName)
                    except ValueError:
                        ObjectsList.append(ObjectName)
                        ObjectName = self.GetObjectName(ObjectTpe)
                        # create the object
                        var = 'self.Objects[ObjectName]'
                        val = '%sObject(ID=ObjectName)' % (ObjectTpe)
                        exec("%s = %s" % (var, val))
                        NewObjects[ObjectName] = oldObjectName

            # now, read parameters only for the new objects
            f = open(file, 'r')
            for ObjectName in list(NewObjects.keys()):
                oldObjectName = NewObjects[ObjectName]
                self.Objects[ObjectName].ReadParameters(f, ID=oldObjectName)
            f.close()

            del NewObjects

    def InitObjectsFromParameterFile(self, file=None):
        '''
        Init all objects using a parameter file
        '''
        
        if file is not None:
            if os.path.exists(file):
                self.ParameterFile = file

        if os.path.exists(self.ParameterFile):

            f = open(self.ParameterFile, 'r')
            lines = f.readlines()
            f.close()

            ObjectsList = []
            for line in lines:
                line = line.split(":")[0]
                ObjectTpe = line.split("_")[0]
                ObjectNum = int(line.split("_")[1])

                if ObjectTpe != 'Points' and ObjectTpe != 'Observer':

                    ObjectName = "%s_%d" % (ObjectTpe, ObjectNum)

                    if ObjectsList.count(ObjectName) == 0:
                        ObjectsList.append(ObjectName)
                        # create the object
                        var = 'self.Objects[ObjectName]'
                        val = '%sObject(ID=ObjectName)' % (ObjectTpe)
                        # setattr('self',var,val)
                        exec("%s = %s" % (var, val))

            # now, read the parameters parameters	(including points and )
            self.ReadParameters()

    def WriteParameters(self, ParameterFile=None):

        if ParameterFile is None:
            ParameterFile = self.ParameterFile

        f = open(ParameterFile, 'w')

        for ObjectKey in list(self.Objects.keys()):
            self.Objects[ObjectKey].WriteParameters(f)

        f.close()

    def ReadParameters(self, ParameterFile=None):
      
        if ParameterFile is None:
            ParameterFile = self.ParameterFile

        if os.path.exists(ParameterFile):
          
            f = open(ParameterFile, 'r')
            for ObjectKey in list(self.Objects.keys()):
                self.Objects[ObjectKey].ReadParameters(f)
            f.close()
            
        else:
            print(("warning : %s : no such file" % ParameterFile))

    ######################
    # image functions
    ######################

    def SaveImage(self, image_name='image.gif'):

        self.updateGL()

        glReadBuffer(GL_FRONT)
        image = glReadPixels(
            0,
            0,
            self.xWin,
            self.yWin,
            GL_RGBA,
            GL_UNSIGNED_BYTE)

        mat = fromstring(image, uint8)
        mat.shape = (self.xWin, self.yWin, 4)

        mat_r = mat[:, :, 0]
        mat_g = mat[:, :, 1]
        mat_b = mat[:, :, 2]

        r = mat_r.tostring()
        g = mat_g.tostring()
        b = mat_b.tostring()

        size = (self.xWin, self.yWin)
        image_r = Image.frombytes("L", size, r)
        image_g = Image.frombytes("L", size, g)
        image_b = Image.frombytes("L", size, b)

        # merge image
        img = Image.merge("RGB", (image_r, image_g, image_b))
        img = img.transpose(Image.FLIP_TOP_BOTTOM)
        img.save(image_name)

    ######################
    # selection functions
    ######################

    def GetGLMousePosition(self, x, y):

        LastViewport = glGetIntegerv(GL_VIEWPORT)
        LastMvmatrix = glGetDoublev(GL_MODELVIEW_MATRIX)
        LastProjmatrix = glGetDoublev(GL_PROJECTION_MATRIX)

        realy = LastViewport[3] - y - 1

        #wx0, wy0, wz0 = gluUnProject (float(x),float(realy),1.0, LastMvmatrix, LastProjmatrix, LastViewport);
        #wx1, wy1, wz1 = gluUnProject (float(x),float(realy),0.0, LastMvmatrix, LastProjmatrix, LastViewport);
        wx, wy, wz = gluUnProject(float(x), float(
            realy), 1, LastMvmatrix, LastProjmatrix, LastViewport)

        return wx, wy, wz

    def PickObject(self, x, y):
        LastViewport = glGetIntegerv(GL_VIEWPORT)
        LastMvmatrix = glGetDoublev(GL_MODELVIEW_MATRIX)
        LastProjmatrix = glGetDoublev(GL_PROJECTION_MATRIX)

        realy = LastViewport[3] - y - 1

        wx0, wy0, wz0 = gluUnProject(float(x), float(
            realy), 1.0, LastMvmatrix, LastProjmatrix, LastViewport)
        wx1, wy1, wz1 = gluUnProject(float(x), float(
            realy), 0.0, LastMvmatrix, LastProjmatrix, LastViewport)


        noline = 1
        for ObjectKey in list(self.Objects.keys()):
            if ObjectKey == "Line1":
                noline = 0

        if (noline):

            self.Objects["Line1"] = LineObject(
                wx0, wy0, wz0, wx1, wy1, wz1, ID="Line1")

            self.Ptx0 = float(wx0)
            self.Pty0 = float(wy0)
            self.Ptz0 = float(wz0)
            self.Ptx1 = float(wx1)
            self.Pty1 = float(wy1)
            self.Ptz1 = float(wz1)

        else:

            self.Ptx0p = float(wx0)
            self.Pty0p = float(wy0)
            self.Ptz0p = float(wz0)
            self.Ptx1p = float(wx1)
            self.Pty1p = float(wy1)
            self.Ptz1p = float(wz1)
            xc, yc, zc = self.NeareastPointFromTwoLines(
                self.Ptx0, self.Pty0, self.Ptz0, self.Ptx1, self.Pty1, self.Ptz1, self.Ptx0p, self.Pty0p, self.Ptz0p, self.Ptx1p, self.Pty1p, self.Ptz1p)

            self.Observer.P[0] = xc
            self.Observer.P[1] = yc
            self.Observer.P[2] = zc

            del self.Objects["Line1"]

    ##################################################################
    #
    #
    #                       EVENTS
    #
    #
    ##################################################################

    def timerEvent(self, event):

            # stop automotion (from Observer)
        if self.Observer.StopAutoMotion:
            self.StopAutoMotion()
            self.Observer.AutoMotionMode = NORMAL

        ################
        # script
        ################

        # run a sript
        if event.timerId() == self.ScriptTimerID:
            if self.Observer.AutoMotionMode != MOTIONS:
                if self.parent.script_lines is not None:
                    self.parent.ExecScript()

        ################
        # auto motion
        ################
        if event.timerId() == self.AutoMotionTimerID:
            tnow = time.time()
            self.AutoMotionDTime = tnow - self.AutoMotionOldTime
            self.AutoMotionOldTime = tnow

            # natural motion
            self.Observer.CallBackAutoMotion(
                self.AutoMotionDTime, self.Journey)
            # motion due to the mouse position
            self.Observer.CallBackMouseFunc(
                -self.xWin / 2 + self.lastPos.x(),
                self.yWin / 2 - self.lastPos.y())

            self.Observer.CallBackDownButton(self.DownButton)

            self.updateGL()

            # save image
            if self.RecordImages:
                self.RecordImage()

        ################
        # record
        ################
        elif event.timerId() == self.RecordTimerID:
            self.RecordPosition()

        ################
        # idle
        ################
        elif event.timerId() == self.IdleTimerID:
            pass

    def resizeEvent(self, event):
        # Account for DPI scaling (for macOS Retina displays)
        dpi_scale = self.devicePixelRatioF()  # Works for Qt 5.6+
        width = int(event.size().width()*dpi_scale)
        height = int(event.size().height()*dpi_scale)

        # Update the size
        self.xWin = width
        self.yWin = height

    def mousePressEvent(self, event):
        self.lastPos = QtCore.QPoint(event.pos())
        self.DownButton = event.button()

    def mouseReleaseEvent(self, event):
        self.DownButton = None

    def mouseMoveEvent(self, event):
        # Account for DPI scaling (for macOS Retina displays)
        dpi_scale = self.devicePixelRatioF()  # Works for Qt 5.6+
        event_x = event.x()*dpi_scale
        event_y = event.y()*dpi_scale
        
        dx = event_x - self.lastPos.x()*dpi_scale
        dy = event_y - self.lastPos.y()*dpi_scale

        lastDownButton = None
        if event.buttons() & QtCore.Qt.LeftButton:
            lastDownButton = GLUT_LEFT_BUTTON
        elif event.buttons() & QtCore.Qt.RightButton:
            lastDownButton = GLUT_RIGHT_BUTTON
        elif event.buttons() & QtCore.Qt.MidButton:
            lastDownButton = GLUT_MIDDLE_BUTTON

        if lastDownButton is not None:
            self.Objects[self.ActiveObject].CallBackMotionFunc(
                dx, dy, lastDownButton)
            self.updateGL()

        self.lastPos = QtCore.QPoint(event.pos())

    def mouseDoubleClickEvent(self, event):
        # Account for DPI scaling (for macOS Retina displays)
        dpi_scale = self.devicePixelRatioF()  # Works for Qt 5.6+
        event_x = event.x()*dpi_scale
        event_y = event.y()*dpi_scale

        if event.buttons() & QtCore.Qt.LeftButton:
            self.PickObject(event_x, event_y)
            self.updateGL()

        elif event.buttons() & QtCore.Qt.RightButton:
            # self.Observer.CenterIntP()
            # self.updateGL()
            self.parent.update()

        elif event.buttons() & QtCore.Qt.MidButton:
            self.MouseX, self.MouseY, self.MouseZ = self.GetGLMousePosition(
                event_x, event_y)
            self.updateGL()

        else:
            self.updateGL()

    def keyPressEvent(self, event):

        key = event.key()

        # Ctr + key
        #

        if int(event.modifiers()) == (QtCore.Qt.ControlModifier):

            if (key == QtCore.Qt.Key_Home):  # increase near plane factor (frustum mode)
                self.Observer.PerspectiveNearFactor = self.Observer.PerspectiveNearFactor * \
                    (2**(1 / 100.))			# 100 push = fact 2
                self.Observer.UpdateParameters()
                self.updateGL()

            elif (key == QtCore.Qt.Key_End):  # decrease near plane factor (frustum mode)
                self.Observer.PerspectiveNearFactor = self.Observer.PerspectiveNearFactor / \
                    (2**(1 / 100.))
                self.Observer.UpdateParameters()
                self.updateGL()

            elif (key == QtCore.Qt.Key_PageUp):  # increase far plane factor (frustum mode) ?
                self.Observer.PerspectiveFarFactor = self.Observer.PerspectiveFarFactor * \
                    (2**(1 / 100.))
                self.Observer.UpdateParameters()
                self.updateGL()

            elif (key == QtCore.Qt.Key_PageDown):  # decrease far plane factor (frustum mode) ?
                self.Observer.PerspectiveFarFactor = self.Observer.PerspectiveFarFactor / \
                    (2**(1 / 100.))
                self.Observer.UpdateParameters()
                self.updateGL()


        #
        # single key
        #

        elif (key == QtCore.Qt.Key_D):		# move to map display mode
            self.parent.Display(updateFromGL=True)
            self.parent.TabWidget.setCurrentIndex(1)

        elif (key == QtCore.Qt.Key_N):		# open and display next file
            idx = self.parent.ModelFiles.index(
                self.parent.CurrentFile) + self.parent.skip
            while(idx >= len(self.parent.ModelFiles)):
                idx -= len(self.parent.ModelFiles)

            self.parent.CurrentFile = self.parent.ModelFiles[idx]
            self.parent.LoadFile(self.parent.CurrentFile)
            # self.parent.infobarDockWidgetContents.reload(self.parent.nb)

        elif (key == QtCore.Qt.Key_B):		# open and display previous file
            idx = self.parent.ModelFiles.index(
                self.parent.CurrentFile) - self.parent.skip
            while(idx < 0):
                idx += len(self.parent.ModelFiles)

            self.parent.CurrentFile = self.parent.ModelFiles[idx]
            self.parent.LoadFile(self.parent.CurrentFile)
            # self.parent.infobarDockWidgetContents.reload(self.parent.nb)

        elif key == QtCore.Qt.Key_Q:		# quit glups
            sys.exit()

        elif key == QtCore.Qt.Key_Escape: 	# quit glups
            sys.exit()

        elif key == QtCore.Qt.Key_O: 		# auto set parameter
            self.AutoSetParameters()
            self.updateGL()

        elif key == QtCore.Qt.Key_C: 		# center the point of interest
            self.Objects[self.ActiveObject].CenterIntP(
                self.Observer.P[0], self.Observer.P[1], self.Observer.P[2])
            self.updateGL()

        elif key == QtCore.Qt.Key_I: 		# display/hide informations
            self.SwitchDisplayInfo()

        elif key == QtCore.Qt.Key_Backspace: 	# make observer object active
            if self.ActiveObject != 'Observer_0':
                del self.Objects[self.ActiveObject]
                self.ActiveObject = 'Observer_0'
                self.updateGL()

        elif key == QtCore.Qt.Key_PageUp:		# increase alpha (transparency)
            if self.ActiveObject == 'Observer_0':
                for ObjectKey in list(self.Objects.keys()):
                    if self.Objects[ObjectKey].ID[:6] == 'Points':
                        self.Objects[ObjectKey].DivAlpha(0.9)
            else:
                self.Objects[self.ActiveObject].DivAlpha(0.9)

            self.updateGL()

        elif key == QtCore.Qt.Key_PageDown:  # decrease alpha (transparency)
            if self.ActiveObject == 'Observer_0':
                for ObjectKey in list(self.Objects.keys()):
                    if self.Objects[ObjectKey].ID[:6] == 'Points':
                        self.Objects[ObjectKey].MulAlpha(0.9)
            else:
                self.Objects[self.ActiveObject].MulAlpha(0.9)

            self.updateGL()

        # =Dot        # toggle between orthogonal (proj. mode=0) and frustum (proj. mode=1) projection
        elif key == QtCore.Qt.Key_Period:
            self.SwitchProjectionMode()

        elif key == QtCore.Qt.Key_Insert:                 # decrease stereo factor
            self.Observer.StereoFactor *= 0.95
            self.Observer.UpdateParameters()
            self.updateGL()

        elif key == QtCore.Qt.Key_Delete:                 # increase stereo factor
            self.Observer.StereoFactor /= 0.95
            self.Observer.UpdateParameters()
            self.updateGL()

        elif key == QtCore.Qt.Key_Home:                   # increase field of view
            self.Observer.PerspectiveFov *= 0.9
            self.Observer.UpdateParameters()
            self.updateGL()

        elif key == QtCore.Qt.Key_End:                    # decrease field of view
            self.Observer.PerspectiveFov /= 0.9
            self.Observer.UpdateParameters()
            self.updateGL()

        elif key == QtCore.Qt.Key_S: 		# switch stereo mode
            self.Observer.StereoMode = self.Observer.StereoMode + 1
            if self.Observer.StereoMode > STEREO_MAX_MODE - 1:
                self.Observer.StereoMode = STEREO_MODE_NONE

            if self.Observer.StereoMode == STEREO_MODE_CROSSED:
                self.Observer.PerspectiveAspect = 0.5
            elif self.Observer.StereoMode == STEREO_MODE_PARALLEL:
                self.Observer.PerspectiveAspect = 0.5
            else:
                self.Observer.PerspectiveAspect = 1

            self.updateGL()

        elif key == QtCore.Qt.Key_F:              # Particle trajectory
            self.parent.TraceParticle()

        # elif key==QtCore.Qt.Key_R: 		# disabeled
        #
        #    # set IntP to 0,0,0
        #    self.Observer.SetIntP(0,0,0)
        #    # center IntP
        #    self.Observer.CenterIntP(self.Observer.P[0],self.Observer.P[1],self.Observer.P[2])
        #
        #    # align xy
        #    self.Observer.AlignXY()
        #
        #    self.updateGL()

        ###############################
        # mark object
        ###############################

        #
        #  !!! here, we should add a marker mode
        #

            '''
            elif key==QtCore.Qt.Key_M:		# disabeled

	      # mark the point with an arrow
	      self.MarkIntPWithArrow()
	      self.updateGL()


            elif key==QtCore.Qt.Key_R:		# disabeled

	      # remove last marker
	      self.RemoveLastMarker()
	      self.updateGL()

            elif key==QtCore.Qt.Key_P:		# disabeled
	      # print markers
	      self.PrintMarkers()


            elif key==QtCore.Qt.Key_W:		# disabeled
	      # write markers
	      self.WriteMarkers()

	    '''

        elif key == QtCore.Qt.Key_M:		# toggle  the mire on/off
            self.SwitchMire()
            self.updateGL()

        elif key == QtCore.Qt.Key_P:		# toggle  the pointer on/off
            self.SwitchPointer()
            self.updateGL()

    ###############################
    # switch between active objects
    ###############################

        elif key == QtCore.Qt.Key_Space:		# switch between active objects

            ObjectKey = sorted(self.Objects.keys())

            i = ObjectKey.index(self.ActiveObject)

            while True:
                i = i + 1
                if i == len(ObjectKey):
                    i = 0

                if ObjectKey[i][:6] != 'Points':		# do not keep points
                    break

            self.ActiveObject = ObjectKey[i]

            if self.Objects[self.ActiveObject].ID != self.ActiveObject:
                print(("there is a bug here !!!",
                       self.Objects[self.ActiveObject].ID,
                       self.ActiveObject))
                sys.exit()

            self.updateGL()

        ###############################
        # display or hide particles
        ###############################

        elif key == QtCore.Qt.Key_1: 		# display/hide particle of type 0
            #if self.nb.get_ntype() > 0:
            if "Points_0" in self.Objects:
              self.Objects["Points_0"].SwitchShow()
              self.updateGL()

        elif key == QtCore.Qt.Key_2: 		# display/hide particle of type 1
            #if self.nb.get_ntype() > 1:
            if "Points_1" in self.Objects:
              self.Objects["Points_1"].SwitchShow()
              self.updateGL()

        elif key == QtCore.Qt.Key_3: 		# display/hide particle of type 2
            #if self.nb.get_ntype() > 2:
            if "Points_2" in self.Objects:
              self.Objects["Points_2"].SwitchShow()
              self.updateGL()

        elif key == QtCore.Qt.Key_4: 		# display/hide particle of type 3
            #if self.nb.get_ntype() > 3:
            if "Points_3" in self.Objects:
              self.Objects["Points_3"].SwitchShow()
              self.updateGL()

        elif key == QtCore.Qt.Key_5: 		# display/hide particle of type 4
            #if self.nb.get_ntype() > 4:
            if "Points_4" in self.Objects:
              self.Objects["Points_4"].SwitchShow()
              self.updateGL()

        elif key == QtCore.Qt.Key_6: 		# display/hide particle of type 5
            #if self.nb.get_ntype() > 5:
            if "Points_5" in self.Objects:
              self.Objects["Points_5"].SwitchShow()
              self.updateGL()

        elif key == QtCore.Qt.Key_7: 		# display/hide particle of type 6
            #if self.nb.get_ntype() > 6:
            if "Points_6" in self.Objects:
              self.Objects["Points_6"].SwitchShow()
              self.updateGL()

        elif key == QtCore.Qt.Key_8: 		# display/hide particle of type 7
            #if self.nb.get_ntype() > 7:
            if "Points_7" in self.Objects:
              self.Objects["Points_7"].SwitchShow()
              self.updateGL()

    ###############################
        # io functions
        ###############################

        elif key == QtCore.Qt.Key_F1: 		# start/stop recording
            if self.RecordTimerID is not None:
                self.StopRecordTrack()
            else:
                self.StartRecordTrack()

        # elif key==QtCore.Qt.Key_F2:
        #    self.StopRecordTrack()

        # elif key==QtCore.Qt.Key_F3:
        #    self.RecordPosition()

        elif key == QtCore.Qt.Key_F7: 		# read parameter file and update the display
            self.ReadParameters()
            self.updateGL()

        elif key == QtCore.Qt.Key_F8:		# write parameter file
            self.WriteParameters()

        elif key == QtCore.Qt.Key_F9: 		# toggle auto-motion on/off

            if self.AutoMotionTimerID is not None:
                self.StopAutoMotion()
            else:
                self.StartAutoMotion()

        elif key == QtCore.Qt.Key_F12:    # stop recording images

            if self.RecordImages:
                self.StopRecordImages()
            else:
                self.StartRecordImages()

        else:

            self.Objects[self.ActiveObject].CallBackKeyboardFunc(event)
            self.updateGL()

    def wheelEvent(self, event):
      
        self.Objects[self.ActiveObject].CallBackWheel(event.angleDelta().y())
        self.updateGL()

    ##################################################################
    #
    #
    #                       other functions
    #
    #
    ##################################################################

    def P(self):
        center = self.Observer.P
        return center[:-1]
        
    def M(self):
        M = pycopy.deepcopy(self.Observer.M)
        M.shape=(4,4)
        return M[:,:-1]

    def SetP(self, pos):
        self.Observer.SetIntP(x=pos[0], y=pos[1], z=pos[2])
        self.Observer.UpdateParameters()
        self.updateGL()

    def SetEye(self, pos):
        self.Observer.SetEye(x=pos[0], y=pos[1], z=pos[2])
        self.Observer.UpdateParameters()
        self.updateGL()

    def GetObjectName(self, name):
        '''
        find an apropriate name
        '''
        i = 0
        for key in list(self.Objects.keys()):
            nam = key.split('_')[0]
            num = key.split('_')[1]

            if nam == name:
                i = max(int(num), i)

        return "%s_%i" % (name, i + 1)

    ###############################
    # motion mode functions
    ###############################

    def SetMode(self, mode='NORMAL'):

        if mode == 'NORMAL':
            self.Observer.MotionMode = ROTATE_AROUND_PTS
            self.Observer.AutoMotionMode = ROTATE_AROUND_PTS

        if mode == 'ROTATION':
            self.Observer.MotionMode = ROTATE_AROUND_PTS
            self.Observer.AutoMotionMode = ROTATE_AROUND_EYE_HEAD

        if mode == 'ROTATION_ONLY':
            self.Observer.MotionMode = ROTATE_AROUND_PTS
            self.Observer.AutoMotionMode = ROTATE_AROUND_ONLY

        elif mode == 'FLIGHT':
            self.Observer.MotionMode = FLIGHT
            self.Observer.AutoMotionMode = FLIGHT

        if mode == 'MOTIONS':
            self.Observer.MotionMode = ROTATE_AROUND_PTS
            self.Observer.AutoMotionMode = MOTIONS

        self.Observer.InitMode()

    ###############################
    # scripts
    ###############################

    def StartScript(self):
        self.ScriptTimerID = self.startTimer(10)

    def StopScript(self):
        self.ScriptTimerID = None

    ###############################
    # auto motion functions
    ###############################

    def StartAutoMotion(self):

        self.AutoMotionTimerID = self.startTimer(0)
        self.AutoMotionOldTime = time.time()
        self.setMouseTracking(True)

        # this let the observer stop the automotion
        self.Observer.StopAutoMotion = False

    def StopAutoMotion(self):

        if self.AutoMotionTimerID is not None:
            self.killTimer(self.AutoMotionTimerID)
            self.AutoMotionTimerID = None
            self.setMouseTracking(False)

            self.Observer.StopAutoMotion = True

    ###############################
    # motions
    ###############################

    def Sleep(self, duration=2):

        # define journey parameters
        self.Journey = Journey(duration=duration, tstart=time.time())
        self.Journey.SetMMotion(None)
        self.Journey.SetPMotion(None)
        self.Journey.SetPstart(self.Observer.P)
        self.Journey.SetMstart(self.Observer.M)
        self.Journey.ComputeMotionParameters()

        # switch to MOTIONS mode + AutoMotion
        self.SetMode('MOTIONS')
        self.StartAutoMotion()

        print("start sleeping")

    def Translate(self, targetP=None, targetM=None, duration=2):
        """
        Translate both P and M in order to move P on targetP
        """

        if targetP is not None:
            self.Observer.targetP = targetP

        if self.Observer.targetP is not None:

            if len(self.Observer.targetP) == 3:
                tP = self.Observer.targetP
                self.Observer.targetP = array([tP[0], tP[1], tP[2], 1])

            if targetM is None:
                DD = ones(3)
                DD[0] = self.Observer.targetP[0] - self.Observer.P[0]
                DD[1] = self.Observer.targetP[1] - self.Observer.P[1]
                DD[2] = self.Observer.targetP[2] - self.Observer.P[2]

                targetM = ones(4)
                targetM[0] = self.Observer.M[0] + DD[0]
                targetM[1] = self.Observer.M[1] + DD[1]
                targetM[2] = self.Observer.M[2] + DD[2]

            # define journey parameters
            self.Journey = Journey(duration=duration, tstart=time.time())
            self.Journey.SetMMotion("Translate")
            self.Journey.SetPMotion("Translate")
            self.Journey.SetPstart(self.Observer.P)
            self.Journey.SetMstart(self.Observer.M)
            self.Journey.SetPend(self.Observer.targetP)
            self.Journey.SetMend(targetM)
            self.Journey.ComputeMotionParameters()

            # switch to MOTIONS mode + AutoMotion
            self.SetMode('MOTIONS')
            self.StartAutoMotion()

            print("start translate")

    def CenterP(self, targetP=None, duration=2):
        """
        Translate P in order to move P on targetP
        M remains unchanged
        """

        if targetP is not None:
            self.Observer.targetP = targetP

        if self.Observer.targetP is not None:

            if len(self.Observer.targetP) == 3:
                tP = self.Observer.targetP
                self.Observer.targetP = array([tP[0], tP[1], tP[2], 1])

            # define journey parameters
            self.Journey = Journey(duration=duration, tstart=time.time())
            self.Journey.SetMMotion(None)
            self.Journey.SetPMotion("Translate")
            self.Journey.SetPstart(self.Observer.P)
            self.Journey.SetMstart(self.Observer.M)
            self.Journey.SetPend(self.Observer.targetP)
            self.Journey.ComputeMotionParameters()

            # switch to MOTIONS mode + AutoMotion
            self.SetMode('MOTIONS')
            self.StartAutoMotion()

            print("start translate")

    def RotateAround(self, angle=0, duration=2, axis='z'):
        """
        Rotate M
        """
        # define journey parameters
        self.Journey = Journey(duration=duration, tstart=time.time())
        self.Journey.SetMMotion("Rotate")
        self.Journey.SetPMotion(None)
        self.Journey.SetPstart(self.Observer.P)
        self.Journey.SetMstart(self.Observer.M)
        self.Journey.SetAngle(angle)
        self.Journey.SetAxis(axis)
        self.Journey.SetRotationPoint(self.Observer.P)
        self.Journey.ComputeMotionParameters()

        # switch to MOTIONS mode + AutoMotion
        self.SetMode('MOTIONS')
        self.StartAutoMotion()

        print("start rotation")

    def RotateTo(self, angle=0, distance=None, duration=2, axis='z'):
        """
        Rotate M and move it up to a distance distance
        """

        ax = ones(3, float)
        point = ones(3, float)

        ax[0] = self.Observer.M[PTS + 0] - self.Observer.M[EYE + 0]
        ax[1] = self.Observer.M[PTS + 1] - self.Observer.M[EYE + 1]
        ax[2] = self.Observer.M[PTS + 2] - self.Observer.M[EYE + 2]

        norm = sqrt(ax[0]**2 + ax[1]**2 + ax[2]**2)
        norm = (self.Observer.IntDist - distance) / norm

        if np.fabs(norm) < 1e-5:
            return

        point[0] = self.Observer.M[EYE + 0] + ax[0] * norm
        point[1] = self.Observer.M[EYE + 1] + ax[1] * norm
        point[2] = self.Observer.M[EYE + 2] + ax[2] * norm

        Mend = array([point[0], point[1], point[2], 1])

        # define journey parameters
        self.Journey = Journey(duration=duration, tstart=time.time())
        self.Journey.SetMMotion("RotateTranslate")
        self.Journey.SetPMotion(None)
        self.Journey.SetPstart(self.Observer.P)
        self.Journey.SetMstart(self.Observer.M)
        self.Journey.SetAngle(angle)
        self.Journey.SetAxis(axis)
        self.Journey.SetMend(Mend)
        self.Journey.SetDirection(sign(norm))
        self.Journey.SetRotationPoint(self.Observer.P)
        self.Journey.ComputeMotionParameters()

        # switch to MOTIONS mode + AutoMotion
        self.SetMode('MOTIONS')
        self.StartAutoMotion()

        print("start rotation to")

    def LookAt(self, targetP=None, duration=2):
        """
        Rotate M to center the targetP with along the line of sight (M[EYE]->M[PTS])
        At the same time, translate P in order to move P on targetP
        """

        if targetP is not None:
            self.Observer.targetP = targetP

        if self.Observer.targetP is not None:

            if len(self.Observer.targetP) == 3:
                tP = self.Observer.targetP
                self.Observer.targetP = array([tP[0], tP[1], tP[2], 1])

        if self.Observer.targetP is not None:
            # compute the angle between the two points

            # the following lines are copy/paste from Observer.CenterIntP !!!

            v1 = ones(3, float)
            v2 = ones(3, float)
            axis = ones(3, float)
            point = ones(3, float)

            v1[0] = self.Observer.M[PTS + 0] - self.Observer.M[EYE + 0]
            v1[1] = self.Observer.M[PTS + 1] - self.Observer.M[EYE + 1]
            v1[2] = self.Observer.M[PTS + 2] - self.Observer.M[EYE + 2]

            v2[0] = targetP[0] - self.Observer.M[EYE + 0]
            v2[1] = targetP[1] - self.Observer.M[EYE + 1]
            v2[2] = targetP[2] - self.Observer.M[EYE + 2]

            n1 = sqrt(v1[0] * v1[0] + v1[1] * v1[1] + v1[2] * v1[2])
            n2 = sqrt(v2[0] * v2[0] + v2[1] * v2[1] + v2[2] * v2[2])

            # rotation axis
            axis[0] = v1[1] * v2[2] - v1[2] * v2[1]
            axis[1] = v1[2] * v2[0] - v1[0] * v2[2]
            axis[2] = v1[0] * v2[1] - v1[1] * v2[0]

            norm = axis[0]**2 + axis[1]**2 + axis[2]**2

            if norm != 0:

                # rotation point
                point[0] = self.Observer.M[EYE + 0]
                point[1] = self.Observer.M[EYE + 1]
                point[2] = self.Observer.M[EYE + 2]

                # rotation angle
                prod = (v1[0] * v2[0] + v1[1] * v2[1] +
                        v1[2] * v2[2]) / (n1 * n2)
                if prod > -1 and prod < 1:
                    angle = arccos(prod)
                    angle = angle * 180 / pi

                # define journey parameters
                self.Journey = Journey(duration=duration, tstart=time.time())
                self.Journey.SetMMotion("Rotate")
                self.Journey.SetPMotion("Translate")
                self.Journey.SetPstart(self.Observer.P)
                self.Journey.SetMstart(self.Observer.M)
                self.Journey.SetPend(self.Observer.targetP)
                self.Journey.SetAngle(angle)
                self.Journey.SetRotationPoint(self.Observer.M)
                self.Journey.SetAxis(axis)
                self.Journey.ComputeMotionParameters()

                # switch to MOTIONS mode + AutoMotion
                self.SetMode('MOTIONS')
                self.StartAutoMotion()

                print("start center")

    def SlideTo(self, distance=None, duration=2):
        """
        Translate M such as to have a distance dist between EYE and P
        """

        if distance is not None:

            axis = ones(3, float)
            point = ones(3, float)

            axis[0] = self.Observer.M[PTS + 0] - self.Observer.M[EYE + 0]
            axis[1] = self.Observer.M[PTS + 1] - self.Observer.M[EYE + 1]
            axis[2] = self.Observer.M[PTS + 2] - self.Observer.M[EYE + 2]

            norm = sqrt(axis[0]**2 + axis[1]**2 + axis[2]**2)
            norm = (self.Observer.IntDist - distance) / norm

            if np.fabs(norm) < 1e-5:
                return

            point[0] = self.Observer.M[EYE + 0] + axis[0] * norm
            point[1] = self.Observer.M[EYE + 1] + axis[1] * norm
            point[2] = self.Observer.M[EYE + 2] + axis[2] * norm

            Mend = array([point[0], point[1], point[2], 1])

            # define journey parameters
            self.Journey = Journey(duration=duration, tstart=time.time())
            self.Journey.SetMMotion("Translate")
            self.Journey.SetPMotion(None)
            self.Journey.SetPstart(self.Observer.P)
            self.Journey.SetMstart(self.Observer.M)
            self.Journey.SetMend(Mend)
            self.Journey.ComputeMotionParameters()

            # switch to MOTIONS mode + AutoMotion
            self.SetMode('MOTIONS')
            self.StartAutoMotion()

            print("start slide to")

    def SettargetP(self, pos=None):

        if pos is None:
            pos = self.P()

        self.Observer.targetP = array([pos[0], pos[1], pos[2], 1])

    ###############################
    # record stuffs
    ###############################

    def StartRecordTrack(self, dt=1000, directory="tracks"):
        self.RecordTimerID = self.startTimer(dt)
        self.InitFilmParameters(directory)
        self.RecordPosition()

    def StopRecordTrack(self):
        if self.RecordTimerID is not None:
            self.RecordPosition()
            self.killTimer(self.RecordTimerID)
        self.RecordTimerID = None
        self.SetMessage("stop recording")

    def RecordPosition(self):

        ModelSize = 10 * sqrt(self.nb.size())

        # add a marker
        name = self.GetObjectName('Sphere')
        self.Objects[name] = SphereObject(ID=name)
        self.Objects[name].SetSize(ModelSize / 100.)
        self.Objects[name].SetPos(
            self.Observer.M[0],
            self.Observer.M[1],
            self.Observer.M[2])
        # print name,self.Observer.M[0],self.Observer.M[1],self.Observer.M[2]

        # update timer
        self.Timer.SetTime()

        # save parameters
        fle = os.path.join(
            self.RecordTrackDirectory,
            "%06d.trk" %
            self.FilmParametersID)
        self.WriteParameters(fle)
        self.FilmParametersID = self.FilmParametersID + 1
        self.SetMessage("writing %s" % (fle))

    ###############################
    # lines functions
    ###############################

    def DrawLine(self, wx0, wy0, wz0, wx1, wy1, wz1):

        ModelLine = glGenLists(1)
        glNewList(ModelLine, GL_COMPILE)

        glPointSize(2.0)

        glBegin(GL_LINES)
        glVertex3f(wx0, wy0, wz0)
        glVertex3f(wx1, wy1, wz1)
        glEnd()

        glPointSize(1.0)

        glEndList()

        return ModelLine

    def NeareastPointFromTwoLines(
            self,
            x0,
            y0,
            z0,
            x1,
            y1,
            z1,
            x0p,
            y0p,
            z0p,
            x1p,
            y1p,
            z1p):

        Dx = x0p - x0
        Dy = y0p - y0
        Dz = z0p - z0

        dx = x1 - x0
        dy = y1 - y0
        dz = z1 - z0

        dxp = x1p - x0p
        dyp = y1p - y0p
        dzp = z1p - z0p

        dr2 = dx * dx + dy * dy + dz * dz
        dr2p = dxp * dxp + dyp * dyp + dzp * dzp

        Dr2 = Dx * dx + Dy * dy + Dz * dz
        Dr2p = Dx * dxp + Dy * dyp + Dz * dzp

        drr2 = dx * dxp + dy * dyp + dz * dzp

        kp = ((Dr2 / dr2) - (Dr2p / drr2)) / ((dr2p / drr2) - (drr2 / dr2))

        k = ((Dr2p / dr2p) - (Dr2 / drr2)) / ((drr2 / dr2p) - (dr2 / drr2))

        x0 = x0 + k * dx
        y0 = y0 + k * dy
        z0 = z0 + k * dz

        x1 = x0p + kp * dxp
        y1 = y0p + kp * dyp
        z1 = z0p + kp * dzp

        # mid point

        x = (x0 + x1) / 2.
        y = (y0 + y1) / 2.
        z = (z0 + z1) / 2.

        return x, y, z

    def getViewParameters(self):
        self.Observer.TranslateParametersBack()
        return self.Observer.param

    ###############################
    # Object function
    ###############################

    def AddNewAxes(self, name='Axes'):
        name = self.GetObjectName(name)
        self.Objects[name] = AxesObject(ID=name)
        self.update()

    def AddNewGrid(self, name='Grid'):
        name = self.GetObjectName(name)
        self.Objects[name] = GridObject(ID=name)
        self.update()

    def AddNewSphere(self, name='Sphere'):
        name = self.GetObjectName(name)
        self.Objects[name] = SphereObject(ID=name)
        self.update()

    def AddNewBox(self, name='Box'):
        name = self.GetObjectName(name)
        self.Objects[name] = BoxObject(ID=name)
        self.update()

    ###############################
    # marker function
    ###############################

    def MarkIntPWithArrow(self):
        # get IntP
        x = self.Observer.P[0]
        y = self.Observer.P[1]
        z = self.Observer.P[2]

        self.AddNewMarker(x, y, z)

    def InitMarkers(self):
        self.CurrentMarker = 0
        self.OutputMarkersFile = 'marker.dat'

        if self.MarkersFile is not None:
            f = open(self.MarkersFile, 'r')
            lines = f.readlines()
            f.close()

            for line in lines:
                line = string.split(line)
                x = float(line[0])
                y = float(line[1])
                z = float(line[2])
                self.AddNewMarker(x, y, z)

    def PrintMarkers(self):
        for ObjectKey in list(self.Objects.keys()):
            if ObjectKey[:6] == "Marker":
                Object = self.Objects[ObjectKey]
                print((Object.ID,
                       Object.param["PosX"],
                       Object.param["PosY"],
                       Object.param["PosZ"]))

    def WriteMarkers(self):

        f = open(self.OutputMarkersFile, 'w')

        for ObjectKey in list(self.Objects.keys()):
            if ObjectKey[:6] == "Marker":
                Object = self.Objects[ObjectKey]
                line = "%21.10f %21.10f %21.10f %s\n" % (
                    Object.param["PosX"], Object.param["PosY"], Object.param["PosZ"], Object.ID)
                f.write(line)

        f.close()

    def AddNewMarker(self, x, y, z):

        Marker = ArrowObject(ID="Marker%d" % (self.CurrentMarker))
        self.CurrentMarker = self.CurrentMarker + 1
        Marker.CenterIntP(x, y, z)
        self.Objects[Marker.ID] = Marker

    def RemoveLastMarker(self):

        ID = "Marker%d" % (self.CurrentMarker - 1)

        for ObjectKey in list(self.Objects.keys()):
            Object = self.Objects[ObjectKey]
            if Object.ID == ID:
                del Object
                self.CurrentMarker = self.CurrentMarker - 1
                return

    ###############################
    # vectors function
    ###############################

    def InitVectors(self):

        self.CurrentVector = 0

        if self.VectorsFile is not None:
            f = open(self.VectorsFile, 'r')
            lines = f.readlines()
            f.close()

            for line in lines:
                line = string.split(line)
                x1 = float(line[0])
                y1 = float(line[1])
                z1 = float(line[2])
                x2 = float(line[3])
                y2 = float(line[4])
                z2 = float(line[5])
                self.AddNewVector(x1, y1, z1, x2, y2, z2)

    def AddNewVector(self, x1, y1, z1, x2, y2, z2):

        pos = array([x1, y1, z1, x2, y2, z2])
        Vector = VectorObject(pos, ID="Vectors%d" % (self.CurrentVector))
        self.CurrentVector = self.CurrentVector + 1
        self.Objects[Vector.ID] = Vector

    ###############################
    # trk function
    ###############################

    def InitTrk(self):

        ModelSize = 10 * sqrt(self.nb.size())

        if self.TrkFiles is not None:

            self.TrkFiles = glob.glob(self.TrkFiles)
            self.TrkFiles.sort()

            for file in self.TrkFiles:
                elts = gtio.read_track(file)
                x = elts['Observer_0:M0']
                y = elts['Observer_0:M1']
                z = elts['Observer_0:M2']

                name = self.GetObjectName('Sphere')
                self.Objects[name] = SphereObject(ID=name)
                self.Objects[name].SetSize(ModelSize / 500.)
                self.Objects[name].SetPos(x, y, z)

                elts = gtio.read_track(file)
                x = elts['Observer_0:M4']
                y = elts['Observer_0:M5']
                z = elts['Observer_0:M6']

                name = self.GetObjectName('Sphere')
                self.Objects[name] = SphereObject(ID=name)
                self.Objects[name].SetSize(ModelSize / 100.)
                self.Objects[name].SetPos(x, y, z)

    ###############################
    # delaunay function
    ###############################

    def AddNewDelaunayLines(self, DelaunayFile, name="DelaunayLines"):
        name = self.GetObjectName(name)
        self.Objects[name] = DelaunayLinesObject(
            self.nb.pos, DelaunayFile=DelaunayFile, ID=name)
        self.update()

    def AddNewDelaunaySurface(self, DelaunayFile, name="DelaunaySurface"):
        name = self.GetObjectName(name)
        self.Objects[name] = DelaunaySurfaceObject(
            self.nb.pos,
            self.nb.var,
            level=self.nb.level,
            DelaunayFile=DelaunayFile,
            ID=name)
        self.update()

    ###############################
    # films/track function
    ###############################

    def InitFilmParameters(self, directory):
        self.FilmParametersID = 0
        self.RecordTrackDirectory = directory

        if os.path.isdir(directory):
            shutil.rmtree(directory)
            self.SetMessage("removing directory %s." % directory)

        self.SetMessage("creating directory %s." % directory)
        os.makedirs(directory)

    def ResetFilmParameters(self):
        self.FilmParametersID = 0

    ###############################
    # recording images functions
    ###############################

    def StartRecordImages(self, directory="pngs"):
        if not self.RecordImages:
            self.InitImagesRecording(directory)

    def StopRecordImages(self):
        self.RecordImages = False

    def InitImagesRecording(self, directory):
        self.RecordImages = True
        self.ImagesDir = directory
        self.ImageNum = 0

        if os.path.isdir(self.ImagesDir):
            shutil.rmtree(self.ImagesDir)
            self.SetMessage("removing directory %s" % self.ImagesDir)

        self.SetMessage("creating directory %s" % self.ImagesDir)
        os.makedirs(self.ImagesDir)

    def RecordImage(self):

        self.ImageNum = self.ImageNum + 1
        name = os.path.join(self.ImagesDir, "%08d.png" % self.ImageNum)
        self.SaveImage(name)

        print(("saving %s" % name))
