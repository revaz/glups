# -*- coding: utf-8 -*-

'''
 @package   glups
 @file      ObserverObject.py
 @brief     Definition of the Observer object
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section COPYRIGHT

 Copyright (C) 2015 EPFL (Ecole Polytechnique F??d??rale de Lausanne)
 LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of glups.
'''

from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *

from .GLObject import *
from .ParameterList import *

from numpy import *

from PyQt5 import QtCore, QtGui, QtOpenGL

from . import geometry
import time


EYE = 0
PTS = 4
HEA = 8
ARM = 12

ROTATE_AROUND_PTS = 0
ROTATE_AROUND_EYE = 1
FLIGHT = 2
FLIGHT_AROUND_PTS = 3
TRANSLATE_PTS_FIXED = 4
TRANSLATE_EYE_FIXED = 5
TRANSLATE = 6
ROTATE_AROUND_EYE_HEAD = 7
ROTATE_AROUND_ONLY = 8
MOTIONS = 9

MOTION_MAX_MODE = 2


class ObserverObject:
    def __init__(self, ID='Observer'):

        self.ID = ID
        self.ListID = None

        IntDist = 1000.		# a changer...

        # default parameters
        self.param = ParametersList(self.ID)
        self.param["M0"] = -1650.734275
        self.param["M1"] = 612.091797
        self.param["M2"] = 40784.338785
        self.param["M3"] = 1.000000
        self.param["M4"] = -1650.734275
        self.param["M5"] = 612.091797
        self.param["M6"] = 181.588173
        self.param["M7"] = 1.000000
        self.param["M8"] = -1650.734275
        self.param["M9"] = 41214.842409
        self.param["M10"] = 40784.338785
        self.param["M11"] = 1.000000
        self.param["M12"] = 38952.016337
        self.param["M13"] = 612.091797
        self.param["M14"] = 40784.338785
        self.param["M15"] = 1.000000
        self.param["P0"] = 0.0
        self.param["P1"] = 0.0
        self.param["P2"] = 0.0
        self.param["P3"] = 1.000000

        self.param["StereoFactor"] = 40.000000
        self.param["EyeDist"] = 499.211692
        self.param["Eye"] = 0.0
        self.param["PerspectiveFov"] = 50.000000
        self.param["PerspectiveAspect"] = 1.0  # 1.409736

        self.param["PerspectiveNearFactor"] = 0.1
        self.param["PerspectiveFarFactor"] = 10.
        self.param["PerspectiveNear"] = IntDist * \
            self.param["PerspectiveNearFactor"]
        self.param["PerspectiveFar"] = IntDist * \
            self.param["PerspectiveFarFactor"]

        self.param["ProjectionMode"] = 1
        # 0=disabeled, 1=crossed, 2=parallel, 3=right, 4=left
        self.param["StereoMode"] = 0
        self.param["DisplayInfo"] = 1
        self.param["MotionMode"] = 0				# 0=normal
        self.param["AutoMotionMode"] = 0
        self.param["MotionSpeed"] = 100

        self.TranslateParameters()

        # fixed parameters
        self.RotationFactor = 0.1
        self.RotationEyeFactor = 1
        self.TranslationCte = 100
        self.TranslationFactorXY = 0.025
        self.TranslationFactorZ = 0.025

        self.MotionMode = 0
        self.AutoMotionMode = 0
        self.StopAutoMotion = False
        self.IntDist = sqrt(pow(self.P[0] - self.M[EYE + 0],
                                2) + pow(self.P[1] - self.M[EYE + 1],
                                         2) + pow(self.P[2] - self.M[EYE + 2],
                                                  2))

        # a initializer correctement...
        self.TranslationFactorXY = 0 + self.IntDist * 0.001
        self.TranslationFactorZ = 0 + self.IntDist * 0.001

        # others
        self.targetP = None

        self.UpdateParameters()

    def Display(self, color=None):
        pass

    def TranslateParameters(self):

        self.M = ones(16, float)
        self.MR = ones(16, float)
        self.ML = ones(16, float)
        self.M[0] = self.param["M0"]
        self.M[1] = self.param["M1"]
        self.M[2] = self.param["M2"]
        self.M[3] = self.param["M3"]
        self.M[4] = self.param["M4"]
        self.M[5] = self.param["M5"]
        self.M[6] = self.param["M6"]
        self.M[7] = self.param["M7"]
        self.M[8] = self.param["M8"]
        self.M[9] = self.param["M9"]
        self.M[10] = self.param["M10"]
        self.M[11] = self.param["M11"]
        self.M[12] = self.param["M12"]
        self.M[13] = self.param["M13"]
        self.M[14] = self.param["M14"]
        self.M[15] = self.param["M15"]

        self.P = ones(4, float)
        self.P[0] = self.param["P0"]
        self.P[1] = self.param["P1"]
        self.P[2] = self.param["P2"]
        self.P[3] = self.param["P3"]

        self.PerspectiveFov = self.param["PerspectiveFov"]
        self.PerspectiveAspect = self.param["PerspectiveAspect"]
        self.PerspectiveNear = self.param["PerspectiveNear"]
        self.PerspectiveFar = self.param["PerspectiveFar"]
        self.PerspectiveNearFactor = self.param["PerspectiveNearFactor"]
        self.PerspectiveFarFactor = self.param["PerspectiveFarFactor"]

        self.StereoFactor = self.param["StereoFactor"]
        self.EyeDist = self.param["EyeDist"]
        self.ProjectionMode = self.param["ProjectionMode"]
        self.StereoMode = self.param["StereoMode"]
        self.DisplayInfo = self.param["DisplayInfo"]
        self.MotionMode = self.param["MotionMode"]
        self.MotionMode = self.param["AutoMotionMode"]
        self.MotionSpeed = self.param["MotionSpeed"]

    def TranslateParametersBack(self):

        self.param["M0"] = self.M[0]
        self.param["M1"] = self.M[1]
        self.param["M2"] = self.M[2]
        self.param["M3"] = self.M[3]
        self.param["M4"] = self.M[4]
        self.param["M5"] = self.M[5]
        self.param["M6"] = self.M[6]
        self.param["M7"] = self.M[7]
        self.param["M8"] = self.M[8]
        self.param["M9"] = self.M[9]
        self.param["M10"] = self.M[10]
        self.param["M11"] = self.M[11]
        self.param["M12"] = self.M[12]
        self.param["M13"] = self.M[13]
        self.param["M14"] = self.M[14]
        self.param["M15"] = self.M[15]

        self.param["P0"] = self.P[0]
        self.param["P1"] = self.P[1]
        self.param["P2"] = self.P[2]
        self.param["P3"] = self.P[3]

        self.param["PerspectiveFov"] = self.PerspectiveFov
        self.param["PerspectiveAspect"] = self.PerspectiveAspect
        self.param["PerspectiveNear"] = self.PerspectiveNear
        self.param["PerspectiveFar"] = self.PerspectiveFar
        self.param["PerspectiveNearFactor"] = self.PerspectiveNearFactor
        self.param["PerspectiveFarFactor"] = self.PerspectiveFarFactor

        self.param["StereoFactor"] = self.StereoFactor
        self.param["EyeDist"] = self.EyeDist
        self.param["ProjectionMode"] = self.ProjectionMode
        self.param["StereoMode"] = self.StereoMode
        self.param["DisplayInfo"] = self.DisplayInfo
        self.param["MotionMode"] = self.MotionMode
        self.param["AutoMotionMode"] = self.AutoMotionMode
        self.param["MotionSpeed"] = self.MotionSpeed

    def Info(self):
        print(("M0 =%g" % self.M[0]))
        print(("M1 =%g" % self.M[1]))
        print(("M2 =%g" % self.M[2]))
        print(("M3 =%g" % self.M[3]))
        print(("M4 =%g" % self.M[4]))
        print(("M5 =%g" % self.M[5]))
        print(("M6 =%g" % self.M[6]))
        print(("M7 =%g" % self.M[7]))
        print(("M8 =%g" % self.M[8]))
        print(("M9 =%g" % self.M[9]))
        print(("M10=%g" % self.M[10]))
        print(("M11=%g" % self.M[11]))
        print(("M12=%g" % self.M[12]))
        print(("M13=%g" % self.M[13]))
        print(("M14=%g" % self.M[14]))
        print(("M15=%g" % self.M[15]))
        print(("P0 =%g" % self.P[0]))
        print(("P1 =%g" % self.P[1]))
        print(("P2 =%g" % self.P[2]))
        print(("P3 =%g" % self.P[3]))

    def WriteParameters(self, file):
        self.TranslateParametersBack()
        self.param.Write(file)

    def ReadParameters(self, file):
        self.param.Read(file)
        self.TranslateParameters()
        self.UpdateParameters()

    def LookAt(self):
        gluLookAt(self.M[EYE + 0],
                  self.M[EYE + 1],
                  self.M[EYE + 2],
                  self.M[PTS + 0],
                  self.M[PTS + 1],
                  self.M[PTS + 2],
                  self.M[HEA + 0] - self.M[EYE + 0],
                  self.M[HEA + 1] - self.M[EYE + 1],
                  self.M[HEA + 2] - self.M[EYE + 2])

    def LookAtRight(self):
        gluLookAt(self.MR[EYE + 0],
                  self.MR[EYE + 1],
                  self.MR[EYE + 2],
                  self.MR[PTS + 0],
                  self.MR[PTS + 1],
                  self.MR[PTS + 2],
                  self.MR[HEA + 0] - self.MR[EYE + 0],
                  self.MR[HEA + 1] - self.MR[EYE + 1],
                  self.MR[HEA + 2] - self.MR[EYE + 2])

    def LookAtLeft(self):
        gluLookAt(self.ML[EYE + 0],
                  self.ML[EYE + 1],
                  self.ML[EYE + 2],
                  self.ML[PTS + 0],
                  self.ML[PTS + 1],
                  self.ML[PTS + 2],
                  self.ML[HEA + 0] - self.ML[EYE + 0],
                  self.ML[HEA + 1] - self.ML[EYE + 1],
                  self.ML[HEA + 2] - self.ML[EYE + 2])

    def ComputeEyes(self):

        axis = ones(4, float)
        axis[0] = self.M[ARM + 0] - self.M[EYE + 0]
        axis[1] = self.M[ARM + 1] - self.M[EYE + 1]
        axis[2] = self.M[ARM + 2] - self.M[EYE + 2]

        r = sqrt(axis[0] * axis[0] + axis[1] * axis[1] + axis[2] * axis[2])

        if axis.all() != 0:

            axis[0] = axis[0] / r
            axis[1] = axis[1] / r
            axis[2] = axis[2] / r

            for i in range(4):					  # this must be changed

                self.MR[EYE + i] = self.M[EYE + i] + axis[i] * self.EyeDist
                self.MR[PTS + i] = self.M[PTS + i] + axis[i] * self.EyeDist
                self.MR[HEA + i] = self.M[HEA + i] + axis[i] * self.EyeDist
                self.MR[ARM + i] = self.M[ARM + i] + axis[i] * self.EyeDist

                self.ML[EYE + i] = self.M[EYE + i] - axis[i] * self.EyeDist
                self.ML[PTS + i] = self.M[PTS + i] - axis[i] * self.EyeDist
                self.ML[HEA + i] = self.M[HEA + i] - axis[i] * self.EyeDist
                self.ML[ARM + i] = self.M[ARM + i] - axis[i] * self.EyeDist

    def SetEye(self, x, y, z):

        dx = x - self.M[EYE + 0]
        dy = y - self.M[EYE + 1]
        dz = z - self.M[EYE + 2]

        self.TranslateM(dx, dy, dz)

        self.UpdateParameters()

    def CenterIntP(self, x=None, y=None, z=None):
        '''
        Warning, x,y,z are not used (for compatibility only)
        '''

        v1 = ones(3, float)
        v2 = ones(3, float)
        axis = ones(3, float)
        point = ones(3, float)

        v1[0] = self.M[PTS + 0] - self.M[EYE + 0]
        v1[1] = self.M[PTS + 1] - self.M[EYE + 1]
        v1[2] = self.M[PTS + 2] - self.M[EYE + 2]

        v2[0] = self.P[0] - self.M[EYE + 0]
        v2[1] = self.P[1] - self.M[EYE + 1]
        v2[2] = self.P[2] - self.M[EYE + 2]

        n1 = sqrt(v1[0] * v1[0] + v1[1] * v1[1] + v1[2] * v1[2])
        n2 = sqrt(v2[0] * v2[0] + v2[1] * v2[1] + v2[2] * v2[2])

        # rotation axis
        axis[0] = v1[1] * v2[2] - v1[2] * v2[1]
        axis[1] = v1[2] * v2[0] - v1[0] * v2[2]
        axis[2] = v1[0] * v2[1] - v1[1] * v2[0]

        norm = axis[0]**2 + axis[1]**2 + axis[2]**2

        if norm != 0:

            # rotation point
            point[0] = self.M[EYE + 0]
            point[1] = self.M[EYE + 1]
            point[2] = self.M[EYE + 2]

            # rotation angle
            prod = (v1[0] * v2[0] + v1[1] * v2[1] + v1[2] * v2[2]) / (n1 * n2)
            if prod > -1 and prod < 1:
                angle = arccos(prod)
                angle = angle * 180 / pi

                self.M = self.RotateAround(angle, axis, point, self.M)

            self.UpdateParameters()

    def SetIntP(self, x=0, y=0, z=0):
        '''
        '''

        self.P[0] = x
        self.P[1] = y
        self.P[2] = z
        self.P[3] = 1
        self.UpdateParameters()

    def AutoSetParameters(self, modelsize, CenterOfMass):

        self.P[0] = CenterOfMass[0]
        self.P[1] = CenterOfMass[1]
        self.P[2] = CenterOfMass[2]
        self.P[3] = 1

        # dist from
        dist = modelsize

        self.M[EYE + 0] = self.P[0]
        self.M[EYE + 1] = self.P[1]
        self.M[EYE + 2] = self.P[2] + 3 * dist
        self.M[EYE + 3] = 1.0

        self.M[PTS + 0] = self.M[EYE + 0] + 0.0
        self.M[PTS + 1] = self.M[EYE + 1] + 0.0
        self.M[PTS + 2] = self.M[EYE + 2] - dist
        self.M[PTS + 3] = 1.0

        self.M[HEA + 0] = self.M[EYE + 0] + 0.0
        self.M[HEA + 1] = self.M[EYE + 1] + dist
        self.M[HEA + 2] = self.M[EYE + 2] + 0.0
        self.M[HEA + 3] = 1.0

        self.M[ARM + 0] = self.M[EYE + 0] + dist
        self.M[ARM + 1] = self.M[EYE + 1] + 0.0
        self.M[ARM + 2] = self.M[EYE + 2] + 0.0
        self.M[ARM + 3] = 1.0

        self.UpdateParameters()

    def PutIntPinZero(self):
        self.P[0] = 0
        self.P[1] = 0
        self.P[2] = 0

    def AlignXY(self):

        # center IntP
        self.CenterIntP()

        # dist from
        dist = sqrt(pow(self.M[EYE + 0] - self.P[0],
                        2) + pow(self.M[EYE + 1] - self.P[1],
                                 2) + pow(self.M[EYE + 2] - self.P[2],
                                          2))

        self.M[EYE + 0] = self.P[0]
        self.M[EYE + 1] = self.P[1]
        self.M[EYE + 2] = self.P[2] + dist
        self.M[EYE + 3] = 1.0

        self.M[PTS + 0] = self.M[EYE + 0] + 0.0
        self.M[PTS + 1] = self.M[EYE + 1] + 0.0
        self.M[PTS + 2] = self.M[EYE + 2] - dist
        self.M[PTS + 3] = 1.0

        self.M[HEA + 0] = self.M[EYE + 0] + 0.0
        self.M[HEA + 1] = self.M[EYE + 1] + dist
        self.M[HEA + 2] = self.M[EYE + 2] + 0.0
        self.M[HEA + 3] = 1.0

        self.M[ARM + 0] = self.M[EYE + 0] + dist
        self.M[ARM + 1] = self.M[EYE + 1] + 0.0
        self.M[ARM + 2] = self.M[EYE + 2] + 0.0
        self.M[ARM + 3] = 1.0

    def AlignXZ(self):

        # center IntP
        self.CenterIntP()

        # dist from
        dist = sqrt(pow(self.M[EYE + 0] - self.P[0],
                        2) + pow(self.M[EYE + 1] - self.P[1],
                                 2) + pow(self.M[EYE + 2] - self.P[2],
                                          2))

        self.M[EYE + 0] = self.P[0]
        self.M[EYE + 1] = self.P[1] - dist
        self.M[EYE + 2] = self.P[2]
        self.M[EYE + 3] = 1.0

        self.M[PTS + 0] = self.M[EYE + 0] + 0.0
        self.M[PTS + 1] = self.M[EYE + 1] + dist
        self.M[PTS + 2] = self.M[EYE + 2] + 0.0
        self.M[PTS + 3] = 1.0

        self.M[HEA + 0] = self.M[EYE + 0] + 0.0
        self.M[HEA + 1] = self.M[EYE + 1] + 0.0
        self.M[HEA + 2] = self.M[EYE + 2] + dist
        self.M[HEA + 3] = 1.0

        self.M[ARM + 0] = self.M[EYE + 0] + dist
        self.M[ARM + 1] = self.M[EYE + 1] + 0.0
        self.M[ARM + 2] = self.M[EYE + 2] + 0.0
        self.M[ARM + 3] = 1.0

    def AlignYZ(self):

        # center IntP
        self.CenterIntP()

        # dist from
        dist = sqrt(pow(self.M[EYE + 0] - self.P[0],
                        2) + pow(self.M[EYE + 1] - self.P[1],
                                 2) + pow(self.M[EYE + 2] - self.P[2],
                                          2))

        self.M[EYE + 0] = self.P[0] + dist
        self.M[EYE + 1] = self.P[1]
        self.M[EYE + 2] = self.P[2]
        self.M[EYE + 3] = 1.0

        self.M[PTS + 0] = self.M[EYE + 0] - dist
        self.M[PTS + 1] = self.M[EYE + 1] + 0.0
        self.M[PTS + 2] = self.M[EYE + 2] + 0.0
        self.M[PTS + 3] = 1.0

        self.M[HEA + 0] = self.M[EYE + 0] + 0.0
        self.M[HEA + 1] = self.M[EYE + 1] + 0.0
        self.M[HEA + 2] = self.M[EYE + 2] + dist
        self.M[HEA + 3] = 1.0

        self.M[ARM + 0] = self.M[EYE + 0] + 0.0
        self.M[ARM + 1] = self.M[EYE + 1] + dist
        self.M[ARM + 2] = self.M[EYE + 2] + 0.0
        self.M[ARM + 3] = 1.0

        self.UpdateParameters()

    def AlignZX(self):

        # center IntP
        self.CenterIntP()

        # dist from
        dist = sqrt(pow(self.M[EYE + 0] - self.P[0],
                        2) + pow(self.M[EYE + 1] - self.P[1],
                                 2) + pow(self.M[EYE + 2] - self.P[2],
                                          2))

        self.M[EYE + 0] = self.P[0]
        self.M[EYE + 1] = self.P[1] + dist
        self.M[EYE + 2] = self.P[2]
        self.M[EYE + 3] = 1.0

        self.M[PTS + 0] = self.M[EYE + 0] + 0.0
        self.M[PTS + 1] = self.M[EYE + 1] - dist
        self.M[PTS + 2] = self.M[EYE + 2] + 0.0
        self.M[PTS + 3] = 1.0

        self.M[HEA + 0] = self.M[EYE + 0] + dist
        self.M[HEA + 1] = self.M[EYE + 1] + 0.0
        self.M[HEA + 2] = self.M[EYE + 2] + 0.0
        self.M[HEA + 3] = 1.0

        self.M[ARM + 0] = self.M[EYE + 0] + 0.0
        self.M[ARM + 1] = self.M[EYE + 1] + 0.0
        self.M[ARM + 2] = self.M[EYE + 2] + dist
        self.M[ARM + 3] = 1.0

        self.UpdateParameters()

    def InitMode(self):

        if (self.MotionMode == FLIGHT):

            # set M correctly in order to preserve self.IntDist

            dist = self.IntDist / 3.

            # PTS

            v0 = (self.M[PTS + 0] - self.M[EYE + 0])
            v1 = (self.M[PTS + 1] - self.M[EYE + 1])
            v2 = (self.M[PTS + 2] - self.M[EYE + 2])
            d = sqrt(v0**2 + v1**2 + v2**2)

            self.M[PTS + 0] = self.M[EYE + 0] + v0 / d * dist * 3
            self.M[PTS + 1] = self.M[EYE + 1] + v1 / d * dist * 3
            self.M[PTS + 2] = self.M[EYE + 2] + v2 / d * dist * 3
            self.M[PTS + 3] = 1.0

            # HEA

            v0 = (self.M[HEA + 0] - self.M[EYE + 0])
            v1 = (self.M[HEA + 1] - self.M[EYE + 1])
            v2 = (self.M[HEA + 2] - self.M[EYE + 2])
            d = sqrt(v0**2 + v1**2 + v2**2)

            self.M[HEA + 0] = self.M[EYE + 0] + v0 / d * dist
            self.M[HEA + 1] = self.M[EYE + 1] + v1 / d * dist
            self.M[HEA + 2] = self.M[EYE + 2] + v2 / d * dist
            self.M[HEA + 3] = 1.0

            # ARM

            v0 = (self.M[ARM + 0] - self.M[EYE + 0])
            v1 = (self.M[ARM + 1] - self.M[EYE + 1])
            v2 = (self.M[ARM + 2] - self.M[EYE + 2])
            d = sqrt(v0**2 + v1**2 + v2**2)

            self.M[ARM + 0] = self.M[EYE + 0] + v0 / d * dist
            self.M[ARM + 1] = self.M[EYE + 1] + v1 / d * dist
            self.M[ARM + 2] = self.M[EYE + 2] + v2 / d * dist
            self.M[ARM + 3] = 1.0

            self.UpdateParameters()

    def UpdateParameters(self):

        # mode=NORMAL/ROTATION/ROTATION_ONLY/MOTIONS
        if (self.MotionMode == ROTATE_AROUND_PTS) or (
                self.MotionMode == FLIGHT_AROUND_PTS):
            self.IntDist = sqrt(pow(self.P[0] - self.M[EYE + 0], 2)
                                + pow(self.P[1] - self.M[EYE + 1], 2)
                                + pow(self.P[2] - self.M[EYE + 2], 2))
        # mode=FLIGHT
        else:
            self.IntDist = sqrt((self.M[EYE + 0] - self.M[ARM + 0]) * (self.M[EYE + 0] - self.M[ARM + 0])
                                + (self.M[EYE + 1] - self.M[ARM + 1]) * (self.M[EYE + 1] - self.M[ARM + 1])
                                + (self.M[EYE + 2] - self.M[ARM + 2]) * (self.M[EYE + 2] - self.M[ARM + 2])) * 3

        self.TranslationFactorXY = 0 + self.IntDist * 0.001
        self.TranslationFactorZ = 0 + self.IntDist * 0.001

        # perspective
        self.PerspectiveNear = self.IntDist * self.PerspectiveNearFactor
        self.PerspectiveFar = self.IntDist * self.PerspectiveFarFactor

        #EyeAngle = IntDist/StereoFactor;
        self.EyeDist = self.IntDist / self.StereoFactor
        self.ComputeEyes()

    def RotateAround(self, angle, axis, point, ObsM):
        return geometry.RotateAround(angle, axis, point, ObsM)

    def TranslateM(self, x, y, z):

        self.M[EYE + 0] += x
        self.M[EYE + 1] += y
        self.M[EYE + 2] += z

        self.M[PTS + 0] += x
        self.M[PTS + 1] += y
        self.M[PTS + 2] += z

        self.M[HEA + 0] += x
        self.M[HEA + 1] += y
        self.M[HEA + 2] += z

        self.M[ARM + 0] += x
        self.M[ARM + 1] += y
        self.M[ARM + 2] += z

    def TranslateAll(x, y, z):

        self.P[0] += x
        self.P[1] += y
        self.P[2] += z

        self.TranslateM(x, y, z)

        self.UpdateParameters()

    def Translate_Obs_Arm(self, fact):

        vect = ones(3, float)

        vect[0] = (self.M[EYE + 0] - self.M[ARM + 0])
        vect[1] = (self.M[EYE + 1] - self.M[ARM + 1])
        vect[2] = (self.M[EYE + 2] - self.M[ARM + 2])
        norm = sqrt(vect[0] * vect[0] + vect[1] * vect[1] + vect[2] * vect[2])
        vect[0] = vect[0] / norm * fact
        vect[1] = vect[1] / norm * fact
        vect[2] = vect[2] / norm * fact

        self.M[EYE + 0] += vect[0]
        self.M[EYE + 1] += vect[1]
        self.M[EYE + 2] += vect[2]

        self.M[PTS + 0] += vect[0]
        self.M[PTS + 1] += vect[1]
        self.M[PTS + 2] += vect[2]

        self.M[HEA + 0] += vect[0]
        self.M[HEA + 1] += vect[1]
        self.M[HEA + 2] += vect[2]

        self.M[ARM + 0] += vect[0]
        self.M[ARM + 1] += vect[1]
        self.M[ARM + 2] += vect[2]

    def Translate_Obs_Hea(self, fact):

        vect = ones(3, float)

        vect[0] = (self.M[EYE + 0] - self.M[HEA + 0])
        vect[1] = (self.M[EYE + 1] - self.M[HEA + 1])
        vect[2] = (self.M[EYE + 2] - self.M[HEA + 2])
        norm = sqrt(vect[0] * vect[0] + vect[1] * vect[1] + vect[2] * vect[2])
        vect[0] = vect[0] / norm * fact
        vect[1] = vect[1] / norm * fact
        vect[2] = vect[2] / norm * fact

        self.M[EYE + 0] += vect[0]
        self.M[EYE + 1] += vect[1]
        self.M[EYE + 2] += vect[2]

        self.M[PTS + 0] += vect[0]
        self.M[PTS + 1] += vect[1]
        self.M[PTS + 2] += vect[2]

        self.M[HEA + 0] += vect[0]
        self.M[HEA + 1] += vect[1]
        self.M[HEA + 2] += vect[2]

        self.M[ARM + 0] += vect[0]
        self.M[ARM + 1] += vect[1]
        self.M[ARM + 2] += vect[2]

    def Translate_Obs_Pts(self, fact):

        vect = ones(3, float)

        vect[0] = (self.M[PTS + 0] - self.M[EYE + 0])
        vect[1] = (self.M[PTS + 1] - self.M[EYE + 1])
        vect[2] = (self.M[PTS + 2] - self.M[EYE + 2])

        norm = sqrt(vect[0] * vect[0] + vect[1] * vect[1] + vect[2] * vect[2])

        vect[0] = vect[0] / norm * fact
        vect[1] = vect[1] / norm * fact
        vect[2] = vect[2] / norm * fact

        self.M[EYE + 0] += vect[0]
        self.M[EYE + 1] += vect[1]
        self.M[EYE + 2] += vect[2]

        self.M[PTS + 0] += vect[0]
        self.M[PTS + 1] += vect[1]
        self.M[PTS + 2] += vect[2]

        self.M[HEA + 0] += vect[0]
        self.M[HEA + 1] += vect[1]
        self.M[HEA + 2] += vect[2]

        self.M[ARM + 0] += vect[0]
        self.M[ARM + 1] += vect[1]
        self.M[ARM + 2] += vect[2]

    def Rotate_Obs_xWin(self, angle):

        axis = ones(3, float)
        point = ones(3, float)

        # rotation axis
        axis[0] = self.M[ARM + 0] - self.M[EYE + 0]
        axis[1] = self.M[ARM + 1] - self.M[EYE + 1]
        axis[2] = self.M[ARM + 2] - self.M[EYE + 2]

        # rotation point
        point[0] = self.P[0]
        point[1] = self.P[1]
        point[2] = self.P[2]

        # compute rotation matrix
        self.M = self.RotateAround(angle, axis, point, self.M)

    def Rotate_Obs_yWin(self, angle):

        axis = ones(3, float)
        point = ones(3, float)

        # rotation axis
        axis[0] = self.M[HEA + 0] - self.M[EYE + 0]
        axis[1] = self.M[HEA + 1] - self.M[EYE + 1]
        axis[2] = self.M[HEA + 2] - self.M[EYE + 2]

        # rotation point
        point[0] = self.P[0]
        point[1] = self.P[1]
        point[2] = self.P[2]

        # compute rotation matrix
        self.M = self.RotateAround(angle, axis, point, self.M)

    def Rotate_Obs_zWin(self, angle):

        axis = ones(3, float)
        point = ones(3, float)

        # rotation axis
        axis[0] = self.M[PTS + 0] - self.M[EYE + 0]
        axis[1] = self.M[PTS + 1] - self.M[EYE + 1]
        axis[2] = self.M[PTS + 2] - self.M[EYE + 2]

        # rotation point
        point[0] = self.P[0]
        point[1] = self.P[1]
        point[2] = self.P[2]

        # compute rotation matrix
        self.M = self.RotateAround(angle, axis, point, self.M)

    def Rotate_Pts_xWin(self, angle):

        axis = ones(3, float)
        point = ones(3, float)

        # rotation axis
        axis[0] = self.M[ARM + 0] - self.M[EYE + 0]
        axis[1] = self.M[ARM + 1] - self.M[EYE + 1]
        axis[2] = self.M[ARM + 2] - self.M[EYE + 2]

        # rotation point
        point[0] = self.M[EYE + 0]
        point[1] = self.M[EYE + 1]
        point[2] = self.M[EYE + 2]

        # compute rotation matrix
        self.M = self.RotateAround(angle, axis, point, self.M)

    def Rotate_Pts_yWin(self, angle):

        axis = ones(3, float)
        point = ones(3, float)

        # rotation axis
        axis[0] = self.M[HEA + 0] - self.M[EYE + 0]
        axis[1] = self.M[HEA + 1] - self.M[EYE + 1]
        axis[2] = self.M[HEA + 2] - self.M[EYE + 2]

        # rotation point
        point[0] = self.M[EYE + 0]
        point[1] = self.M[EYE + 1]
        point[2] = self.M[EYE + 2]

        # compute rotation matrix
        self.M = self.RotateAround(angle, axis, point, self.M)

    def Rotate_Pts_zWin(self, angle):

        axis = ones(3, float)
        point = ones(3, float)

        # rotation axis
        axis[0] = self.M[PTS + 0] - self.M[EYE + 0]
        axis[1] = self.M[PTS + 1] - self.M[EYE + 1]
        axis[2] = self.M[PTS + 2] - self.M[EYE + 2]

        # rotation point
        point[0] = self.M[EYE + 0]
        point[1] = self.M[EYE + 1]
        point[2] = self.M[EYE + 2]

        # compute rotation matrix
        self.M = self.RotateAround(angle, axis, point, self.M)

    def CallBackMouseFunc(self, x, y):

        if self.AutoMotionMode == ROTATE_AROUND_PTS:
            # self.Rotate_Obs_yWin(-x*self.RotationFactor*0.01)
            self.Rotate_Obs_xWin(+y * self.RotationFactor * 0.01)
            self.Translate_Obs_Pts(x * self.TranslationFactorZ * 0.025)

        elif self.AutoMotionMode == ROTATE_AROUND_EYE:
            self.Rotate_Pts_yWin(-x * self.RotationFactor * 0.01)
            self.Rotate_Pts_xWin(+y * self.RotationFactor * 0.01)

        elif self.AutoMotionMode == ROTATE_AROUND_EYE_HEAD:
            self.Translate_Obs_Pts(x * self.TranslationFactorZ * 0.025)

        elif self.AutoMotionMode == ROTATE_AROUND_ONLY:
            pass

        elif self.AutoMotionMode == FLIGHT:
            # self.Rotate_Obs_xWin(+y*self.RotationFactor*0.01)
            self.Rotate_Pts_yWin(-x * self.RotationFactor * 0.01)
            self.Rotate_Pts_xWin(+y * self.RotationFactor * 0.01)

    def CallBackMotionFunc(self, dx, dy, LastDownButton):

        if (LastDownButton == GLUT_LEFT_BUTTON):
            if self.MotionMode == ROTATE_AROUND_PTS:
                self.Rotate_Obs_yWin(-dx * self.RotationFactor)
                self.Rotate_Obs_xWin(-dy * self.RotationFactor)
            elif self.MotionMode == ROTATE_AROUND_EYE:
                self.Rotate_Pts_yWin(-dx * self.RotationFactor)
                self.Rotate_Pts_xWin(-dy * self.RotationFactor)
            elif self.MotionMode == FLIGHT:
                self.Rotate_Pts_yWin(-dx * self.RotationFactor)
                self.Rotate_Pts_xWin(-dy * self.RotationFactor)

        elif (LastDownButton == GLUT_MIDDLE_BUTTON):
            if self.MotionMode == ROTATE_AROUND_PTS:
                self.Rotate_Obs_zWin(-dx * self.RotationFactor)
                self.Rotate_Obs_zWin(-dy * self.RotationFactor)
            elif self.MotionMode == ROTATE_AROUND_EYE:
                self.Rotate_Pts_zWin(-dx * self.RotationFactor)
                self.Rotate_Pts_zWin(-dy * self.RotationFactor)
            elif self.MotionMode == FLIGHT:
                self.Rotate_Pts_zWin(-dx * self.RotationFactor)
                self.Rotate_Pts_zWin(-dy * self.RotationFactor)

        elif (LastDownButton == GLUT_RIGHT_BUTTON):
            if self.MotionMode == ROTATE_AROUND_PTS:
                self.Translate_Obs_Pts(-dx * self.TranslationFactorZ)
            elif self.MotionMode == ROTATE_AROUND_EYE:
                self.Translate_Obs_Pts(-dx * self.TranslationFactorZ)
            elif self.MotionMode == FLIGHT:
                self.Translate_Obs_Pts(-dx * self.TranslationFactorZ)

        self.UpdateParameters()

    def CallBackDownButton(self, button):
        angle = 30

        if button == 1:
            delta = -angle
        elif button == 2:
            delta = angle
        else:
            delta = 0

        if self.MotionMode == ROTATE_AROUND_PTS:
            self.Translate_Obs_Pts(delta * self.TranslationFactorZ)

        elif self.MotionMode == ROTATE_AROUND_EYE:
            self.Rotate_Pts_zWin(delta * self.RotationEyeFactor * 0.005)

        elif self.MotionMode == FLIGHT:
            self.Rotate_Pts_zWin(delta * self.RotationEyeFactor * 0.01)

    def CallBackWheel(self, delta):
        if self.MotionMode == ROTATE_AROUND_PTS:
            self.Translate_Obs_Pts(delta * self.TranslationFactorZ)
        elif self.MotionMode == ROTATE_AROUND_EYE:
            self.Rotate_Pts_zWin(delta * self.RotationEyeFactor * 0.005)

        elif self.MotionMode == FLIGHT:
            # self.Rotate_Obs_xWin(-delta*self.RotationFactor*0.01)
            dx = delta * self.MotionSpeed * 0.01
            self.Translate_Obs_Pts(dx)

        self.UpdateParameters()

    def CallBackKeyboardFunc(self, event):

        key = event.key()

        if key == QtCore.Qt.Key_X: 		# align x axis to the right
            self.AlignXY()
        elif key == QtCore.Qt.Key_Y:		# align y axis to the right
            self.AlignYZ()
        elif key == QtCore.Qt.Key_Z:   	# align z axis to the right
            self.AlignZX()

        elif key == QtCore.Qt.Key_U:   	# set interesting point to (0,0,0)
            self.PutIntPinZero()

        elif key == QtCore.Qt.Key_R:   	# reverse motion speed
            self.MotionSpeed = -self.MotionSpeed

        elif key == QtCore.Qt.Key_M: 		# switch motion mode
            self.MotionMode = self.MotionMode + 1
            if self.MotionMode > MOTION_MAX_MODE - 1:
                self.MotionMode = ROTATE_AROUND_PTS

        elif event.key() == QtCore.Qt.Key_Plus:  # increase motion speed

            # if   self.MotionMode==ROTATE_AROUND_PTS:
            #  self.Translate_Obs_Pts(+self.IntDist*0.1)
            # elif self.MotionMode==ROTATE_AROUND_EYE:
            self.MotionSpeed = self.MotionSpeed / 0.9

        elif event.key() == QtCore.Qt.Key_Minus:    # decrease motion speed

            # if   self.MotionMode==ROTATE_AROUND_PTS:
            #	self.Translate_Obs_Pts(-self.IntDist*0.1)
            # elif self.MotionMode==ROTATE_AROUND_EYE:
            self.MotionSpeed = self.MotionSpeed * 0.9

        elif event.key() == QtCore.Qt.Key_Left:     # rotate left
            if self.MotionMode == ROTATE_AROUND_PTS:
                self.Rotate_Obs_yWin(+self.RotationEyeFactor)
            elif self.MotionMode == ROTATE_AROUND_EYE:
                self.Rotate_Pts_zWin(+self.RotationEyeFactor)
            elif self.MotionMode == FLIGHT:
                self.Rotate_Pts_yWin(self.RotationFactor * 10.0)

        elif event.key() == QtCore.Qt.Key_Right:   	# rotate right
            if self.MotionMode == ROTATE_AROUND_PTS:
                self.Rotate_Obs_yWin(-self.RotationEyeFactor)
            elif self.MotionMode == ROTATE_AROUND_EYE:
                self.Rotate_Pts_zWin(-self.RotationEyeFactor)
            elif self.MotionMode == FLIGHT:
                self.Rotate_Pts_yWin(-self.RotationFactor * 10.0)

        elif event.key() == QtCore.Qt.Key_Up:    	# rotate up
            if self.MotionMode == ROTATE_AROUND_PTS:
                self.Rotate_Obs_xWin(+self.RotationEyeFactor)
            elif self.MotionMode == ROTATE_AROUND_EYE:
                self.Rotate_Pts_xWin(+self.RotationEyeFactor * 0.5)
            elif self.MotionMode == FLIGHT:
                self.Rotate_Pts_xWin(self.RotationFactor * 10.0)

        elif event.key() == QtCore.Qt.Key_Down:    	# rotate down
            if self.MotionMode == ROTATE_AROUND_PTS:
                self.Rotate_Obs_xWin(-self.RotationEyeFactor)
            elif self.MotionMode == ROTATE_AROUND_EYE:
                self.Rotate_Pts_xWin(-self.RotationEyeFactor * 0.5)
            elif self.MotionMode == FLIGHT:
                self.Rotate_Pts_xWin(-self.RotationFactor * 10.0)

        self.UpdateParameters()

    def CallBackAutoMotion(self, dt, Journey=None):

        if self.AutoMotionMode == ROTATE_AROUND_PTS:
            da = dt * self.MotionSpeed * 1e-2
            self.Rotate_Obs_yWin(da)

        elif self.AutoMotionMode == FLIGHT:

            dx = dt * self.MotionSpeed
            self.Translate_Obs_Pts(dx)

        elif self.AutoMotionMode == ROTATE_AROUND_EYE_HEAD:

            da = dt * self.MotionSpeed * 1e-2
            self.Rotate_Obs_yWin(da)

        elif self.AutoMotionMode == ROTATE_AROUND_ONLY:

            da = dt * self.MotionSpeed * 1e-2
            self.Rotate_Obs_yWin(da)

        elif self.AutoMotionMode == MOTIONS:

            # play with time
            tnow = time.time()
            t = clip((tnow - Journey.tstart) / Journey.duration, 0, 1)

            self.P = Journey.GetP(t)
            self.M = Journey.GetM(t)

            if t >= 1:
                self.targetP = None
                self.StopAutoMotion = True

                print("stop motion")

        self.UpdateParameters()
