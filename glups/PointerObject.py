# -*- coding: utf-8 -*-

'''
 @package   glups
 @file      PointerObject.py
 @brief     Definition of a Pointer object
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section COPYRIGHT

 Copyright (C) 2015 EPFL (Ecole Polytechnique F??d??rale de Lausanne)
 LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of glups.
'''

from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *

from PyQt5 import QtCore, QtGui, QtOpenGL

from .GLObject import *
from .ParameterList import *


class PointerObject(GLObject):

    def __init__(self, ID='Pointer'):

        self.ID = ID
        self.ListID = None

        # init parameters
        self.param = ParametersList(self.ID)
        self.param["Color_r"] = 1.0
        self.param["Color_g"] = 0.0
        self.param["Color_b"] = 0.0
        self.param["Color_a"] = 1.0
        self.param["PosX"] = 0.0
        self.param["PosY"] = 0.0
        self.param["PosZ"] = 0.0
        self.param["show"] = 1
        self.param["SizeX"] = 1.0
        self.param["SizeY"] = 1.0
        self.param["SizeZ"] = 1.0
        self.param["RotX"] = 0.0
        self.param["RotY"] = 0.0
        self.param["RotZ"] = 0.0
        self.param["RotationFactor"] = 0.1
        self.param["SizeFactor"] = 0.1

        self.param["SizeObsFactor"] = 0.03

        self.GenList()

    def GenList(self):

        dd = 0.3

        if self.ListID is None:
            self.ListID = glGenLists(1)
        glNewList(self.ListID, GL_COMPILE)

        glPointSize(2.0)

        glBegin(GL_LINES)
        glVertex3f(0, dd, 0)
        glVertex3f(0, 1, 0)

        glVertex3f(0, -dd, 0)
        glVertex3f(0, -1, 0)

        glVertex3f(dd, 0, 0)
        glVertex3f(1, 0, 0)

        glVertex3f(-dd, 0, 0)
        glVertex3f(-1, 0, 0)

        glVertex3f(0, 0, dd)
        glVertex3f(0, 0, 1)

        glVertex3f(0, 0, -dd)
        glVertex3f(0, 0, -1)

        glEnd()

        glBegin(GL_LINE_LOOP)

        # for ti in range(360):
        #  t = float(ti)*pi/180.
        #  glVertex3f(cos(t)*0.7,sin(t)*0.7,0)

        glEnd()

        glPointSize(1.0)

        glEndList()

    def SetFromObserver(self, obs):

        self.SetPos(obs.P[0], obs.P[1], obs.P[2])
        self.SetSize(obs.IntDist * self.param["SizeObsFactor"])
