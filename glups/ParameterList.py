# -*- coding: utf-8 -*-

'''
 @package   glups
 @file      ParameterList.py
 @brief     Definition of the ParameterList class
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section COPYRIGHT

 Copyright (C) 2015 EPFL (Ecole Polytechnique F??d??rale de Lausanne)
 LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of glups.
'''

import string


class ParametersList(dict):

    def __init__(self, ID=None):
        self.ID = ID

    def Info(self):

        for key in list(self.keys()):
            print(("%s:%s =%s" % (self.ID, key, str(self[key]))))

    def Write(self, f):

        keys = sorted(self.keys())
        for key in keys:
            string = "%s:%s = %s\n" % (self.ID, key, str(self[key]))
            f.write(string)

    def Read(self, f, ID=None):

        if ID is None:
            ID = self.ID

        f.seek(0)
        lines = f.readlines()
        f.seek(0)

        for line in lines:
            Id, val = str.split(line, ':')
            if Id == ID:
                val = str.strip(val)
                key, val = str.split(val, '=')
                key = str.strip(key)
                cmd = """self["%s"] = %s""" % (key, val)
                exec(cmd)
