# -*- coding: utf-8 -*-

'''
 @package   glups
 @file      GLObject.py
 @brief     Definition of the basic GLObject class.
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section COPYRIGHT

 Copyright (C) 2015 EPFL (Ecole Polytechnique F??d??rale de Lausanne)
 LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of glups.
'''

from numpy import *

from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *

from PyQt5 import QtCore, QtGui, QtOpenGL


class GLObject:

    def __init__(self, ID=None):
        '''
        here, we init some parameters
        '''
        self.ID = ID
        self.ListID = None

    def GenList(self):
        '''
        here, create the objet GLlist
        '''
        pass

    def Display(self, color=None):
        '''
        display the object
        '''
        if (self.param['show']):
            if color is None:
                glColor4d(
                    self.param["Color_r"],
                    self.param["Color_g"],
                    self.param["Color_b"],
                    self.param["Color_a"])
            else:
                glColor4d(color[0], color[1], color[2], self.param["Color_a"])

            glPushMatrix()
            glTranslated(
                self.param["PosX"],
                self.param["PosY"],
                self.param["PosZ"])
            glScaled(
                self.param["SizeX"],
                self.param["SizeY"],
                self.param["SizeZ"])
            glRotated(self.param["RotX"], 1, 0, 0)
            glRotated(self.param["RotY"], 0, 1, 0)
            glRotated(self.param["RotZ"], 0, 0, 1)
            glCallList(self.ListID)
            glPopMatrix()

    def Info(self):
        '''
        give info
        '''
        pass

    def Show(self):
        '''
        show
        '''
        self.param['show'] = 1

    def Hide(self):
        '''
        hide
        '''
        self.param['show'] = 0

    def SwitchShow(self):
        '''
        switch show
        '''
        if self.param['show'] == 1:
            self.param['show'] = 0
        else:
            self.param['show'] = 1

    def ReadParameters(self, file, ID=None):
        '''
        read parameters
        '''
        self.param.Read(file, ID)

    def WriteParameters(self, file):
        '''
        write parameters
        '''
        self.param.Write(file)

    def AutoSetParameters(self, ModelSize, CenterOfMass):
        '''
        auto set parameters
        '''
        self.param["PosX"] = CenterOfMass[0]
        self.param["PosY"] = CenterOfMass[1]
        self.param["PosZ"] = CenterOfMass[2]
        self.param["RotX"] = 0.0
        self.param["RotY"] = 0.0
        self.param["RotZ"] = 0.0
        self.param["SizeX"] = ModelSize
        self.param["SizeY"] = ModelSize
        self.param["SizeZ"] = ModelSize

    def SetPos(self, x, y, z):
        self.param["PosX"] = float(x)
        self.param["PosY"] = float(y)
        self.param["PosZ"] = float(z)

    def GetPos(self):
        return array(
            [self.param["PosX"], self.param["PosY"], self.param["PosZ"]])

    def SetRot(self, rx, ry, rz):
        self.param["RotX"] = float(rx)
        self.param["RotY"] = float(ry)
        self.param["RotZ"] = float(rz)

    def GetRot(self):
        return array(
            [self.param["RotX"], self.param["RotY"], self.param["RotZ"]])

    def SetSize(self, size):
        self.SetSizeX(size)
        self.SetSizeY(size)
        self.SetSizeZ(size)

    def GetSize(self):
        return array(
            [self.param["SizeX"], self.param["SizeY"], self.param["SizeZ"]])

    def SetSizeX(self, size):
        self.param["SizeX"] = float(size)

    def SetSizeY(self, size):
        self.param["SizeY"] = float(size)

    def SetSizeZ(self, size):
        self.param["SizeZ"] = float(size)

    def MulAlpha(self, f):
        self.param["Color_a"] = self.param["Color_a"] * f
        self.param["Color_a"] = max(0, self.param["Color_a"])
        self.param["Color_a"] = min(1, self.param["Color_a"])

    def DivAlpha(self, f):
        self.param["Color_a"] = self.param["Color_a"] / f
        self.param["Color_a"] = max(0, self.param["Color_a"])
        self.param["Color_a"] = min(1, self.param["Color_a"])

    def SetAlpha(self, a):
        self.param["Color_a"] = a
        self.param["Color_a"] = max(0, self.param["Color_a"])
        self.param["Color_a"] = min(1, self.param["Color_a"])

    def GetAlpha(self):
        return self.param["Color_a"]

    def SetColor4d(self, r, g, b, a):
        self.param["Color_r"] = r
        self.param["Color_g"] = g
        self.param["Color_b"] = b
        self.param["Color_a"] = a

    def SetColor3d(self, r, g, b):
        self.param["Color_r"] = r
        self.param["Color_g"] = g
        self.param["Color_b"] = b

    def SetColor(self, c):
        self.param["Color_r"] = c[0]
        self.param["Color_g"] = c[1]
        self.param["Color_b"] = c[2]
        self.param["Color_a"] = c[3]

    def CenterIntP(self, Centerx, Centery, Centerz):
        self.param["PosX"] = Centerx
        self.param["PosY"] = Centery
        self.param["PosZ"] = Centerz

    def GenList(self):
        pass

    def CallBackMouseFunc(self, dx, dy):
        pass

    def CallBackMotionFunc(self, dx, dy, LastDownButton):

        if (LastDownButton == GLUT_LEFT_BUTTON):
            self.param["RotY"] += dx * self.param["RotationFactor"]
            self.param["RotX"] += dy * self.param["RotationFactor"]
        elif (LastDownButton == GLUT_MIDDLE_BUTTON):
            self.param["RotZ"] -= dx * self.param["RotationFactor"]
            self.param["RotZ"] -= dy * self.param["RotationFactor"]
        elif (LastDownButton == GLUT_RIGHT_BUTTON):
            self.param["SizeX"] += dx * self.param["SizeFactor"]
            self.param["SizeY"] += dx * self.param["SizeFactor"]
            self.param["SizeZ"] += dx * self.param["SizeFactor"]

    def CallBackKeyboardFunc(self, event):

        key = event.key()

        if event.key() == QtCore.Qt.Key_Plus:
            self.param["SizeX"] /= 0.9
            self.param["SizeY"] /= 0.9
            self.param["SizeZ"] /= 0.9

        elif event.key() == QtCore.Qt.Key_Minus:
            self.param["SizeX"] += 0.9
            self.param["SizeY"] += 0.9
            self.param["SizeZ"] += 0.9

        elif event.key() == QtCore.Qt.Key_V:
            self.SwitchShow()

    def CallBackWheel(self, delta):
        self.param["SizeX"] += self.param["SizeX"] * delta * 1e-3
        self.param["SizeY"] += self.param["SizeY"] * delta * 1e-3
        self.param["SizeZ"] += self.param["SizeZ"] * delta * 1e-3
