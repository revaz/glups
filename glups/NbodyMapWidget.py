# -*- coding: utf-8 -*-

'''
 @package   glups
 @file      NbodyMapWidget.py
 @brief     Definition of the NbodyMapWidget class (display nbody using the pNbody mapping technique).
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section COPYRIGHT

 Copyright (C) 2015 EPFL (Ecole Polytechnique F??d??rale de Lausanne)
 LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of glups.
'''

import sys
import math
from PyQt5 import QtCore, QtGui, QtOpenGL, QtWidgets


from pNbody import *
from pNbody import libqt

from PIL import ImageQt as IQt


class NbodyMapWidget(QtWidgets.QWidget):
    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        # Widget accepts focus (tab and mouse)
        self.setFocusPolicy(QtCore.Qt.StrongFocus)
        self.parent = parent

        self.setMouseTracking(True)		# see mouseMoveEvent

        self.setCursor(QtCore.Qt.CrossCursor)

        self.pixmapOffset = QtCore.QPoint()
        self.pixmap = None
        self.palette_pixmap = None

        self.dyLabel = 20
        self.x0Label = 5
        self.y0Label = 5
        self.colorLabel = QtCore.Qt.red

        self.Mat = None
        self.Min = 0
        self.Max = 0
        self.Cd = 0

        self.CreateInfo()

    ########################################
    # events
    ########################################

    def keyPressEvent(self, event):

        key = event.key()

        if (key == QtCore.Qt.Key_N):
            self.setCursor(QtCore.Qt.WaitCursor)
            idx = self.parent.ModelFiles.index(
                self.parent.CurrentFile) + self.parent.skip
            while(idx >= len(self.parent.ModelFiles)):
                idx -= len(self.parent.ModelFiles)

            self.parent.CurrentFile = self.parent.ModelFiles[idx]
            self.parent.LoadFile(self.parent.CurrentFile)
            self.parent.Display(updateFromGL=False)
            self.setCursor(QtCore.Qt.CrossCursor)
        if (key == QtCore.Qt.Key_B):
            self.setCursor(QtCore.Qt.WaitCursor)
            idx = self.parent.ModelFiles.index(
                self.parent.CurrentFile) - self.parent.skip
            while(idx < 0):
                idx += len(self.parent.ModelFiles)

            self.parent.CurrentFile = self.parent.ModelFiles[idx]
            self.parent.LoadFile(self.parent.CurrentFile)
            # self.parent.infobarDockWidgetContents.reload(self.parent.nb)
            self.parent.Display(updateFromGL=False)
            self.setCursor(QtCore.Qt.CrossCursor)

        if (key == QtCore.Qt.Key_D):		# display map
            self.setCursor(QtCore.Qt.WaitCursor)
            self.parent.Display(updateFromGL=False)
            self.setCursor(QtCore.Qt.CrossCursor)

        if (key == QtCore.Qt.Key_R):		# redisplay map
            self.setCursor(QtCore.Qt.WaitCursor)
            self.parent.ReDisplay()
            self.setCursor(QtCore.Qt.CrossCursor)

        elif key == QtCore.Qt.Key_Escape: 	# quit
            sys.exit()

        elif key == QtCore.Qt.Key_Q: 		# quit
            sys.exit()

    def mousePressEvent(self, event):

        if event.buttons() & QtCore.Qt.LeftButton:
            self.lastPos = QtCore.QPoint(event.pos())
            self.lastPosX = self.lastPos.x()
            self.lastPosY = self.lastPos.y()

        elif event.buttons() & QtCore.Qt.RightButton:
            pass

        elif event.buttons() & QtCore.Qt.MidButton:
            pass

    def mouseDoubleClickEvent(self, event):

        if event.buttons() & QtCore.Qt.LeftButton:
            pass

        elif event.buttons() & QtCore.Qt.RightButton:
            self.setCursor(QtCore.Qt.WaitCursor)
            self.parent.ReDisplay()
            self.setCursor(QtCore.Qt.CrossCursor)

        elif event.buttons() & QtCore.Qt.MidButton:
            pass

    def mouseMoveEvent(self, event):
        x = event.x()
        y = event.y()

        if self.Mat is not None:

            shape = self.Mat.shape
            x = min(shape[0] - 1, x)
            x = max(0, x)
            y = min(shape[1] - 1, y)
            y = max(0, y)
            val = self.Mat[x, y]

            txt = "value = %g" % (val)
            self.label.setText("""<font color="red">%s</font>""" % txt)

            txt = "time = %g" % self.parent.nb.atime
            self.time.setText("""<font color="red">%s</font>""" % txt)

            txt = "min=%10.3e max=%10.3e cd=%10.3e" % (
                self.Min, self.Max, self.Cd)
            self.info.setText("""<font color="red">%s</font>""" % txt)

    ########################################
    # palette
    ########################################

    def DisplayPalette(self, p="rainbow4"):

        n = 512
        matint = (arange(n) * ones((64, n))) / 2
        matint = matint.astype(uint8)

        #imageQt = libqt.QNumarrayImage(matint, p)
        imagePIL = get_image(matint, palette_name="rainbow4")
        imageQt = IQt.ImageQt(imagePIL)

        self.palette_pixmap = QtGui.QPixmap.fromImage(imageQt)

    ########################################
    # display
    ########################################

    def DisplayMap(self, mat, matint, mn, mx, cd, p="rainbow4"):

        self.Mat = mat
        self.Min = mn
        self.Max = mx
        self.Cd = cd

        txt = "time = %g" % self.parent.nb.atime
        self.time.setText("""<font color="red">%s</font>""" % txt)
        self.info.setText(
            "min=%10.3e max=%10.3e cd=%10.3e" %
            (self.Min, self.Max, self.Cd))

        imageQt = libqt.QNumarrayImage(matint, p)
        #imagePIL = get_image(matint, palette_name="rainbow4")
        #imageQt = IQt.ImageQt(imagePIL)
        self.pixmap = QtGui.QPixmap.fromImage(imageQt)

        # self.DisplayPalette(p=p)

        self.update()

    def SaveMap(self, filename, mat, matint, p="rainbow4"):

        imageQt = libqt.QNumarrayImage(matint, p)
        imageQt.save(filename)

    ########################################
    # painter
    ########################################

    def paintEvent(self, event):

        if self.pixmap is not None:
            painter = QtGui.QPainter(self)
            painter.fillRect(self.rect(), QtCore.Qt.white)
            painter.drawPixmap(self.pixmapOffset, self.pixmap)

        if self.palette_pixmap is not None:
            painter = QtGui.QPainter(self)
            painter.fillRect(QtCore.QRect(), QtCore.Qt.white)
            painter.drawPixmap(
                QtCore.QPoint(
                    512, 0), self.palette_pixmap, QtCore.QRect(
                    0, 0, 64, 512))

    ########################################
    # info
    ########################################

    def CreateInfo(self):

        font = QtGui.QFont()
        # font.setWeight(100)
        # font.setBold(True)

        self.label = QtWidgets.QLabel(self)
        self.label.setGeometry(
            QtCore.QRect(
                self.x0Label,
                self.y0Label,
                512,
                18))
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.label.setText(
            QtWidgets.QApplication.translate(
                "Form",
                "Observer position :",
                None))
        txt = 'value = %g' % (0)
        self.label.setText("""<font color="red">%s</font>""" % txt)

        self.time = QtWidgets.QLabel(self)
        self.time.setGeometry(
            QtCore.QRect(
                self.x0Label,
                self.y0Label +
                self.dyLabel,
                512,
                18))
        self.time.setFont(font)
        self.time.setObjectName("time")
        txt = 'time = %g' % (0)
        self.time.setText("""<font color="red">%s</font>""" % txt)

        self.info = QtWidgets.QLabel(self)
        self.info.setGeometry(
            QtCore.QRect(
                self.x0Label,
                self.y0Label +
                2 *
                self.dyLabel,
                512,
                18))
        self.info.setFont(font)
        self.info.setObjectName("info")
        self.info.setText(
            QtWidgets.QApplication.translate(
                "Form",
                "Observer position :",
                None))

        txt = "min=%10.3e max=%10.3e cd=%10.3e" % (self.Min, self.Max, self.Cd)
        self.info.setText("""<font color="red">%s</font>""" % txt)
