# -*- coding: utf-8 -*-

'''
 @package   glups
 @file      TimerObject.py
 @brief     Definition of a Timer object
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section COPYRIGHT

 Copyright (C) 2015 EPFL (Ecole Polytechnique F??d??rale de Lausanne)
 LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of glups.
'''

import time
from .GLObject import *
from .ParameterList import *


class TimerObject(GLObject):

    def __init__(self, ID='Timer'):

        self.ID = ID
        self.ListID = None

        # init parameters
        self.param = ParametersList(self.ID)
        self.param["show"] = 0
        self.param["Time"] = 0.0

    def SetTime(self, tnow=None):
        if tnow is None:
            self.param["Time"] = time.time()
        else:
            self.param["Time"] = tnow

    def Show(self):
        '''
        do nothing here
        '''
        return None
