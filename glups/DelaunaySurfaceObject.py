from OpenGL.GL import *
# -*- coding: utf-8 -*-

'''
 @package   glups
 @file      DelaunaySurfaceObject.py
 @brief     Definition of an DelaunaySurface object
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section COPYRIGHT

 Copyright (C) 2015 EPFL (Ecole Polytechnique F??d??rale de Lausanne)
 LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of glups.
'''

from OpenGL.GLU import *
from OpenGL.GLUT import *

from .GLObject import *
from .ParameterList import *

from numpy import *

from glups import libglups
from . import delaunay


class DelaunaySurfaceObject(GLObject):

    def __init__(self, pos, val, level, DelaunayFile, ID='DelaunayLines'):

        self.ID = ID
        self.ListID = None
        self.pos = pos
        self.val = val
        self.level = level
        self.DelaunayFile = DelaunayFile

        # init parameters
        self.param = ParametersList(self.ID)
        self.param["Color_r"] = 1.0
        self.param["Color_g"] = 0.5
        self.param["Color_b"] = 0.5
        self.param["Color_a"] = 0.5
        self.param["show"] = 1
        self.param["PosX"] = 0.0
        self.param["PosY"] = 0.0
        self.param["PosZ"] = 0.0
        self.param["SizeX"] = 1.0
        self.param["SizeY"] = 1.0
        self.param["SizeZ"] = 1.0
        self.param["RotX"] = 0.0
        self.param["RotY"] = 0.0
        self.param["RotZ"] = 0.0
        self.param["RotationFactor"] = 0.1
        self.param["SizeFactor"] = 0.1

        self.GenList()

    def GenList(self):

        if self.ListID is None:
            self.ListID = glGenLists(1)
        glNewList(self.ListID, GL_COMPILE)

        # glBegin(GL_LINES)
        delaunay.FromDelaunayGenerateSurfaceList(
            self.pos, self.val, self.level, DelaunayFile=self.DelaunayFile)
        # glEnd()

        glEndList()

    def Display(self, color=None):
        '''
        display the object
        '''
        if (self.param['show']):
            if color is None:
                glColor4d(
                    self.param["Color_r"],
                    self.param["Color_g"],
                    self.param["Color_b"],
                    self.param["Color_a"])
            else:
                glColor4d(color[0], color[1], color[2], self.param["Color_a"])
            glCallList(self.ListID)
