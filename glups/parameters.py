# -*- coding: utf-8 -*-

'''
 @package   glups
 @file      parameters.py
 @brief     Definition of basic parameters
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section COPYRIGHT

 Copyright (C) 2015 EPFL (Ecole Polytechnique F??d??rale de Lausanne)
 LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of glups.
'''

import os
import sys
import string

################################
# installation path
################################
GLUPSPATH = os.path.dirname(__file__)


################################
# home directory
################################
HOME = os.environ['HOME']

################################
# default config directory
################################
CONFIGDIR = os.path.join(GLUPSPATH, 'config')


# parameter file
PARAMETERFILE = os.path.join(CONFIGDIR, 'glups')
if os.path.isfile(os.path.join(HOME, '.glups')):
    PARAMETERFILE = os.path.join(HOME, '.glups')
