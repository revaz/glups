# -*- coding: utf-8 -*-

'''
 @package   glups
 @file      Journey.py
 @brief     Definition of the Journey class.
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section COPYRIGHT

 Copyright (C) 2015 EPFL (Ecole Polytechnique F??d??rale de Lausanne)
 LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of glups.
'''

import numpy as np
import copy as pycopy
from scipy.optimize import bisect
from . import geometry

EYE = 0
PTS = 4
HEA = 8
ARM = 12


class Journey():

    def __init__(self, duration=0, tstart=0, angle=0):

        self.MMotion = None
        self.PMotion = None
        self.duration = duration
        self.tstart = tstart

        self.Pstart = None        # initial position
        self.Pend = None        # final position
        self.s = 0.5         # acceleration time
        self.ar = None        # accel exponent (rotation)
        self.at = None        # accel exponent (translation)
        self.Pdistance = None
        self.Mdistance = None

        self.Mstart = None        # initial observer position
        self.Mend = None        # final observer position
        self.angle = None

        self.rotationpoint = None

    def SetMMotion(self, mtype):
        self.MMotion = mtype

    def SetPMotion(self, ptype):
        self.PMotion = ptype

    def SetPstart(self, Pstart):
        self.Pstart = pycopy.copy(Pstart)

    def SetPend(self, Pend):
        self.Pend = pycopy.copy(Pend)

    def SetMstart(self, Mstart):
        self.Mstart = pycopy.copy(Mstart)

    def SetMend(self, Mend):
        self.Mend = pycopy.copy(Mend)

    def SetAngle(self, angle):
        self.angle = angle

    def SetAxis(self, axis):
        self.axis = axis

    def SetDirection(self, direction):
        self.direction = direction

    def SetRotationPoint(self, point):
        self.rotationpoint = point

    def ComputeMotionParameters(self):

        if self.PMotion == "Translate":

            # compute distance
            self.Pdistance = np.sqrt((self.Pstart[0] - self.Pend[0])**2 + (
                self.Pstart[1] - self.Pend[1])**2 + (self.Pstart[2] - self.Pend[2])**2)

            # compute exponent
            def fct(x):
                return np.exp(x * self.s) * (x * (1 - self.s) +
                                             1) - 1 - self.Pdistance / 2.0

            self.at = bisect(
                fct,
                0.0001,
                100.0,
                args=(),
                xtol=1e-10,
                maxiter=100)

        if self.MMotion == "Rotate":

            # compute exponent
            def fct(x):
                return np.exp(x * self.s) * (x * (1 - self.s) +
                                             1) - 1 - np.fabs(self.angle) / 2.0

            self.ar = bisect(
                fct,
                0.0001,
                100.0,
                args=(),
                xtol=1e-10,
                maxiter=100)

        if self.MMotion == "Translate":

            # compute distance
            self.Mdistance = np.sqrt((self.Mstart[0] - self.Mend[0])**2 + (
                self.Mstart[1] - self.Mend[1])**2 + (self.Mstart[2] - self.Mend[2])**2)

            # compute exponent
            def fct(x):
                return np.exp(x * self.s) * (x * (1 - self.s) +
                                             1) - 1 - self.Mdistance / 2.0

            self.aMt = bisect(
                fct,
                0.0001,
                100.0,
                args=(),
                xtol=1e-10,
                maxiter=100)

        if self.MMotion == "RotateTranslate":

            # rotation
            # compute exponent
            def fct(x):
                return np.exp(x * self.s) * (x * (1 - self.s) +
                                             1) - 1 - np.fabs(self.angle) / 2.0

            self.ar = bisect(
                fct,
                0.0001,
                100.0,
                args=(),
                xtol=1e-10,
                maxiter=100)

            # translateion
            # compute distance
            self.Mdistance = np.sqrt((self.Mstart[0] - self.Mend[0])**2 + (
                self.Mstart[1] - self.Mend[1])**2 + (self.Mstart[2] - self.Mend[2])**2)

            # compute exponent
            def fct(x):
                return np.exp(x * self.s) * (x * (1 - self.s) +
                                             1) - 1 - self.Mdistance / 2.0

            self.aMt = bisect(
                fct,
                0.0001,
                100.0,
                args=(),
                xtol=1e-10,
                maxiter=100)

    def Type(self):
        return self.jtype

    def GetPDistance(self, t):
        """
        Get a distance along the journey path line
        t : a normalized time between [0,1]
        u : the half time [0,1]
        """

        t = np.clip(t, 0, 1)

        if t < 0.5:
            u = 2 * t
        else:
            u = 2 * (1. - t)

        if u <= 0.5:
            x = np.exp(self.at * u) - 1
        else:
            x = np.exp(self.at * self.s) * (self.at * (u - self.s) + 1) - 1

        if t > 0.5:
            x = self.Pdistance - x

        return x

    def GetMDistance(self, t):
        """
        Get a distance along the journey path line
        t : a normalized time between [0,1]
        u : the half time [0,1]
        """

        t = np.clip(t, 0, 1)

        if t < 0.5:
            u = 2 * t
        else:
            u = 2 * (1. - t)

        if u <= 0.5:
            x = np.exp(self.aMt * u) - 1
        else:
            x = np.exp(self.aMt * self.s) * (self.aMt * (u - self.s) + 1) - 1

        if t > 0.5:
            x = self.Mdistance - x

        return x

    def GetAngle(self, t):
        """
        Get an agnle along the journey path line
        t : a normalized time between [0,1]
        u : the half time [0,1]
        """

        t = np.clip(t, 0, 1)

        if t < 0.5:
            u = 2 * t
        else:
            u = 2 * (1. - t)

        if u <= 0.5:
            x = np.exp(self.ar * u) - 1
        else:
            x = np.exp(self.ar * self.s) * (self.ar * (u - self.s) + 1) - 1

        if self.angle < 0:
            x = -x

        if t > 0.5:
            x = self.angle - x

        return x

    def GetM(self, t):
        """
        return M for a given time
        """

        ###################################
        # rotate observer around the EYE

        if self.MMotion == "Rotate":

            angle = self.GetAngle(t)

            axis = np.ones(3, float)
            point = np.ones(3, float)

            # rotation point
            point[0] = self.rotationpoint[0]
            point[1] = self.rotationpoint[1]
            point[2] = self.rotationpoint[2]

            # rotation axis
            if self.axis == "z":
                axis[0] = self.Mstart[HEA + 0] - self.Mstart[EYE + 0]
                axis[1] = self.Mstart[HEA + 1] - self.Mstart[EYE + 1]
                axis[2] = self.Mstart[HEA + 2] - self.Mstart[EYE + 2]
            elif self.axis == "x":
                axis[0] = self.Mstart[ARM + 0] - self.Mstart[EYE + 0]
                axis[1] = self.Mstart[ARM + 1] - self.Mstart[EYE + 1]
                axis[2] = self.Mstart[ARM + 2] - self.Mstart[EYE + 2]
            elif self.axis == "y":
                axis[0] = self.Mstart[PTS + 0] - self.Mstart[EYE + 0]
                axis[1] = self.Mstart[PTS + 1] - self.Mstart[EYE + 1]
                axis[2] = self.Mstart[PTS + 2] - self.Mstart[EYE + 2]
            else:
                # we assume axis is already a vector
                axis = self.axis

            # compute rotation matrix
            M = geometry.RotateAround(
                angle, axis, point, pycopy.deepcopy(
                    self.Mstart))

            return M

        elif self.MMotion == "RotateTranslate":

            ##########################
            # first, rotate

            angle = self.GetAngle(t)

            axis = np.ones(3, float)
            point = np.ones(3, float)

            # rotation point
            point[0] = self.rotationpoint[0]
            point[1] = self.rotationpoint[1]
            point[2] = self.rotationpoint[2]

            # rotation axis
            if self.axis == "z":
                axis[0] = self.Mstart[HEA + 0] - self.Mstart[EYE + 0]
                axis[1] = self.Mstart[HEA + 1] - self.Mstart[EYE + 1]
                axis[2] = self.Mstart[HEA + 2] - self.Mstart[EYE + 2]
            elif self.axis == "x":
                axis[0] = self.Mstart[ARM + 0] - self.Mstart[EYE + 0]
                axis[1] = self.Mstart[ARM + 1] - self.Mstart[EYE + 1]
                axis[2] = self.Mstart[ARM + 2] - self.Mstart[EYE + 2]
            elif self.axis == "y":
                axis[0] = self.Mstart[PTS + 0] - self.Mstart[EYE + 0]
                axis[1] = self.Mstart[PTS + 1] - self.Mstart[EYE + 1]
                axis[2] = self.Mstart[PTS + 2] - self.Mstart[EYE + 2]
            else:
                # we assume axis is already a vector
                axis = self.axis

            # compute rotation matrix
            M = geometry.RotateAround(
                angle, axis, point, pycopy.deepcopy(
                    self.Mstart))

            ##########################
            # now translate

            dist = self.GetMDistance(t)

            axis[0] = M[PTS + 0] - M[EYE + 0]
            axis[1] = M[PTS + 1] - M[EYE + 1]
            axis[2] = M[PTS + 2] - M[EYE + 2]

            norm = np.sqrt(axis[0]**2 + axis[1]**2 + axis[2]**2)
            norm = dist / norm

            point[0] = M[EYE + 0] + axis[0] * norm
            point[1] = M[EYE + 1] + axis[1] * norm
            point[2] = M[EYE + 2] + axis[2] * norm

            Mstart = np.array([M[0], M[1], M[2], 1])
            Mend = np.array([point[0], point[1], point[2], 1])

            DP = (Mend - Mstart) * self.direction

            M[EYE + 0] = M[EYE + 0] + DP[0]
            M[EYE + 1] = M[EYE + 1] + DP[1]
            M[EYE + 2] = M[EYE + 2] + DP[2]

            M[PTS + 0] = M[PTS + 0] + DP[0]
            M[PTS + 1] = M[PTS + 1] + DP[1]
            M[PTS + 2] = M[PTS + 2] + DP[2]

            M[HEA + 0] = M[HEA + 0] + DP[0]
            M[HEA + 1] = M[HEA + 1] + DP[1]
            M[HEA + 2] = M[HEA + 2] + DP[2]

            M[ARM + 0] = M[ARM + 0] + DP[0]
            M[ARM + 1] = M[ARM + 1] + DP[1]
            M[ARM + 2] = M[ARM + 2] + DP[2]

            return M

        elif self.MMotion == "Translate":

            dist = self.GetMDistance(t)

            M = np.ones(4)
            M[0] = self.Mstart[0]
            M[1] = self.Mstart[1]
            M[2] = self.Mstart[2]

            Mstart = np.ones(4)
            Mstart[0] = self.Mstart[0]
            Mstart[1] = self.Mstart[1]
            Mstart[2] = self.Mstart[2]

            DP = (self.Mend - Mstart) * dist / self.Mdistance

            M = np.zeros(16)

            M[EYE + 0] = self.Mstart[EYE + 0] + DP[0]
            M[EYE + 1] = self.Mstart[EYE + 1] + DP[1]
            M[EYE + 2] = self.Mstart[EYE + 2] + DP[2]

            M[PTS + 0] = self.Mstart[PTS + 0] + DP[0]
            M[PTS + 1] = self.Mstart[PTS + 1] + DP[1]
            M[PTS + 2] = self.Mstart[PTS + 2] + DP[2]

            M[HEA + 0] = self.Mstart[HEA + 0] + DP[0]
            M[HEA + 1] = self.Mstart[HEA + 1] + DP[1]
            M[HEA + 2] = self.Mstart[HEA + 2] + DP[2]

            M[ARM + 0] = self.Mstart[ARM + 0] + DP[0]
            M[ARM + 1] = self.Mstart[ARM + 1] + DP[1]
            M[ARM + 2] = self.Mstart[ARM + 2] + DP[2]

            return M

        else:
            return self.Mstart

    def GetP(self, t):
        """
        return P for a given time
        """

        ###################################
        # translate P to reach Pend
        if self.PMotion == "Translate":
            dist = self.GetPDistance(t)
            P = self.Pstart + (self.Pend - self.Pstart) * dist / self.Pdistance
            return P

        else:
            return self.Pstart

    def CheckAndPlot(self):

        ts = arange(0, 1, 0.01)
        xs = zeros(len(ts))
        for i, t in enumerate(ts):
            xs[i] = self.GetPDistance(t)

        #from matplotlib import pyplot as plt
        #plt.plot(ts, xs)
        #plt.show()
