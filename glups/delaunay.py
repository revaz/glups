# -*- coding: utf-8 -*-

'''
 @package   glups
 @file      delaunay.py
 @brief     Definition of some functions related to delaunay tessellation.
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section COPYRIGHT

 Copyright (C) 2015 EPFL (Ecole Polytechnique F??d??rale de Lausanne)
 LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of glups.
'''

from pNbody import *
import sys

from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *


flag = 0


def read_delaunay3d(file):

    f = open(file, 'r')
    nco = int(f.readline())

    lines = f.readlines()
    lines = list(map(string.strip, lines))
    elts = list(map(string.split, lines))

    w = array([int(x[0]) for x in elts])
    x = array([int(x[1]) for x in elts])
    y = array([int(x[2]) for x in elts])
    z = array([int(x[3]) for x in elts])

    coo = transpose(array([w, x, y, z]))

    return nco, coo


def draw_tri(pts):
    x0 = pts[0, 0]
    y0 = pts[0, 1]
    z0 = pts[0, 2]
    x1 = pts[1, 0]
    y1 = pts[1, 1]
    z1 = pts[1, 2]
    x2 = pts[2, 0]
    y2 = pts[2, 1]
    z2 = pts[2, 2]

    '''
  g.relocate(x0,y0)
  g.draw(x1,y1)
  g.relocate(x1,y1)
  g.draw(x2,y2)
  g.relocate(x2,y2)
  g.draw(x0,y0)
  '''


def gl_draw_tet(pts):

    x0 = pts[0, 0]
    y0 = pts[0, 1]
    z0 = pts[0, 2]
    x1 = pts[1, 0]
    y1 = pts[1, 1]
    z1 = pts[1, 2]
    x2 = pts[2, 0]
    y2 = pts[2, 1]
    z2 = pts[2, 2]
    x3 = pts[3, 0]
    y3 = pts[3, 1]
    z3 = pts[3, 2]

    glVertex3f(x0, y0, z0)
    glVertex3f(x1, y1, z1)
    glVertex3f(x0, y0, z0)
    glVertex3f(x2, y2, z2)
    glVertex3f(x0, y0, z0)
    glVertex3f(x3, y3, z3)

    glVertex3f(x1, y1, z1)
    glVertex3f(x2, y2, z2)
    glVertex3f(x1, y1, z1)
    glVertex3f(x3, y3, z3)

    glVertex3f(x2, y2, z2)
    glVertex3f(x3, y3, z3)


def draw_level_in_sorted_tri(
        v,
        x0,
        y0,
        z0,
        v0,
        x1,
        y1,
        z1,
        v1,
        x2,
        y2,
        z2,
        v2):

    if ((v <= v0) and (v >= v2)) and (v0 != v2):

        a = (v - v0) / (v2 - v0)
        xx1 = a * (x2 - x0) + x0
        yy1 = a * (y2 - y0) + y0
        zz1 = a * (z2 - z0) + z0

        glVertex3f(xx1, yy1, zz1)

        if (v <= v1) and (v >= v2):

            a = (v - v1) / (v2 - v1)
            xx2 = a * (x2 - x1) + x1
            yy2 = a * (y2 - y1) + y1
            zz2 = a * (z2 - z1) + z1

            glVertex3f(xx2, yy2, zz2)

        elif (v0 != v1):

            a = (v - v1) / (v0 - v1)
            xx2 = a * (x0 - x1) + x1
            yy2 = a * (y0 - y1) + y1
            zz2 = a * (z0 - z1) + z1

            glVertex3f(xx2, yy2, zz2)

        else:
            print(("mmh, there is a bug here !!!", v0, v1, v2, v))
            sys.exit()


def draw_surface_in_sorted_tri(
        v,
        x0,
        y0,
        z0,
        v0,
        x1,
        y1,
        z1,
        v1,
        x2,
        y2,
        z2,
        v2):

    if ((v <= v0) and (v >= v2)) and (v0 != v2):

        a = (v - v0) / (v2 - v0)
        xx1 = a * (x2 - x0) + x0
        yy1 = a * (y2 - y0) + y0
        zz1 = a * (z2 - z0) + z0

        glVertex3f(xx1, yy1, zz1)

        if (v <= v1) and (v >= v2):

            a = (v - v1) / (v2 - v1)
            xx2 = a * (x2 - x1) + x1
            yy2 = a * (y2 - y1) + y1
            zz2 = a * (z2 - z1) + z1

            glVertex3f(xx2, yy2, zz2)

        elif (v0 != v1):

            a = (v - v1) / (v0 - v1)
            xx2 = a * (x0 - x1) + x1
            yy2 = a * (y0 - y1) + y1
            zz2 = a * (z0 - z1) + z1

            glVertex3f(xx2, yy2, zz2)

        else:
            print(("mmh, there is a bug here !!!", v0, v1, v2, v))
            sys.exit()


def draw_level(pts, v):

    x0 = pts[0, 0]
    y0 = pts[0, 1]
    z0 = pts[0, 2]
    x1 = pts[1, 0]
    y1 = pts[1, 1]
    z1 = pts[1, 2]
    x2 = pts[2, 0]
    y2 = pts[2, 1]
    z2 = pts[2, 2]

    #v0 = x0**2+y0**2
    #v1 = x1**2+y1**2
    #v2 = x2**2+y2**2

    # sort points

    if v0 > v1:
        if v0 > v2:
            if v1 > v2:
                draw_level_in_sorted_tri(
                    v, x0, y0, z0, v0, x1, y1, z1, v1, x2, y2, z2, v2)
            else:
                draw_level_in_sorted_tri(
                    v, x0, y0, z0, v0, x2, y2, z2, v2, x1, y1, z1, v1)
        else:
            draw_level_in_sorted_tri(
                v, x2, y2, z2, v2, x0, y0, z0, v0, x1, y1, z1, v1)

    else:
        if v1 > v2:
            if v0 > v2:
                draw_level_in_sorted_tri(
                    v, x1, y1, z1, v1, x0, y0, z0, v0, x2, y2, z2, v2)
            else:
                draw_level_in_sorted_tri(
                    v, x1, y1, z1, v1, x2, y2, z2, v2, x0, y0, z0, v0)
        else:
            draw_level_in_sorted_tri(
                v, x2, y2, z2, v2, x1, y1, z1, v1, x0, y0, z0, v0)


def gl_old_draw_surface(pts, val, v):

    xx0 = pts[0, 0]
    yy0 = pts[0, 1]
    zz0 = pts[0, 2]
    xx1 = pts[1, 0]
    yy1 = pts[1, 1]
    zz1 = pts[1, 2]
    xx2 = pts[2, 0]
    yy2 = pts[2, 1]
    zz2 = pts[2, 2]
    xx3 = pts[3, 0]
    yy3 = pts[3, 1]
    zz3 = pts[3, 2]

    vv0 = val[0]
    vv1 = val[1]
    vv2 = val[2]
    vv3 = val[3]

    x0 = xx0
    y0 = yy0
    z0 = zz0
    v0 = vv0

    x1 = xx1
    y1 = yy1
    z1 = zz1
    v1 = vv1

    x2 = xx2
    y2 = yy2
    z2 = zz2
    v2 = vv2

    # sort points

    if v0 > v1:
        if v0 > v2:
            if v1 > v2:
                draw_surface_in_sorted_tri(
                    v, x0, y0, z0, v0, x1, y1, z1, v1, x2, y2, z2, v2)
            else:
                draw_surface_in_sorted_tri(
                    v, x0, y0, z0, v0, x2, y2, z2, v2, x1, y1, z1, v1)
        else:
            draw_surface_in_sorted_tri(
                v, x2, y2, z2, v2, x0, y0, z0, v0, x1, y1, z1, v1)

    else:
        if v1 > v2:
            if v0 > v2:
                draw_surface_in_sorted_tri(
                    v, x1, y1, z1, v1, x0, y0, z0, v0, x2, y2, z2, v2)
            else:
                draw_surface_in_sorted_tri(
                    v, x1, y1, z1, v1, x2, y2, z2, v2, x0, y0, z0, v0)
        else:
            draw_surface_in_sorted_tri(
                v, x2, y2, z2, v2, x1, y1, z1, v1, x0, y0, z0, v0)

    x0 = xx0
    y0 = yy0
    z0 = zz0
    v0 = vv0

    x1 = xx1
    y1 = yy1
    z1 = zz1
    v1 = vv1

    x2 = xx3
    y2 = yy3
    z2 = zz3
    v2 = vv3

    # sort points

    if v0 > v1:
        if v0 > v2:
            if v1 > v2:
                draw_surface_in_sorted_tri(
                    v, x0, y0, z0, v0, x1, y1, z1, v1, x2, y2, z2, v2)
            else:
                draw_surface_in_sorted_tri(
                    v, x0, y0, z0, v0, x2, y2, z2, v2, x1, y1, z1, v1)
        else:
            draw_surface_in_sorted_tri(
                v, x2, y2, z2, v2, x0, y0, z0, v0, x1, y1, z1, v1)

    else:
        if v1 > v2:
            if v0 > v2:
                draw_surface_in_sorted_tri(
                    v, x1, y1, z1, v1, x0, y0, z0, v0, x2, y2, z2, v2)
            else:
                draw_surface_in_sorted_tri(
                    v, x1, y1, z1, v1, x2, y2, z2, v2, x0, y0, z0, v0)
        else:
            draw_surface_in_sorted_tri(
                v, x2, y2, z2, v2, x1, y1, z1, v1, x0, y0, z0, v0)

    x0 = xx1
    y0 = yy1
    z0 = zz1
    v0 = vv1

    x1 = xx2
    y1 = yy2
    z1 = zz2
    v1 = vv2

    x2 = xx3
    y2 = yy3
    z2 = zz3
    v2 = vv3

    # sort points

    if v0 > v1:
        if v0 > v2:
            if v1 > v2:
                draw_level_in_sorted_tri(
                    v, x0, y0, z0, v0, x1, y1, z1, v1, x2, y2, z2, v2)
            else:
                draw_level_in_sorted_tri(
                    v, x0, y0, z0, v0, x2, y2, z2, v2, x1, y1, z1, v1)
        else:
            draw_level_in_sorted_tri(
                v, x2, y2, z2, v2, x0, y0, z0, v0, x1, y1, z1, v1)

    else:
        if v1 > v2:
            if v0 > v2:
                draw_level_in_sorted_tri(
                    v, x1, y1, z1, v1, x0, y0, z0, v0, x2, y2, z2, v2)
            else:
                draw_level_in_sorted_tri(
                    v, x1, y1, z1, v1, x2, y2, z2, v2, x0, y0, z0, v0)
        else:
            draw_level_in_sorted_tri(
                v, x2, y2, z2, v2, x1, y1, z1, v1, x0, y0, z0, v0)


def find_cross(v, x0, y0, z0, v0, x1, y1, z1, v1):

    if ((v >= v0) and (v <= v1)) or ((v >= v1) and (v <= v0)):
        if (v0 != v1):
            a = (v - v0) / (v1 - v0)
            xx1 = a * (x1 - x0) + x0
            yy1 = a * (y1 - y0) + y0
            zz1 = a * (z1 - z0) + z0

            a = array([xx1, yy1, zz1])
            a.shape = (1, 3)
            return a

    return None


def gl_draw_surface(pts, val, v):

    global flag

    xx0 = pts[0, 0]
    yy0 = pts[0, 1]
    zz0 = pts[0, 2]
    xx1 = pts[1, 0]
    yy1 = pts[1, 1]
    zz1 = pts[1, 2]
    xx2 = pts[2, 0]
    yy2 = pts[2, 1]
    zz2 = pts[2, 2]
    xx3 = pts[3, 0]
    yy3 = pts[3, 1]
    zz3 = pts[3, 2]

    vv0 = val[0]
    vv1 = val[1]
    vv2 = val[2]
    vv3 = val[3]

    point_list = array([], float)
    point_list.shape = (0, 3)

    v0 = vv0
    x0 = xx0
    y0 = yy0
    z0 = zz0
    v1 = vv1
    x1 = xx1
    y1 = yy1
    z1 = zz1
    pts = find_cross(v, x0, y0, z0, v0, x1, y1, z1, v1)
    if pts is not None:
        point_list = concatenate((point_list, pts))

    v0 = vv0
    x0 = xx0
    y0 = yy0
    z0 = zz0
    v1 = vv2
    x1 = xx2
    y1 = yy2
    z1 = zz2
    pts = find_cross(v, x0, y0, z0, v0, x1, y1, z1, v1)
    if pts is not None:
        point_list = concatenate((point_list, pts))

    v0 = vv0
    x0 = xx0
    y0 = yy0
    z0 = zz0
    v1 = vv3
    x1 = xx3
    y1 = yy3
    z1 = zz3
    pts = find_cross(v, x0, y0, z0, v0, x1, y1, z1, v1)
    if pts is not None:
        point_list = concatenate((point_list, pts))

    v0 = vv1
    x0 = xx1
    y0 = yy1
    z0 = zz1
    v1 = vv2
    x1 = xx2
    y1 = yy2
    z1 = zz2
    pts = find_cross(v, x0, y0, z0, v0, x1, y1, z1, v1)
    if pts is not None:
        point_list = concatenate((point_list, pts))

    v0 = vv1
    x0 = xx1
    y0 = yy1
    z0 = zz1
    v1 = vv3
    x1 = xx3
    y1 = yy3
    z1 = zz3
    pts = find_cross(v, x0, y0, z0, v0, x1, y1, z1, v1)
    if pts is not None:
        point_list = concatenate((point_list, pts))

    v0 = vv2
    x0 = xx2
    y0 = yy2
    z0 = zz2
    v1 = vv3
    x1 = xx3
    y1 = yy3
    z1 = zz3
    pts = find_cross(v, x0, y0, z0, v0, x1, y1, z1, v1)
    if pts is not None:
        point_list = concatenate((point_list, pts))

    # here, we can draw

    if len(point_list) == 4:
        glBegin(GL_TRIANGLE_STRIP)
        for p in point_list:
            glVertex3f(p[0], p[1], p[2])
        glEnd()

    elif len(point_list) == 3:
        glBegin(GL_TRIANGLES)
        for p in point_list:
            glVertex3f(p[0], p[1], p[2])
        glEnd()


def ComputeDelaunay(pos, PointsFile='/tmp/points.dat',
                    DelaunayFile='/tmp/delaunay.dat'):

    ndim = pos.shape[1]
    nbody = len(pos)

    f = open(PointsFile, 'w')

    # save points in a sutable format
    f.write("%i None\n" % (ndim))
    f.write("%d\n" % nbody)

    for i in range(nbody):
        line = "%10.5f %10.5f %10.5f \n" % (pos[i][0], pos[i][1], pos[i][2])
        f.write(line)

    f.close()

    # compute delaunay
    print("computing delaunay...")
    os.system("cat %s | ./qdelaunay i > %s" % (PointsFile, DelaunayFile))
    print("done...")


def FromDelaunayGenerateLinesList(pos, DelaunayFile='/tmp/delaunay.dat'):

    print("read delaunay...")
    nco, coo = read_delaunay3d(DelaunayFile)
    print("done...")

    print("create list")

    for pt in coo:
        gl_draw_tet(pos[pt])

    print("done")


def FromDelaunayGenerateSurfaceList(
        pos, val, level, DelaunayFile='/tmp/delaunay.dat'):

    global flag

    print("read delaunay...")
    nco, coo = read_delaunay3d(DelaunayFile)
    print("done...")

    print("create list")

    for pt in coo:
        if flag == 0:
            gl_draw_surface(pos[pt], val[pt], level)

            # glBegin(GL_LINES)
            # gl_old_draw_surface(pos[pt],val[pt],level)
            # glEnd()

    print("done")
