# -*- coding: utf-8 -*-

'''
 @package   glups
 @file      PointObject.py
 @brief     Definition of a Point object
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section COPYRIGHT

 Copyright (C) 2015 EPFL (Ecole Polytechnique F??d??rale de Lausanne)
 LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of glups.
'''

from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *

from .GLObject import *
from .ParameterList import *

from numpy import *

from glups import libglups


def TextureInit():
    pass


def TextureEnable():

    glPointSize(1.0)
    quadratic = array([1.0, 0.0, 1 / 100000.])
    glPointParameterfv(GL_POINT_DISTANCE_ATTENUATION, quadratic)
    glPointParameterf(GL_POINT_SIZE_MIN, 1.0)
    glPointParameterf(GL_POINT_SIZE_MAX, 2048.0)

    # /* Centre la texture sur le point */
    glTexEnvf(GL_POINT_SPRITE, GL_COORD_REPLACE, GL_TRUE)
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)

    glEnable(GL_BLEND)
    glEnable(GL_POINT_SMOOTH)
    glEnable(GL_POINT_SPRITE)
    glEnable(GL_TEXTURE_2D)
    glBlendFunc(GL_SRC_ALPHA, GL_ONE)


def TextureDisable():

    glDisable(GL_POINT_SMOOTH)
    glDisable(GL_POINT_SPRITE)
    glDisable(GL_TEXTURE_2D)
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)


class PointsObject(GLObject):

    def __init__(self, nb, ID='Points', var=None, mn=None, mx=None, pal=None, size=None):

        self.ID = ID
        self.ListID = None
        self.nb = nb

        self.mn = mn
        self.mx = mx
        self.var = var
        self.pal = pal
        
        if size is None:
          self.size= 1
        else:
          self.size = size

        # init parameters
        self.param = ParametersList(self.ID)
        self.param["Color_r"] = 1.0
        self.param["Color_g"] = 1.0
        self.param["Color_b"] = 1.0
        self.param["Color_a"] = 1.0
        self.param["show"] = 1

        # TextureInit()

        self.GenList()

    def GenList(self):

        if self.ListID is None:
            self.ListID = glGenLists(1)
        glNewList(self.ListID, GL_COMPILE)

        if self.nb.nbody > 0:

            # glBegin(GL_POINTS)
            # for i in range(self.nb.nbody):
            #  glVertex3f(self.nb.pos[i,0],self.nb.pos[i,1],self.nb.pos[i,2])
            #
            # glEnd()

            # here, we do the same in C
            # libglups.MakePointList(self.nb.pos)
            # libglups.MakeArrowList(self.nb.pos,self.nb.vel,0.3)

            if self.var is not None:
                libglups.MakeColorPointList(
                    self.nb.pos, self.var, float(
                        self.mn), float(
                        self.mx), self.pal)
            else:
                libglups.MakePointList(self.nb.pos)

            ###################################################################
            #
            """

     glBegin( GL_POINTS )

     for i in range(self.nb.nbody):

       #glColor3f(*colors[index])
       #glNormal3f(*normals[index])
       #glTexCoord3f(*tex_coords[index])
       #glTexCoord3f(width,height,depth)

       glVertex3f(self.nb.pos[i,0],self.nb.pos[i,1],self.nb.pos[i,2])


     glEnd()

     """
            #
            ###################################################################

        glEndList()

    def Display(self, color=None):
        '''
        display the object
        '''

        # TextureEnable()
        glPointSize(self.size)

        if (self.param['show']):
            if color is None:
                glColor4d(
                    self.param["Color_r"],
                    self.param["Color_g"],
                    self.param["Color_b"],
                    self.param["Color_a"])
            else:
                glColor4d(color[0], color[1], color[2], self.param["Color_a"])

            glPushMatrix()
            glCallList(self.ListID)
            glPopMatrix()

        # TextureDisable()

    def MulAlpha(self, f):
        self.param["Color_a"] = self.param["Color_a"] * f
        self.param["Color_a"] = max(0, self.param["Color_a"])
        self.param["Color_a"] = min(1, self.param["Color_a"])

    def DivAlpha(self, f):
        self.param["Color_a"] = self.param["Color_a"] / f
        self.param["Color_a"] = max(0, self.param["Color_a"])
        self.param["Color_a"] = min(1, self.param["Color_a"])

    def SetAlpha(self, a):
        self.param["Color_a"] = a
        self.param["Color_a"] = max(0, self.param["Color_a"])
        self.param["Color_a"] = min(1, self.param["Color_a"])

    def GetAlpha(self):
        return self.param["Color_a"]

    def SetColor4d(self, r, g, b, a):
        self.param["Color_r"] = r
        self.param["Color_g"] = g
        self.param["Color_b"] = b
        self.param["Color_a"] = a

    def SetColor3d(self, r, g, b):
        self.param["Color_r"] = r
        self.param["Color_g"] = g
        self.param["Color_b"] = b

    def SetColor(self, c):
        self.param["Color_r"] = c[0]
        self.param["Color_g"] = c[1]
        self.param["Color_b"] = c[2]
        self.param["Color_a"] = c[3]
