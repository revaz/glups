####################################################################################################################################
#
# BINARY CLASS
#
####################################################################################################################################    

class Nbody_binary(NbodyDefault):

  
  def get_read_fcts(self):
    return [self.read_particles,self.read_mass]
    
  def get_write_fcts(self):
    return [self.write_particles,self.write_mass]    
    
  def get_spec_vars(self):
    '''
    return specific variables default values for the class
    '''
    return {'tnow'   :0.,
            'label'  :'',
            'dt'     :0.
	    }
      

  def read_particles(self,f):
    '''
    read binary particle file
    '''
    
    ##########################################
    # read the header and send it to each proc
    ##########################################    
    tpl = (Float32,Int,Float32,40)
    header = io.ReadBlock(f,tpl,byteorder=self.byteorder,pio=self.pio)
    tnow,nbody,dt,label = header 

    ##########################################
    # computes npart_all
    ##########################################
    ntype = 1

    if self.pio == 'yes':
      npart = ones((ntype)) * nbody
      npart_tot = mpi.mpi_reduce(npart) 
      npart_all = npart_all = zeros((mpi.NTask,ntype))
    
    else:
      npart_tot = ones((ntype)) * nbody 
            
      npart_all = zeros((mpi.NTask,ntype))
      
      for i in range(ntype):
        for Task in range(mpi.NTask-1,-1,-1):
          npart_all[Task,i] =  npart_tot[i]/mpi.NTask +  npart_tot[i]%mpi.NTask*(Task==0)
	  
      npart = npart_all[mpi.ThisTask]	  
                
    ##########################################
    # read and send particles attribute
    ##########################################        

    pos = io.ReadArray(f,Float32,shape=(nbody,3),byteorder=self.byteorder,pio=self.pio,nlocal=npart_all)
    vel = io.ReadArray(f,Float32,shape=(nbody,3),byteorder=self.byteorder,pio=self.pio,nlocal=npart_all)
    
    ##########################################
    # make global
    ##########################################    
    
    self.tnow  = tnow
    self.label = label
    self.dt    = dt
    
    self.pos = pos
    self.vel = vel
    
    self.npart = npart
    self.npart_tot = npart_tot
        

  def read_mass(self,f):
    '''
    read binary mass file
    '''
                
    ##########################################
    # read and send mass
    ##########################################        
    
    # here we assume that self.npart is known from the particle file
    npart_all = array(mpi.mpi_Allgather(self.npart))
    mass = io.ReadArray(f,Float32,byteorder=self.byteorder,pio=self.pio,nlocal=npart_all)
        
    ##########################################
    # make global
    ##########################################    
    self.mass = mass
    

  def write_particles(self,f):
    '''
    specific format for particle file
    '''
    npart_all = array(mpi.mpi_Allgather(self.npart))
    
    if self.pio == 'yes':
      nbody = self.nbody
    else:
      nbody = mpi.mpi_reduce(self.nbody) 
    
    
    # header 
    tpl = ((self.tnow,Float32),(nbody,Int),(self.dt,Float32),(self.label,40))
    io.WriteBlock(f,tpl,byteorder=self.byteorder)																																		   
    
    # positions 	
    io.WriteArray(f,self.pos.astype(Float32),byteorder=self.byteorder,pio=self.pio,nlocal=npart_all)																												   
    # velocities																																						   
    io.WriteArray(f,self.vel.astype(Float32),byteorder=self.byteorder,pio=self.pio,nlocal=npart_all)																												   
	
	
  def write_mass(self,f):
    '''
    specific format for mass file
    '''
    npart_all = array(mpi.mpi_Allgather(self.npart))
    
    # mass 	
    io.WriteArray(f,self.mass.astype(Float32),byteorder=self.byteorder,pio=self.pio,nlocal=npart_all)																												   
    
    
	
        
  def spec_info(self):
    """
    Write spec info
    """	
    infolist = []
    infolist.append("")
    infolist.append("tnow                : %s"%self.tnow)	
    infolist.append("label               : %s"%self.label)		
      
    return infolist  
      

  
