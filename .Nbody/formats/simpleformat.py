
class Nbody_simpleformat(NbodyDefault):
  
  def get_read_fcts(self):
    return [self.read_particles]
    
  def get_write_fcts(self):
    return [self.write_particles]    
    
  def get_spec_vars(self):
    '''
    return specific variables default values for the class
    '''
    return {'tnow'   :0.,
            'ngas'   :0,
            'nstar'  :0
	    }

  def read_particles(self,f):
    '''
    read binary particle file
    '''
    
    ##########################################
    # read the header and send it to each proc
    ##########################################    
        
    tpl = (Float32,Int32,Int32)
    header = io.ReadBlock(f,tpl,byteorder=self.byteorder,pio=self.pio)
    tnow, ngas, nstar = header 
    
    ##########################################
    # set nbody and npart
    ##########################################    
    
    npart_read = array([ngas,nstar])
        
    # all particles    
    npart,npart_all       = self.get_npart_and_npart_all(npart_read)
    # only gas particles
    npartgas,npartgas_all = self.get_npart_and_npart_all(npart_read[0])
    
    # now, get the corect values
    ngas  = npart[0]
    nstar = npart[1]
    nbody = sum(npart)
           		
    ####################################################################################
    # read and send particles position/velocities/mass/metallicity
    ####################################################################################      

    self.pos = io.ReadArray(f,Float32,shape=(nbody,3),byteorder=self.byteorder,pio=self.pio,nlocal=npart_all)
    self.vel = io.ReadArray(f,Float32,shape=(nbody,3),byteorder=self.byteorder,pio=self.pio,nlocal=npart_all)
    self.mass= io.ReadArray(f,Float32,                byteorder=self.byteorder,pio=self.pio,nlocal=npart_all)
    self.meta= io.ReadArray(f,Float32,                byteorder=self.byteorder,pio=self.pio,nlocal=npartgas_all)
    self.meta  = concatenate((self.meta,zeros(nbody-npart[0]).astype(Float32)))
    
    ##########################################
    # make default and specific variables global
    ##########################################  
    self.make_default_variables_global(vars())
    self.make_specific_variables_global(vars())
    
    

  def write_particles(self,f):
    '''
    specific format for particle file
    '''
    npart_all = array(mpi.mpi_Allgather(self.npart))
    
    if self.pio == 'yes':
      ngas  = self.ngas
      nstar = self.nstar
    else:
      ngas  = mpi.mpi_reduce(self.ngas)
      nstar = mpi.mpi_reduce(self.nstar)
    
    # header 
    tpl = ((self.tnow,Float32),(ngas,Int32),(nstar,Int32))
    io.WriteBlock(f,tpl,byteorder=self.byteorder)																																		   
    
    # positions 	
    io.WriteArray(f,self.pos.astype(Float32),byteorder=self.byteorder,pio=self.pio,nlocal=npart_all)																												   
    io.WriteArray(f,self.vel.astype(Float32),byteorder=self.byteorder,pio=self.pio,nlocal=npart_all)
    io.WriteArray(f,self.mass.astype(Float32),byteorder=self.byteorder,pio=self.pio,nlocal=npart_all)																												   
    io.WriteArray(f,self.meta[:self.npart[0]].astype(Float32),byteorder=self.byteorder,pio=self.pio,nlocal=npart_all)

  def spec_info(self):
    """
    Write spec info
    """	
    infolist = []
    infolist.append("")
    infolist.append("tnow                : %s"%self.tnow)	
    infolist.append("ngas                : %s"%self.ngas)
    infolist.append("nstar               : %s"%self.nstar)		
    return infolist  

  def Metallicity(self):
    """
    Return the metallicity
    """	
    return self.meta  
