####################################################################################################################################
#
# TIRET CLASS
#
####################################################################################################################################    

class Nbody_tiretdm(NbodyDefault):

  
  def get_read_fcts(self):
    return [self.read_particles]
    
  def get_write_fcts(self):
    return [self.write_particles]    
    
  def get_spec_vars(self):
    '''
    return specific variables default values for the class
    '''
    return {'nstar'   :0,
            'ngas'    :0,
            'nbulge'  :0
	    }
      

  def read_particles(self,f):
    '''
    read binary particle file
    '''
    
    self.byteorder = 'big'
    
    ##########################################
    # read the header and send it to each proc
    ##########################################    
        
    tpl = (Int,Int,Int,Int)
    header = io.ReadBlock(f,tpl,byteorder=self.byteorder,pio=self.pio)
    nstar, ngas, nbulge, nhalo = header 
    nbody = nstar + ngas + nbulge + nhalo

    ##########################################
    # computes npart_all
    ##########################################
    ntype = 1

    if self.pio == 'yes':
      npart = ones((ntype)) * nbody
      npart_tot = mpi.mpi_reduce(npart) 
      npart_all = npart_all = zeros((mpi.NTask,ntype))
    
    else:
      npart_tot = ones((ntype)) * nbody 
            
      npart_all = zeros((mpi.NTask,ntype))
      
      for i in range(ntype):
        for Task in range(mpi.NTask-1,-1,-1):
          npart_all[Task,i] =  npart_tot[i]/mpi.NTask +  npart_tot[i]%mpi.NTask*(Task==0)
	  
      npart = npart_all[mpi.ThisTask]	  
                
    ##########################################
    # read and send particles attribute
    ##########################################        

    star_pos  = io.ReadArray(f,Float64,shape=(nstar,3),byteorder=self.byteorder,pio=self.pio,nlocal=npart_all)
    star_vel  = io.ReadArray(f,Float64,shape=(nstar,3),byteorder=self.byteorder,pio=self.pio,nlocal=npart_all)
    star_mass = io.ReadArray(f,Float64,shape=(nstar),  byteorder=self.byteorder,pio=self.pio,nlocal=npart_all)
    star_tpe  = io.ReadArray(f,Int    ,shape=(nstar),  byteorder=self.byteorder,pio=self.pio,nlocal=npart_all)
  
    gas_pos  = io.ReadArray(f,Float64,shape=(ngas,3),byteorder=self.byteorder,pio=self.pio,nlocal=npart_all)
    gas_vel  = io.ReadArray(f,Float64,shape=(ngas,3),byteorder=self.byteorder,pio=self.pio,nlocal=npart_all)
    gas_mass = io.ReadArray(f,Float64,shape=(ngas),  byteorder=self.byteorder,pio=self.pio,nlocal=npart_all)
    gas_tpe  = io.ReadArray(f,Int    ,shape=(ngas),  byteorder=self.byteorder,pio=self.pio,nlocal=npart_all)
    
    bulge_pos  = io.ReadArray(f,Float64,shape=(nbulge,3),byteorder=self.byteorder,pio=self.pio,nlocal=npart_all)
    bulge_vel  = io.ReadArray(f,Float64,shape=(nbulge,3),byteorder=self.byteorder,pio=self.pio,nlocal=npart_all)
    bulge_mass = io.ReadArray(f,Float64,shape=(nbulge),  byteorder=self.byteorder,pio=self.pio,nlocal=npart_all)
    bulge_tpe  = io.ReadArray(f,Int    ,shape=(nbulge),  byteorder=self.byteorder,pio=self.pio,nlocal=npart_all)

    halo_pos   = io.ReadArray(f,Float64,shape=(nhalo,3),byteorder=self.byteorder,pio=self.pio,nlocal=npart_all)
    halo_vel   = io.ReadArray(f,Float64,shape=(nhalo,3),byteorder=self.byteorder,pio=self.pio,nlocal=npart_all)
    halo_mass  = io.ReadArray(f,Float64,shape=(nhalo),  byteorder=self.byteorder,pio=self.pio,nlocal=npart_all)
    halo_tpe   = io.ReadArray(f,Int    ,shape=(nhalo),  byteorder=self.byteorder,pio=self.pio,nlocal=npart_all)
    
    
    pos  = concatenate((star_pos,gas_pos,bulge_pos,halo_pos))
    vel  = concatenate((star_vel,gas_vel,bulge_vel,halo_vel))
    mass = concatenate((star_mass,gas_mass,bulge_mass,halo_mass))
    tpe  = concatenate((star_tpe,gas_tpe,bulge_tpe,halo_tpe))

   
    ##########################################
    # make global
    ##########################################  
    
    self.tnow = 0.
    
    self.nstar  = nstar
    self.ngas   = ngas
    self.nbulge = nbulge  
    self.nhalo  = nhalo
        
    self.pos = pos
    self.vel = vel

    self.mass = mass    
    self.tpe = tpe
    
    self.npart = npart
    self.npart_tot = npart_tot
        
  

  def write_particles(self,f):
    '''
    specific format for particle file
    '''
    pass																											   
	
	            
  def spec_info(self):
    """
    Write spec info
    """	
    infolist = []
    infolist.append("")
    infolist.append("tnow                : %s"%self.tnow)	
    infolist.append("label               : %s"%self.label)		
      
    return infolist  
      

  def select(self,tpe='gas',val=None):
    """ 
    Return an N-body object that contain only particles of a 
    certain type, defined by in gadget:

    gas		: 	gas particles
    stars	: 	stars particles
    bulge	:	bulge particles

    """   

    index = {'stars':1,'bulge':2,'gas':4,'halo':3}
    
        

    if not index.has_key(tpe):
      print "unknown type %s"%(tpe)
      return self


    return self.selectc((self.tpe==index[tpe]))

   
