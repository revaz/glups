####################################################################################################################################
#
# DEFAULT CLASS
#
####################################################################################################################################    

class Nbody_default(NbodyDefault):
  '''
  This class is usefull to create an empty Nbody object
  '''
  
  def get_read_fcts(self):
    return [self.read_particles]
    
  def get_write_fcts(self):
    return [self.write_particles]    

  def get_spec_vars(self):
    return {}
    
  def read_particles(self,f):
    pass
      
  def write_particles(self,f):
    pass
    
