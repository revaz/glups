#!/usr/bin/env python
'''
 @package   pNbody
 @file      setup.py
 @brief     Install pNbody
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

from setuptools import setup, find_packages, Extension
from glob import glob
from copy import deepcopy
import numpy as np


# default flags
flags = ["-Wno-unused-parameter"]

# directory containing headers
include_dirs = [
    ".",
    np.get_include()
]

# libraries
libraries = ["m"]

# C modules
modules_name = [
    "libglups"
]

# data files
#data_files = [('config', glob('config/glups'))]
package_files  = glob('config/glups')

# DO NOT TOUCH BELOW

# Generate extensions
ext_modules = []

for k in modules_name:
    # compile extension
    tmp = Extension("glups." + k,
                    glob("src/" + k + "/*.c"),
                    include_dirs=include_dirs,
                    libraries=libraries,
                    extra_compile_args=flags)

    ext_modules.append(tmp)


# scripts to install
scripts = glob("scripts/*")

setup(
    name="glups",
    author="Yves Revaz",
    author_email="yves.revaz@epfl.ch",
    url="http://obswww.unige.ch/~revaz/glups/index.html",
    description="glups module",
    license="GPLv3",
    version="5.0",

    packages=find_packages(),
    scripts=scripts,
    ext_modules=ext_modules,
    #data_files=data_files,
    package_data={'glups': package_files},
    install_requires=["pNbody","numpy", "scipy", "h5py",
                      "astropy","PyQt5","PyOpenGL","qtconsole"],

)
